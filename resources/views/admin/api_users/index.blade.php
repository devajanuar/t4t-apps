@extends('admin.layout.main')
@section('breadcrumb')
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">API User Manage</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}" class="text-muted">Home</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">API User Manage</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5">
            <div align="right">
                <a href="{{ url('admin/api-users/create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add User</a>
            </div>
        </div>
    </div>
@endsection
@section('content')
    @push('bottom')
        <script>
            $(function() {
                $('#data-table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ url('admin/api-users/list') }}",
                    order: [ [0,'desc']]
                })
            })
        </script>
    @endpush
    <div class="card">
        <div class="card-body">
            <table class="table table-striped table-bordered" id="data-table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>

@endsection