@extends('admin.layout.main')
@section('breadcrumb')
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">API User Manage</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}" class="text-muted">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('admin/api-users') }}" class="text-muted">API User Manage</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Create</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5">
            <div align="right">
                <a href="{{ url('admin/api-users') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back To List</a>
            </div>
        </div>
    </div>
@endsection
@section('content')
    @php /** @var $row \App\Models\ApiUsers */ @endphp
    @php /** @var $data_permission \Illuminate\Support\Collection */ @endphp
    <div class="card">
        <div class="card-body">
            <form method="post" action="">
                {!! csrf_field() !!}

                <div class="form-body">

                    <input type="hidden" name="id" value="{{ $row->id }}">

                    <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" name="name" required value="{{ $row->name }}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="">Username</label>
                        <input type="text" name="username" required value="{{ $row->username }}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="">Password</label>
                        <input type="password" name="password" @if(!$row->id) required @endif class="form-control">
                        @if($row->id)
                        <div class="help-block">Please leave empty if not changed</div>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="">API Access Permission</label>
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th width="20px">No</th><th>API</th><th width="25px">Enable</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(\App\Helpers\ApiPermissionList::data() as $i => $item)
                                @php
                                    $permission_name = strtolower(\Illuminate\Support\Str::slug($item['label'],'_'));
                                    $checked = "";
                                    if(isset($data_permission)) {
                                        $selected_data = $data_permission->where("permission_name", $permission_name)->where("is_active",1)->count();
                                        $checked = ($selected_data)?"checked":"";
                                    }
                                @endphp
                                <tr>
                                    <td>{{ $no = $i + 1 }}</td>
                                    <td>{{ $item['label'] }}</td>
                                    <td class="text-center"><input {{ $checked }} type="checkbox" name="permission[{{ $permission_name }}]" value="1"></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    
                </div>

                <div class="form-actions">
                    <div class="text-right">
                        <input type="submit" value="Save" class="btn btn-success">
                        <input type="reset" value="Reset" class="btn btn-dark">
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection