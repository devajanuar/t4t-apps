@extends('admin.layout.main')
@section('breadcrumb')
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Dashboard</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}" class="text-muted">Home</a></li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5">
            <div align="right">

            </div>
        </div>
    </div>
@endsection
@section('content')

    <div id="app-vue">
        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm">
                                <h4 class="card-title">
                                   Hits Chart
                                </h4>
                            </div>
                            <div class="col-sm">
                                <div class="text-right">
                                    <a href="javascript:;" @click="showFilterHits" class="btn btn-sm btn-info">Filter</a>
                                </div>
                            </div>
                        </div>
                        <div v-if="hits_loading" class="text-center mt-3"><i class="fa fa-spin fa-spinner"></i> Please wait loading...</div>
                        <div id="line-chart-hits"></div>
                    </div>
                </div>
                <div class="modal fade" id="modal-filter-hits">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Filter Hits Chart</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm">
                                        <div class="form-group">
                                            <label for="">From</label>
                                            <input type="date" v-model="hits_filter_from" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <div class="form-group">
                                            <label for="">To</label>
                                            <input type="date" v-model="hits_filter_to" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" @click="filterHits" class="btn btn-primary">Apply</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </div>

            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm">
                                <h4 class="card-title">
                                    Top 10 Hits Chart
                                </h4>
                            </div>
                            <div class="col-sm">
                                <div class="text-right">
                                    <a href="javascript:;" @click="showFilterTopHits" class="btn btn-sm btn-info">Filter</a>
                                </div>
                            </div>
                        </div>
                        <div v-if="top_hits_loading" class="text-center mt-3"><i class="fa fa-spin fa-spinner"></i> Please wait loading...</div>
                        <div id="line-chart-top-hits"></div>
                    </div>
                </div>
                <div class="modal fade" id="modal-filter-top-hits">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Filter Top Hits Chart</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm">
                                        <div class="form-group">
                                            <label for="">From</label>
                                            <input type="date" v-model="top_hits_filter_from" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <div class="form-group">
                                            <label for="">To</label>
                                            <input type="date" v-model="top_hits_filter_to" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" @click="filterTopHits" class="btn btn-primary">Apply</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </div>


            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm">
                                <h4 class="card-title">
                                    Errors Chart
                                </h4>
                            </div>
                            <div class="col-sm">
                                <div class="text-right">
                                    <a href="javascript:;" @click="showFilterErrors" class="btn btn-sm btn-info">Filter</a>
                                </div>
                            </div>
                        </div>
                        <div v-if="errors_loading" class="text-center mt-3"><i class="fa fa-spin fa-spinner"></i> Please wait loading...</div>
                        <div id="line-chart-errors"></div>
                    </div>
                </div>
                <div class="modal fade" id="modal-filter-errors">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Filter Errors Chart</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm">
                                        <div class="form-group">
                                            <label for="">From</label>
                                            <input type="date" v-model="errors_filter_from" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <div class="form-group">
                                            <label for="">To</label>
                                            <input type="date" v-model="errors_filter_to" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" @click="filterErrors" class="btn btn-primary">Apply</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </div>

            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm">
                                <h4 class="card-title">
                                    Top 10 Errors Chart
                                </h4>
                            </div>
                            <div class="col-sm">
                                <div class="text-right">
                                    <a href="javascript:;" @click="showFilterTopErrors" class="btn btn-sm btn-info">Filter</a>
                                </div>
                            </div>
                        </div>
                        <div v-if="top_errors_loading" class="text-center mt-3"><i class="fa fa-spin fa-spinner"></i> Please wait loading...</div>
                        <div id="line-chart-top-errors"></div>
                    </div>
                </div>
                <div class="modal fade" id="modal-filter-top-errors">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Filter Top Errors Chart</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm">
                                        <div class="form-group">
                                            <label for="">From</label>
                                            <input type="date" v-model="top_errors_filter_from" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <div class="form-group">
                                            <label for="">To</label>
                                            <input type="date" v-model="top_errors_filter_to" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" @click="filterTopErrors" class="btn btn-primary">Apply</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </div>


            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm">
                                <h4 class="card-title">
                                    Tree Allocated By Period
                                </h4>
                            </div>
                            <div class="col-sm">
                                <div class="text-right">
                                    <a href="javascript:;" @click="showFilterTreeAllocated" class="btn btn-sm btn-info">Filter</a>
                                </div>
                            </div>
                        </div>
                        <div v-if="tree_allocated_loading" class="text-center mt-3"><i class="fa fa-spin fa-spinner"></i> Please wait loading...</div>
                        <div id="line-chart-tree-allocated"></div>
                    </div>
                </div>
                <div class="modal fade" id="modal-filter-tree-allocated">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Filter Tree Allocated</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm">
                                        <div class="form-group">
                                            <label for="">From</label>
                                            <input type="date" v-model="tree_allocated_filter_from" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <div class="form-group">
                                            <label for="">To</label>
                                            <input type="date" v-model="tree_allocated_filter_to" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="">Participant</label>
                                            <select v-model="tree_allocated_participant_id" class="form-control">
                                                <option value="">All Participant</option>
                                                @foreach(\App\Models\Participants::all() as $participant)
                                                    <option value="{{ $participant->id }}">{{ $participant->first_name." ".$participant->last_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" @click="filterTreeAllocated" class="btn btn-primary">Apply</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </div>
        </div>
    </div>

    @push('bottom')
        <script>
            var vue = new Vue({
                el: "#app-vue",
                data() {
                    return {
                        hits_chart: null,
                        hits_filter_from: "",
                        hits_filter_to: "",
                        hits_loading: true,
                        hits_data: [],

                        top_hits_chart: null,
                        top_hits_filter_from: "",
                        top_hits_filter_to: "",
                        top_hits_loading: true,
                        top_hits_data: [],

                        errors_chart: null,
                        errors_filter_from: "",
                        errors_filter_to: "",
                        errors_loading: true,
                        errors_data: [],

                        top_errors_chart: null,
                        top_errors_filter_from: "",
                        top_errors_filter_to: "",
                        top_errors_loading: true,
                        top_errors_data: [],

                        tree_allocated_chart: null,
                        tree_allocated_filter_from: "",
                        tree_allocated_filter_to: "",
                        tree_allocated_loading: true,
                        tree_allocated_participant_id: "",
                        tree_allocated_data: []
                    }
                },
                mounted() {
                    this.hits_chart = new Morris.Line({
                        element: 'line-chart-hits',
                        data: [],
                        xkey: 'request_date',
                        ykeys: ['total'],
                        labels: ['Total Request'],
                        gridLineColor: '#eef0f2',
                        lineColors: ['#5f76e8'],
                        lineWidth: 1,
                        hideHover: 'auto'
                    });
                    this.reloadHits()

                    this.top_hits_chart = new Morris.Line({
                        element: 'line-chart-top-hits',
                        parseTime: false,
                        data: [],
                        xkey: 'url',
                        ykeys: ['total'],
                        labels: ['Total Request'],
                        gridLineColor: '#eef0f2',
                        lineColors: ['#5f76e8'],
                        lineWidth: 1,
                        hideHover: 'auto'
                    });
                    this.reloadTopHits()


                    this.errors_chart = new Morris.Line({
                        element: 'line-chart-errors',
                        data: [],
                        xkey: 'request_date',
                        ykeys: ['total'],
                        labels: ['Total Request'],
                        gridLineColor: '#eef0f2',
                        lineColors: ['#5f76e8'],
                        lineWidth: 1,
                        hideHover: 'auto'
                    });
                    this.reloadErrors()

                    this.top_errors_chart = new Morris.Line({
                        element: 'line-chart-top-errors',
                        parseTime: false,
                        data: [],
                        xkey: 'url',
                        ykeys: ['total'],
                        labels: ['Total Request'],
                        gridLineColor: '#eef0f2',
                        lineColors: ['#5f76e8'],
                        lineWidth: 1,
                        hideHover: 'auto'
                    });
                    this.reloadTopErrors()


                    this.tree_allocated_chart = new Morris.Line({
                        element: 'line-chart-tree-allocated',
                        parseTime: false,
                        data: [],
                        xkey: 'request_date',
                        ykeys: ['total'],
                        labels: ['Total Request'],
                        gridLineColor: '#eef0f2',
                        lineColors: ['#5f76e8'],
                        lineWidth: 1,
                        hideHover: 'auto'
                    });
                    this.reloadTreeAllocated()
                },
                methods: {
                    showFilterHits: function() {
                      $('#modal-filter-hits').modal('show')
                    },
                    filterHits: function() {
                        $('#modal-filter-hits').modal('hide')
                        this.reloadHits()
                    },
                    reloadHits: function() {
                        this.hits_loading = true
                        axios.get("{{ url('admin/dashboard/data-hits') }}?filter_from=" + this.hits_filter_from + "&filter_to=" + this.hits_filter_to)
                            .then(resp=>{
                                this.hits_chart.setData(resp.data)
                            })
                            .catch(err=>{
                                alert("Something went wrong while load hits data!")
                            })
                            .finally(()=>{
                                this.hits_loading = false
                            })
                    },



                    showFilterTopHits: function() {
                        $('#modal-filter-top-hits').modal('show')
                    },
                    filterTopHits: function() {
                        $('#modal-filter-top-hits').modal('hide')
                        this.reloadTopHits()
                    },
                    reloadTopHits: function() {
                        this.top_hits_loading = true
                        axios.get("{{ url('admin/dashboard/data-top-hits') }}?filter_from=" + this.top_hits_filter_from + "&filter_to=" + this.top_hits_filter_to)
                            .then(resp=>{
                                this.top_hits_chart.setData(resp.data)
                            })
                            .catch(err=>{
                                alert("Something went wrong while load hits data!")
                            })
                            .finally(()=>{
                                this.top_hits_loading = false
                            })
                    },

                    // Errors
                    showFilterErrors: function() {
                        $('#modal-filter-errors').modal('show')
                    },
                    filterErrors: function() {
                        $('#modal-filter-errors').modal('hide')
                        this.reloadErrors()
                    },
                    reloadErrors: function() {
                        this.errors_loading = true
                        axios.get("{{ url('admin/dashboard/data-errors') }}?filter_from=" + this.errors_filter_from + "&filter_to=" + this.errors_filter_to)
                            .then(resp=>{
                                this.errors_chart.setData(resp.data)
                            })
                            .catch(err=>{
                                alert("Something went wrong while load errors data!")
                            })
                            .finally(()=>{
                                this.errors_loading = false
                            })
                    },



                    showFilterTopErrors: function() {
                        $('#modal-filter-top-errors').modal('show')
                    },
                    filterTopErrors: function() {
                        $('#modal-filter-top-errors').modal('hide')
                        this.reloadTopErrors()
                    },
                    reloadTopErrors: function() {
                        this.top_errors_loading = true
                        axios.get("{{ url('admin/dashboard/data-top-errors') }}?filter_from=" + this.top_errors_filter_from + "&filter_to=" + this.top_errors_filter_to)
                            .then(resp=>{
                                this.top_errors_chart.setData(resp.data)
                            })
                            .catch(err=>{
                                alert("Something went wrong while load errors data!")
                            })
                            .finally(()=>{
                                this.top_errors_loading = false
                            })
                    },


                    showFilterTreeAllocated: function() {
                        $('#modal-filter-tree-allocated').modal('show')
                    },
                    filterTreeAllocated: function() {
                        $('#modal-filter-tree-allocated').modal('hide')
                        this.reloadTreeAllocated()
                    },
                    reloadTreeAllocated: function() {
                        this.top_tree_allocated = true
                        axios.get("{{ url('admin/dashboard/data-tree-allocated') }}?filter_from=" + this.tree_allocated_filter_from + "&filter_to=" + this.tree_allocated_filter_to + "&participant_id=" + this.tree_allocated_participant_id)
                            .then(resp=>{
                                this.tree_allocated_chart.setData(resp.data)
                            })
                            .catch(err=>{
                                alert("Something went wrong while load tree allocated data!")
                            })
                            .finally(()=>{
                                this.tree_allocated_loading = false
                            })
                    }
                }
            })
        </script>
    @endpush
@endsection