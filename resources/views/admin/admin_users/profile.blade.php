@extends('admin.layout.main')
@section('breadcrumb')
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">My Profile</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}" class="text-muted">Home</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Profile</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5">
            <div align="right">

            </div>
        </div>
    </div>
@endsection
@section('content')
    @php /** @var $row \App\Models\ApiAdmins */ @endphp
    <div class="card">
        <div class="card-body">
            <form method="post" action="">
                {!! csrf_field() !!}

                <div class="form-body">

                    <input type="hidden" name="id" value="{{ $row->id }}">

                    <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" name="name" required value="{{ $row->name }}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="">Email</label>
                        <input type="email" name="email" required value="{{ $row->email }}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="">Password</label>
                        <input type="password" name="password" class="form-control">
                        <div class="help-block">Please leave empty if not changed</div>
                    </div>
                </div>

                <div class="form-actions">
                    <div class="text-right">
                        <input type="submit" value="Save" class="btn btn-success">
                        <input type="reset" value="Reset" class="btn btn-dark">
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection