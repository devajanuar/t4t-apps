@extends('admin.layout.main')
@section('breadcrumb')
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">API Setting</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}" class="text-muted">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('admin/api-setting') }}" class="text-muted">API Setting</a></li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5">
            <div align="right">

            </div>
        </div>
    </div>
@endsection
@section('content')
    @php /** @var $row \App\Models\ApiSettings */ @endphp
    <div class="card">
        <div class="card-body">
            <form method="post" action="{{ url('admin/api-setting/save') }}">
                {!! csrf_field() !!}
                <div class="form-body">
                    <div class="form-group">
                        <label for="">Rate Limiting</label>
                        <input type="number" name="rate_limiting" required value="{{ get_setting('rate_limiting') }}" class="form-control">
                        <div class="help-block">How many request allowed the user can hit for a minute (default: 60)</div>
                    </div>

                    <div class="form-group">
                        <label for="">Blocked Bad Login Times</label>
                        <input type="number" name="bad_login_qty" required value="{{ get_setting('bad_login_qty') }}" class="form-control">
                        <div class="help-block">How many bad login allowed before the user temporary blocked (default: 3)</div>
                    </div>

                    <div class="form-group">
                        <label for="">Temporary Blocked (Minutes)</label>
                        <input type="number" name="temporary_blocked_minute" required value="{{ get_setting('temporary_blocked_minute') }}" class="form-control">
                        <div class="help-block">How many time for temporary blocked</div>
                    </div>

                    <div class="form-group">
                        <label for="">Token Expiry (Minutes)</label>
                        <input type="number" name="token_expiry" required value="{{ get_setting('token_expiry') }}" class="form-control">
                        <div class="help-block">How many time for token expiry</div>
                    </div>


                    <div class="form-group">
                        <label for="">Block For Specific IP Addresses</label>
                        <input type="text" name="block_ip_address" value="{{ get_setting('block_ip_address') }}" class="form-control">
                        <div class="help-block">Separate it with comma. Ex: 1.1.1.1,1.1.1.2</div>
                    </div>
                </div>

                <div class="form-actions">
                    <div class="text-right">
                        <input type="submit" value="Save" class="btn btn-success">
                        <input type="reset" value="Reset" class="btn btn-dark">
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection