@extends("admin.layout.auth")
@section('content')

    <div class="auth-box row">
        <div class="col-sm-12">
            {!! alert_flash() !!}
        </div>
        <div class="col-lg-7 col-md-5 modal-bg-img" style="background-image: url({{ asset("assets/adminmart/assets/images/big/login-bg-office.jpg") }});"></div>
        <div class="col-lg-5 col-md-7 bg-white">
            <div class="p-3">
                <div class="text-center">
                    <img src="{{ asset("assets/adminmart/assets/images/big/icon.png") }}" alt="wrapkit">
                </div>
                <h2 class="mt-3 text-center">Sign In</h2>
                <p class="text-center">Enter your email address and password to access admin panel.</p>
                <form method="post" class="mt-4">
                    {!! csrf_field() !!}
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="text-dark" for="uname">Email</label>
                                <input class="form-control" type="email"
                                       placeholder="enter your email" required name="email" >
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="text-dark" for="pwd">Password</label>
                                <input class="form-control" id="pwd" name="password" type="password"
                                       placeholder="enter your password">
                            </div>
                        </div>
                        <div class="col-lg-12 text-center">
                            <button type="submit" class="btn btn-block btn-dark">Sign In</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection