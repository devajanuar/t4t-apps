@extends('admin.layout.main')
@section('breadcrumb')
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">API Log Activity</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}" class="text-muted">Home</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">API Log Activity</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5">
            <div class="text-right">
                <a href="javascript:;" onclick="reloadTable()" class="btn btn-primary btn-reload-data"><i class="icon-refresh"></i> Reload Data</a>
                <a href="javascript:;" onclick="downloadData()" class="btn btn-secondary btn-reload-data"><i class="icon-printer"></i> Download XLS</a>
            </div>
        </div>
    </div>
@endsection
@section('content')

    <div class="modal fade" id="modal-download">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Download Log</h4>
                </div>
                <form target="_blank" action="{{ url('admin/api-log/download') }}">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm">
                            <div class="form-group">
                                <label for="">From</label>
                                <input type="date" name="filter_from" required class="form-control">
                            </div>
                        </div>
                        <div class="col-sm">
                            <div class="form-group">
                                <label for="">To</label>
                                <input type="date" name="filter_to" required class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Download</button>
                </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal fade" id="modal-payload">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Payload Detail</h4>
                </div>
                <div class="modal-body">
                    <pre></pre>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    @push('bottom')
        <script>
            $(function() {
                $('#data-table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ url('admin/api-log/list') }}",
                    order: [ [0,'desc']]
                })
            })

            function downloadData() {
                $('#modal-download').modal('show');
            }

            function reloadTable() {
                $('#data-table').DataTable().ajax.reload();
            }

            function showPayloadDetail(id) {
                $.get("{{ url('admin/api-log/payload') }}/"+id,function(resp) {
                   $('#modal-payload .modal-body pre').html(resp.data)
                   $('#modal-payload').modal('show')
                });
            }
        </script>
    @endpush
    <div class="card">
        <div class="card-body table-responsive">
            <table style="font-size: 12px !important;" class="table table-striped table-bordered" id="data-table">
                <thead>
                    <tr>
                        <th>Request Time</th>
                        <th>URL</th>
                        <th style="white-space: nowrap">IP Address</th>
                        <th>User Agent</th>
                        <th>Token</th>
                        <th style="white-space: nowrap">Response Code</th>
                        <th>Payload</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>

@endsection