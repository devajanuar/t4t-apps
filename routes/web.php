<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::namespace("\App\Http\Controllers\Admin")->group(function() {
    Route::post('admin/auth/login','AuthController@postLogin');
    Route::get('admin/auth/login','AuthController@getLogin');
    Route::get('admin/auth/logout','AuthController@getLogout');

    Route::middleware(['admin'])->group(function() {
        Route::get('admin','DashboardController@getIndex');
        Route::get('admin/dashboard/data-hits','DashboardController@getDataHits');
        Route::get('admin/dashboard/data-top-hits','DashboardController@getDataTopHits');
        Route::get('admin/dashboard/data-errors','DashboardController@getDataErrors');
        Route::get('admin/dashboard/data-top-errors','DashboardController@getDataTopErrors');
        Route::get('admin/dashboard/data-tree-allocated','DashboardController@getDataTreeAllocated');

        # Module Profile
        Route::prefix("admin/profile")->group(function() {
            Route::get('/','AdminUsersController@getProfile');
            Route::post('/','AdminUsersController@postProfile');
        });

        # Module Admin Users
        Route::prefix("admin/admin-users")->group(function() {
            Route::get('/','AdminUsersController@getIndex');
            Route::get('list','AdminUsersController@list');
            Route::get('create','AdminUsersController@getCreate');
            Route::post('create','AdminUsersController@postCreate');
            Route::get('update/{id}','AdminUsersController@getUpdate');
            Route::post('update/{id}','AdminUsersController@postUpdate');
            Route::get('delete/{id}','AdminUsersController@getDelete');
        });

        # Module API User Management
        Route::prefix("admin/api-users")->group(function() {
            Route::get('/','ApiUsersController@getIndex');
            Route::get('list','ApiUsersController@list');
            Route::get('change-active/{id}/{status}','ApiUsersController@getChangeActive');
            Route::get('create','ApiUsersController@getCreate');
            Route::post('create','ApiUsersController@postCreate');
            Route::get('update/{id}','ApiUsersController@getUpdate');
            Route::post('update/{id}','ApiUsersController@postUpdate');
            Route::get('delete/{id}','ApiUsersController@getDelete');
        });


        # Module API Log Activity
        Route::prefix("admin/api-log")->group(function() {
            Route::get('/','ApiLogController@getIndex');
            Route::get('list','ApiLogController@list');
            Route::get('download','ApiLogController@getDownload');
            Route::get('payload/{id}','ApiLogController@getPayload');
        });

        # Module API Setting
        Route::prefix("admin/api-setting")->group(function() {
            Route::get('/','ApiSettingController@getIndex');
            Route::post('save','ApiSettingController@postSave');
        });

    });
});


Route::get("/","DefaultController@getIndex");
