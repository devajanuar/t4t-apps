<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// API Routing
Route::middleware([
    'throttle:'.get_setting('rate_limiting').',1',
    \App\Http\Middleware\ApiLogMiddleware::class,
    'bindings'
])
    ->namespace("\App\Http\Controllers\Api")
    ->group(function() {
        Route::post("auth/request-token","Auth@requestToken");
    });
Route::middleware([
    'throttle:'.get_setting('rate_limiting').',1',
    \App\Http\Middleware\ApiMiddleware::class,
    \App\Http\Middleware\ApiLogMiddleware::class,
    'bindings'
])
    ->namespace("\App\Http\Controllers\Api")
    ->group(function() {

        Route::get("version","Version@index");
        Route::post("participant/create","Participant@create");
        Route::post("transaction/assign","Transaction@assign");
        Route::get("transaction/history-transaction","Transaction@historyTransaction");
        Route::get("tree/stock","Tree@stock");

        // Map API
        Route::get("map/syt","Map@getSyt");
        Route::get("map/sot","Map@getSot");
        Route::get("map/lahan","Map@getLahan");
    });