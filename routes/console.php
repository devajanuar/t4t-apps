<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');


Artisan::command("clear_dummy", function() {
    (new \App\Helpers\DummyGenerator($this))->clearDummy();
});

Artisan::command("make:dummy {total}", function() {
    (new \App\Helpers\DummyGenerator($this))->run();
});


Artisan::command('model:make {table_name?} {model_name?}', function() {
    (new \App\Helpers\ModelGenerator($this))->run();
});
