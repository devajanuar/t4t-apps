<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PlantingPlan extends Model
{
	protected $fillable = ['plans_no', 'plans_date', 'village', 'management_unit', 'target_area', 'field_facilitator', 'lahan_no', 'total_trees', 'programs', 'user_id'];

    public function details()
    {
        return $this->hasMany('App\PlantingPlanDetail','plans_no','plans_no');
    }

    public function scopeOfMaxno($query)
    {
        $year=substr(date('Y'), 2);
        $queryMax =  $query->select(DB::raw('SUBSTRING(`plans_no`,1,4) AS kd_max'))
            ->where(DB::raw('YEAR(created_at)'), '=', date('Y'))
            ->orderBy('plans_no', 'asc')
            ->get();

        $arr1 = array();
        if ($queryMax->count() > 0) {
            foreach ($queryMax as $k=>$v)
            {
                $arr1[$k] = (int)$v->kd_max;
            }
            $arr2 = range(1, max($arr1));
            $missing = array_diff($arr2, $arr1);
            if (empty($missing)) {
                $tmp = end($arr1) + 1;
                $kd_fix = sprintf("%04s", $tmp);
            }else{
                $kd_fix = sprintf("%04s", reset($missing));
            }
        }
        else{
            $kd_fix = '0001';
        }

        return $kd_fix.'410'.date('m').$year;
    }
}
