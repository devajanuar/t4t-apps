<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectPhoto extends Model
{
    protected $fillable = ['project_no', 'filename', 'path'];

    public function project()
    {
    	return $this->belongsTo('App\Project', 'project_no', 'project_no');
    }
}
