<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectDetail extends Model
{
	protected $fillable = ['project_no', 'tree_code', 'detail_qty', 'detail_unit', 'detail_total', 'detail_date'];

    public function program()
    {
    	return $this->belongsTo('App\Program', 'program_no', 'program_no');
    }

    public function tree()
    {
    	return $this->belongsTo('App\Tree', 'tree_code', 'tree_code');
    }
}
