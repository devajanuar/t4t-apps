<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;

class Farmer extends Model
{
    protected $fillable = ['farmer_no', 'name', 'birthday', 'religion', 'ethnic', 'origin', 'gender', 'marrital', 'join_date', 'number_family_member', 'ktp_no', 'phone', 'address', 'village', 'kecamatan', 'city', 'province',  'post_code', 'mou_no', 'mu_no', 'group_no', 'main_income', 'side_income', 'active', 'user_id'];

    public function farmergroup()
    {
        return $this->belongsTo('App\FarmerGroup', 'group_no', 'group_no');
    }

    public function management_unit()
    {
    	return $this->belongsTo('App\Managementunit', 'mu_no', 'mu_no');
    }

    public function farmer_stories()
    {
    	return $this->hasMany('App\FarmerStory', 'farmer_no', 'farmer_no');
    }

    public function farmer_testimonial()
    {
        return $this->hasMany('App\FarmerTestimonial', 'farmer_no', 'farmer_no');
    }

    public function pictures()
    {
    	return $this->hasMany('App\FarmerPhoto', 'farmer_no', 'farmer_no');
    }

    public function lahans()
    {
    	return $this->hasMany('App\Lahan', 'farmer_no', 'farmer_no');
    }

    public function scopeMaxno($query)
    {
        $year=substr(date('Y'), 2);
        $queryMax =  $query->select(DB::raw('SUBSTRING(`farmer_no` ,8) AS kd_max'))
            ->where(DB::raw('MONTH(created_at)'), '=', date('m'))
            ->where(DB::raw('YEAR(created_at)'), '=', date('Y'))
            ->orderBy('farmer_no', 'asc')
            ->get();
        $arr1 = array();
        if ($queryMax->count() > 0) {
            foreach ($queryMax as $k=>$v)
            {
                $arr1[$k] = (int)$v->kd_max;
            }
            $arr2 = range(1, max($arr1));
            $missing = array_diff($arr2, $arr1);
            if (empty($missing)) {
                $tmp = end($arr1) + 1;
                $kd_fix = sprintf("%04s", $tmp);
            }else{
                $kd_fix = sprintf("%04s", reset($missing));
            }
        }
        else{
            $kd_fix = '0001';
        }
        return 'FAR'.$year.date('m').$kd_fix;
    }
}
