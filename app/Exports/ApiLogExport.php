<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 3/11/2020
 * Time: 11:59 PM
 */

namespace App\Exports;


use App\Models\ApiRequestLogs;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ApiLogExport implements FromQuery, WithMapping, WithHeadings
{
    use Exportable;

    private $filter_from;
    private $filter_to;

    public function __construct($filter_from, $filter_to)
    {
        $this->filter_from = $filter_from;
        $this->filter_to = $filter_to;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        // TODO: Implement query() method.
        return DB::table(ApiRequestLogs::$table)
            ->whereDate("created_at",">=", $this->filter_from)
            ->whereDate("created_at","<=", $this->filter_to)
            ->orderBy("id","desc");
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        $row = new ApiRequestLogs($row);
        // TODO: Implement map() method.
        return [
            $row->created_at,
            $row->ip_address,
            $row->user_agent,
            $row->url,
            $row->response_code,
            $row->payload_data
        ];
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        // TODO: Implement headings() method.
        return [
          "Request Time",
          "IP Address",
          "User Agent",
          "URL Request",
          "Response Code",
          "Payload"
        ];
    }
}