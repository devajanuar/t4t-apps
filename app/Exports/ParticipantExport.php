<?php

namespace App\Exports;

use App\Participant;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ParticipantExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Participant::all();
    }

    public function headings(): array
    {
    	return [
    		'id',
    		'participant_no',
    		'category',
    		'first_name',
    		'last_name',
    		'address1',
    		'address2',
    		'company',
    		'city',
    		'state',
    		'postal_code',
    		'country',
    		'email',
    		'website',
    		'phone',
    		'join_date',
    		'active',
    		'user_id',
    		'participants_photo',
    		'comment',
    		'source_of_contact',
    		'created_at',
    		'updated_at',
    	];
    }
}
