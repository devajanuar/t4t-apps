<?php

namespace App\Exports;

use App\Project;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProjectExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Project::all();
    }

    public function headings(): array
    {
    	return [
    		'id',
    		'project_no',
    		'project_category',
    		'project_name',
    		'project_date',
    		'end_project',
    		'project_description',
    		'location',
    		'total_trees',
    		'co2_capture',
    		'donors',
    		'mu_no',
    		'created_at',
    		'updated_at',
    	];
    }
}
