<?php

namespace App\Exports;

use App\FarmerGroup;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class FarmerGroupExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return FarmerGroup::all();
    }

    public function headings(): array
    {
    	return [
    		'id',
    		'group_no',
    		'name',
    		'village',
    		'mu_no',
    		'target_area',
    		'mou_no',
            'phone',
            'pic',
            'organization_structure',
    		'active',
    		'user_id',
    		'created_at',
    		'updated_at'
    	];
    }
}
