<?php

namespace App\Exports;

use App\Lahan;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class LahanExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Lahan::all();
    }

    public function headings(): array
    {
    	return [
    		'id',
    		'lahan_no',
    		'document_no',
    		'land_area',
    		'planting_area',
    		'longitude',
    		'latitude',
    		'coordinate',
    		'polygon',
    		'village',
    		'kecamatan',
    		'city',
    		'province',
    		'description',
    		'elevation',
    		'soil_type',
    		'current_corps',
    		'active',
    		'mu_no',
    		'user_id',
    		'created_at',
    		'updated_at',
    	];
    }
}
