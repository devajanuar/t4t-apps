<?php

namespace App\Exports;

use App\Farmer;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class FarmerExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Farmer::all();
    }

    public function headings(): array
    {
    	return [
    		'id',
    		'farmer_no',
    		'name',
    		'birthday',
    		'religion',
    		'ethnic',
    		'origin',
    		'gender',
    		'join_date',
    		'number_family_member',
    		'ktp_no',
    		'phone',
    		'address',
    		'village',
    		'kecamatan',
    		'city',
    		'province',
    		'post_code',
    		'mu_no',
    		'group_no',
    		'mou_no',
    		'main_income',
    		'side_income',
    		'active',
    		'user_id',
    		'created_at',
    		'updated_at'
    	];
    }
}
