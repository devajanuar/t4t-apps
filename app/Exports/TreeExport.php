<?php

namespace App\Exports;

use App\Tree;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class TreeExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Tree::all();
    }

    public function headings(): array
    {
    	return [
    		'id',
    		'tree_code',
    		'tree_name',
    		'scientific_name',
    		'english_name',
    		'common_name',
    		'short_information',
    		'description',
    		'tree_category',
    		'product_list',
    		'estimate_income',
    		'co2_capture',
    		'created_at',
    		'updated_at',
    	];
    }
}
