<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Project extends Model
{
    protected $fillable = ['project_no', 'type_plant', 'year', 'location', 'total_trees', 'description', 'program_note', 'total_qty', 'approvement', 'active', 'user_id'];

    public function details()
    {
        return $this->hasMany('App\ProgramDetail','project_no','project_no');
    }

    public function management_unit()
    {
    	return $this->belongsTo('App\ManagementUnit', 'mu_no', 'mu_no');
    }

    public function projectphoto()
    {
    	return $this->hasMany('App\ProjectPhoto', 'project_no', 'project_no');
    }

    public function scopeMaxno($query)
    {
        $year=substr(date('Y'), 2);
        $queryMax =  $query->select(DB::raw('SUBSTRING(`project_no` ,8) AS kd_max'))
            ->where(DB::raw('MONTH(created_at)'), '=', date('m'))
            ->where(DB::raw('YEAR(created_at)'), '=', date('Y'))
            ->orderBy('project_no', 'asc')
            ->get();
        $arr1 = array();
        if ($queryMax->count() > 0) {
            foreach ($queryMax as $k=>$v)
            {
                $arr1[$k] = (int)$v->kd_max;
            }
            $arr2 = range(1, max($arr1));
            $missing = array_diff($arr2, $arr1);
            if (empty($missing)) {
                $tmp = end($arr1) + 1;
                $kd_fix = sprintf("%04s", $tmp);
            }else{
                $kd_fix = sprintf("%04s", reset($missing));
            }
        }
        else{
            $kd_fix = '0001';
        }
        return 'PRO'.$year.date('m').$kd_fix;
    }
}
