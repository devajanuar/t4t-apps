<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LahanPhoto extends Model
{
    protected $fillable = ['lahan_no', 'filename', 'path'];

    public function lahan()
    {
    	return $this->belongsTo('App\Lahan', 'lahan_no', 'lahan_no');
    }
}
