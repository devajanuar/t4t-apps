<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $fillable = ['province_code', 'name'];

    public function kabupatens()
    {
    	return $this->hasMany('App\Kabupaten', 'kabupaten_no', 'kabupaten_no');
    }
}
