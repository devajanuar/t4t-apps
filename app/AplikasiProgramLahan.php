<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AplikasiProgramLahan extends Model
{
    protected $fillable = ['year', 'farmer_no', 'nik', 'address', 'lahan_no', 'document_no', 'type_program', 'lahan_status', 'luas_lahan', 'tutupan_lahan', 'luas_tanam', 'tree1', 'tree2', 'tree3', 'tree4', 'tree5', 'description'];

    public function farmers()
    {
    	return $this->belongsTo('App\Farmer', 'farmer_no', 'farmer_no');
    }

    public function lahans()
    {
    	return $this->belongsTo('App\Lahan', 'lahan_no', 'lahan_no');
    }

    public function scopeMaxno($query)
    {
        $year=substr(date('Y'), 2);
        $queryMax =  $query->select(DB::raw('SUBSTRING(`apl_no` ,8) AS kd_max'))
            ->where(DB::raw('MONTH(created_at)'), '=', date('m'))
            ->where(DB::raw('YEAR(created_at)'), '=', date('Y'))
            ->orderBy('apl_no', 'asc')
            ->get();
        $arr1 = array();
        if ($queryMax->count() > 0) {
            foreach ($queryMax as $k=>$v)
            {
                $arr1[$k] = (int)$v->kd_max;
            }
            $arr2 = range(1, max($arr1));
            $missing = array_diff($arr2, $arr1);
            if (empty($missing)) {
                $tmp = end($arr1) + 1;
                $kd_fix = sprintf("%04s", $tmp);
            }else{
                $kd_fix = sprintf("%04s", reset($missing));
            }
        }
        else{
            $kd_fix = '0001';
        }
        return 'FAP'.$year.$kd_fix;
    }
}
