<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kabupaten extends Model
{
    protected $fillable = ['kabupaten_no', 'name', 'province_code'];

    public function provinces()
    {
    	return $this->belongsTo('App\Province', 'province_code', 'province_code');
    }

    public function kecamatans()
    {
    	return $this->hasMany('App\Kecamatan', 'kode_kecamatan', 'kode_kecamatan');
    }
}
