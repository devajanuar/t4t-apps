<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TargetArea extends Model
{
    protected $fillable = ['area_code', 'name', 'mu_no', 'active'];

    public function manunit()
    {
    	return $this->belongsTo('App\Managementunit', 'mu_no', 'id');
    }
}
