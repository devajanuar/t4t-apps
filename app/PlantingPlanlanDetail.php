<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlantingPlanlanDetail extends Model
{
	protected $fillable = ['plans_no', 'tree_code', 'amount', 'unit', 'plans_date'];

    public function planning()
    {
    	return $this->belongsTo('App\PlantingPlan', 'plans_no', 'plans_no');
    }

    public function tree()
    {
    	return $this->belongsTo('App\Tree', 'tree_code', 'tree_code');
    }
}
