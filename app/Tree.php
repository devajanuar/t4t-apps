<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tree extends Model
{
    protected $fillable = ['tree_code', 'tree_name', 'scientific_name', 'english_name', 'common_name', 'short_information', 'description', 'tree_category', 'product_list', 'estimate_income', 'co2_capture'];

    public function tree_photos()
    {
    	return $this->hasMany('App/TreePhoto', 'tree_code', 'tree_code');
    }

    public function programs()
    {
        return $this->hasMany('App/ProgramDetail', 'tree_code', 'tree_code');
    }

    public function scopeMaxno($query)
    {
        $queryMax =  $query->select(DB::raw('SUBSTRING(`tree_code`, 4) AS kd_max'))
            ->orderBy('tree_code', 'asc')
            ->get();
        
        $arr1 = array();
        if ($queryMax->count() > 0) {
            foreach ($queryMax as $k=>$v)
            {
                $arr1[$k] = (int)$v->kd_max;
            }
            $arr2 = range(1, max($arr1));
            $missing = array_diff($arr2, $arr1);
            if (empty($missing)) {
                $tmp = end($arr1) + 1;
                $kd_fix = sprintf("%04s", $tmp);
            }else{
                $kd_fix = sprintf("%04s", reset($missing));
            }
        }
        else{
            $kd_fix = '0001';
        }

        return 'T'.$kd_fix;
    }
}
