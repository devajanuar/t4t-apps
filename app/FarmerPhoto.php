<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FarmerPhoto extends Model
{
    protected $fillable = ['farmer_no', 'filename', 'path'];

    public function farmer()
    {
    	return $this->belongsTo('App\Farmer', 'farmer_no', 'farmer_no');
    }
}

