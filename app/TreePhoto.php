<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TreePhoto extends Model
{
    protected $fillable = ['tree_code', 'filename', 'path'];

    public function trees()
    {
    	return $this->belongsTo('App/Tree', 'tree_code', 'tree_code');
    }
}
