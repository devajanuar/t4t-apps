<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    protected $fillable = ['kode_kecamatan', 'name', 'kabupaten_no'];

    public function kabupatens()
    {
    	return $this->belongsTo('App\Kabupaten', 'kabupaten_no', 'kabupaten_no');
    }

    public function desas()
    {
    	return $this->hasMany('App\Desa', 'kode_desa', 'kode_desa');
    }
}
