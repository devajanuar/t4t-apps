<?php

namespace App\Imports;

use App\Project;
use Maatwebsite\Excel\Concerns\ToModel;

class ProjectImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Project([
            'id'=> $row[0],
            'project_no'=> $row[1],
            'project_category'=> $row[2],
            'project_name'=> $row[3],
            'project_date'=> $row[4],
            'end_project'=> $row[5],
            'project_description'=> $row[6],
            'location'=> $row[7],
            'total_trees'=> $row[8],
            'co2_capture'=> $row[9],
            'donors'=> $row[10],
            'mu_no'=> $row[11],
            'created_at'=> $row[12],
            'updated_at'=> $row[13],
        ]);
    }
}
