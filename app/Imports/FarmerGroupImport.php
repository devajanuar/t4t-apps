<?php

namespace App\Imports;

use App\FarmerGroup;
use Maatwebsite\Excel\Concerns\ToModel;

class FarmerGroupImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new FarmerGroup([
            'id' => $row[0],
            'group_no' => $row[1],
            'name' => $row[2],
            'village' => $row[3],
            'mu_no' => $row[4],
            'target_area' => $row[5],
            'mou_no' => $row[6],
            'phone' => $row[7],
            'pic' => $row[8],
            'organization_structure' => $row[9],
            'active' => $row[10],
            'user_id' => $row[11],
            'created_at' => $row[12],
            'updated_at' => $row[13]
        ]);
    }
}
