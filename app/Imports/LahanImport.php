<?php

namespace App\Imports;

use App\Lahan;
use Maatwebsite\Excel\Concerns\ToModel;

class LahanImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Lahan([
            'id'=> $row[0],
            'lahan_no'=> $row[1],
            'document_no'=> $row[2],
            'land_area'=> $row[3],
            'planting_area'=> $row[4],
            'longitude'=> $row[5],
            'latitude'=> $row[6],
            'coordinate'=> $row[7],
            'polygon'=> $row[8],
            'village'=> $row[9],
            'kecamatan'=> $row[10],
            'city'=> $row[11],
            'province'=> $row[12],
            'description'=> $row[13],
            'elevation'=> $row[14],
            'soil_type'=> $row[15],
            'current_corps'=> $row[16],
            'active'=> $row[17],
            'mu_no'=> $row[18],
            'user_id'=> $row[19],
            'created_at'=> $row[20],
            'updated_at'=> $row[21],
        ]);
    }
}
