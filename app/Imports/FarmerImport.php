<?php

namespace App\Imports;

use App\Farmer;
use Maatwebsite\Excel\Concerns\ToModel;

class FarmerImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Farmer([
             'id'  => $row[0],
             'farmer_no'   => $row[1],
             'name'   => $row[2],
             'birthday'    => $row[3],
             'religion'  => $row[4],
             'ethnic'   => $row[5],
             'origin'  => $row[6],
             'gender'   => $row[7],
             'join_date'   => $row[8],
             'number_family_member'    => $row[9],
             'ktp_no'  => $row[10],
             'phone'   => $row[11],         
             'address'  => $row[12],
             'village'   => $row[13],
             'kecamatan'   => $row[14],
             'city'    => $row[15],
             'province'  => $row[16],
             'post_code'   => $row[17],
             'mu_no'  => $row[18],
             'group_no'   => $row[19],
             'mou_no'   => $row[20],
             'main_income'    => $row[21],
             'side_income'  => $row[22],
             'active'   => $row[23],
             'user_id' => $row[24],
             'created_at' => $row[25],
             'updated_at' => $row[26]
        ]);
    }
}
