<?php

namespace App\Imports;

use App\Participant;
use Maatwebsite\Excel\Concerns\ToModel;

class ParticipantImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Participant([
            'id' => $row[0],
            'participant_no' => $row[1],
            'category' => $row[2],
            'first_name' => $row[3],
            'last_name' => $row[4],
            'address1' => $row[5],
            'address2' => $row[6],
            'company' => $row[7],
            'city' => $row[8],
            'state' => $row[9],
            'postal_code' => $row[10],
            'country' => $row[11],
            'email' => $row[12],
            'website' => $row[13],
            'phone' => $row[14],
            'join_date' => $row[15],
            'active' => $row[16],
            'user_id' => $row[17],
            'participants_photo' => $row[18],
            'comment' => $row[19],
            'source_of_contact' => $row[20],
            'created_at' => $row[21],
            'updated_at' => $row[22],
        ]);
    }
}
