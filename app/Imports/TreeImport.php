<?php

namespace App\Imports;

use App\Tree;
use Maatwebsite\Excel\Concerns\ToModel;

class TreeImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Tree([
            'id' => $row[0],
            'tree_code' => $row[1],
            'tree_name' => $row[2],
            'scientific_name' => $row[3],
            'english_name' => $row[4],
            'common_name' => $row[5],
            'short_information' => $row[6],
            'description' => $row[7],
            'tree_category' => $row[8],
            'product_list' => $row[9],
            'estimate_income' => $row[10],
            'co2_capture' => $row[11],
            'created_at' => $row[12],
            'updated_at' => $row[13],
        ]);
    }
}
