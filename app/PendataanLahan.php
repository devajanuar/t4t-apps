<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PendataanLahan extends Model
{
    protected $fillable = ['pendataan_no', 'mu_no', 'target_area', 'village', 'field_facilitator', 'user_id', 'active', 'status'];

	public function details()
    {
        return $this->hasMany('App\PendataanLahanDetail','pendataan_no','pendataan_no');
    }

    public function scopeOfMaxno($query)
    {
        $year=substr(date('Y'), 2);
        $queryMax =  $query->select(DB::raw('SUBSTRING(`pendataan_no`,1,4) AS kd_max'))
            ->where(DB::raw('YEAR(created_at)'), '=', date('Y'))
            ->orderBy('pendataan_no', 'asc')
            ->get();

        $arr1 = array();
        if ($queryMax->count() > 0) {
            foreach ($queryMax as $k=>$v)
            {
                $arr1[$k] = (int)$v->kd_max;
            }
            $arr2 = range(1, max($arr1));
            $missing = array_diff($arr2, $arr1);
            if (empty($missing)) {
                $tmp = end($arr1) + 1;
                $kd_fix = sprintf("%04s", $tmp);
            }else{
                $kd_fix = sprintf("%04s", reset($missing));
            }
        }
        else{
            $kd_fix = '0001';
        }

        return $kd_fix.'/PL/'.date('m').'/'.$year;
    }
}
