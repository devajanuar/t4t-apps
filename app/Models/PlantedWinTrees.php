<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/25/2020
 * Time: 11:04 PM
 */

namespace App\Models;


class PlantedWinTrees extends Model
{
    public static $table = "planted_win_trees";

    public $id;
	public $created_at;
	public $updated_at;
	public $planted_date;
	public $lahan_no;
	public $win_number;
	public $tree_code;
	public $latitude;
	public $longitude;
	public $quantity;

    /**
     * @return Trees|null
     */
	public function tree() {
	    return Trees::findBy("tree_code", $this->tree_code);
    }

    /**
     * @return Lahans|null
     */
    public function lahan() {
        return Lahans::findBy("lahan_no", $this->lahan_no);
    }

    /**
     * @return TransactionTrees|null
     */
    public function win() {
        return TransactionTrees::findBy("win_number", $this->win_number);
    }
}