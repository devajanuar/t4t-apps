<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/25/2020
 * Time: 11:04 PM
 */

namespace App\Models;


class ProjectDetails extends Model
{
    public static $table = "project_details";

    public $id;
	public $project_no;
	public $lahan_no;

    /**
     * @return Projects|null
     */
    public function project() {
        return Projects::findBy("project_no",$this->project_no);
    }
}