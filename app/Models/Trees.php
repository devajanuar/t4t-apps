<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/25/2020
 * Time: 11:04 PM
 */

namespace App\Models;


use Illuminate\Support\Facades\DB;

class Trees extends Model
{
    public static $table = "trees";

    public $id;
	public $tree_code;
	public $tree_name;
	public $scientific_name;
	public $english_name;
	public $common_name;
	public $short_information;
	public $description;
	public $tree_category;
	public $product_list;
	public $estimate_income;
	public $co2_capture;
	public $created_at;
	public $updated_at;


	public static function findRandom() {
	    $data = DB::table(static::$table)->orderByRaw("RAND()")->take(1)->first();
	    return $data;
    }

}