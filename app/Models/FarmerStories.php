<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/25/2020
 * Time: 11:04 PM
 */

namespace App\Models;


use Illuminate\Database\Query\Builder;

class FarmerStories extends Model
{
    public static $table = "farmer_stories";

    public $id;
	public $farmer_no;
	public $story_no;
	public $story_title;
	public $year;
	public $story_description;
	public $active;
	public $created_at;
	public $updated_at;

    /**
     * @param $farmer_no
     * @return FarmerStories[]
     */
	public static function findAllByFarmerNo($farmer_no) {
	    $result = static::allWhere(function(Builder $query) use ($farmer_no) {
	        $query->where("farmer_no", $farmer_no);
	        $query->where("active",1);
	        return $query;
        });
	    return $result;
    }
}