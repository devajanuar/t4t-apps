<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/25/2020
 * Time: 11:04 PM
 */

namespace App\Models;


class ApiUserPermissions extends Model
{
    public static $table = "api_user_permissions";

    public $id;
	public $api_users_id;
	public $permission_name;
	public $is_active;
	
    public static function findAllByUser($api_users_id) {
        return DB(static::$table)->where("api_users_id",$api_users_id)->get();
    }
}