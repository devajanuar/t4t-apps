<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/25/2020
 * Time: 11:04 PM
 */

namespace App\Models;


class LahanPhotos extends Model
{
    public static $table = "lahan_photos";

    public $id;
	public $lahan_no;
	public $filename;
	public $path;
	public $created_at;
	public $updated_at;
	
    public static function findAllByLahan($lahan_no) {
        $result = static::findAllBy("lahan_no",$lahan_no);
        $final = [];
        foreach($result as $item) {
            $final[] = [
              'lahan_no'=> $item->lahan_no,
              'filename'=> asset($item->path.'/'.$item->filename)
            ];
        }
        return $final;
    }
}