<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/25/2020
 * Time: 11:04 PM
 */

namespace App\Models;


class ApiSettings extends Model
{
    public static $table = "api_settings";

    public $id;
	public $created_at;
	public $updated_at;
	public $setting_name;
	public $setting_value;
	

}