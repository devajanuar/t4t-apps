<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/25/2020
 * Time: 11:04 PM
 */

namespace App\Models;


class PasswordResets extends Model
{
    public static $table = "password_resets";

    public $email;
	public $token;
	public $created_at;
	

}