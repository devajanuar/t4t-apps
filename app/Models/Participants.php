<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/25/2020
 * Time: 11:04 PM
 */

namespace App\Models;


use Illuminate\Support\Facades\DB;

class Participants extends Model
{
    public static $table = "participants";

    public $id;
	public $participant_no;
	public $category;
	public $first_name;
	public $last_name;
	public $address1;
	public $address2;
	public $company;
	public $city;
	public $state;
	public $postal_code;
	public $country;
	public $email;
	public $website;
	public $phone;
	public $join_date;
	public $active;
	public $user_id;
	public $participants_photo;
	public $comment;
	public $source_of_contact;
	public $created_at;
	public $updated_at;
	

	public static function generateParticipantNo()
    {
        $max = DB::table(static::$table)->max("id") + 1;
        return 'EU'.str_pad($max, 5, 0, STR_PAD_LEFT);
    }
}