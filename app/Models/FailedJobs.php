<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/25/2020
 * Time: 11:04 PM
 */

namespace App\Models;


class FailedJobs extends Model
{
    public static $table = "failed_jobs";

    public $id;
	public $connection;
	public $queue;
	public $payload;
	public $exception;
	public $failed_at;
	

}