<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/25/2020
 * Time: 11:04 PM
 */

namespace App\Models;


class TreePhotos extends Model
{
    public static $table = "tree_photos";

    public $id;
	public $tree_code;
	public $filename;
	public $path;
	public $created_at;
	public $updated_at;

    public static function findAllByTreeCode($tree_code) {
        $result = static::findAllBy("tree_code",$tree_code);
        $final = [];
        foreach($result as $item) {
            $final[] = [
                'tree_code'=> $item->tree_code,
                'filename'=> asset($item->path.'/'.$item->filename)
            ];
        }
        return $final;
    }
}