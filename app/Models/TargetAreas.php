<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/25/2020
 * Time: 11:04 PM
 */

namespace App\Models;


class TargetAreas extends Model
{
    public static $table = "target_areas";

    public $id;
	public $area_code;
	public $name;
	public $mu_no;
	public $active;
	public $created_at;
	public $updated_at;
	

}