<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/25/2020
 * Time: 11:04 PM
 */

namespace App\Models;


use Illuminate\Support\Str;

class TransactionTrees extends Model
{
    public static $table = "transaction_trees";

    public $id;
	public $created_at;
	public $updated_at;
	public $transaction_at;
	public $win_number;
	public $participant_no;
	public $tree_code;
	public $quantity;

    /**
     * @return Participants|null
     */
	public function participant() {
	    return Participants::findBy("participant_no", $this->participant_no);
    }



	public static function generateWinNumber() {
	    return Str::random(6);
    }

}