<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/25/2020
 * Time: 11:04 PM
 */

namespace App\Models;


class Projects extends Model
{
    public static $table = "projects";

    public $id;
	public $project_no;
	public $project_category;
	public $project_name;
	public $project_date;
	public $end_project;
	public $project_description;
	public $location;
	public $total_trees;
	public $co2_capture;
	public $donors;
	public $mu_no;
	public $created_at;
	public $updated_at;
	

}