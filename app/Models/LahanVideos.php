<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/25/2020
 * Time: 11:04 PM
 */

namespace App\Models;


class LahanVideos extends Model
{
    public static $table = "lahan_videos";

    public $id;
	public $lahan_no;
	public $filename;
	public $extension;
	public $path;
	public $mime_type;
	public $created_at;
	public $updated_at;
	

}