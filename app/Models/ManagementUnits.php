<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/25/2020
 * Time: 11:04 PM
 */

namespace App\Models;


class ManagementUnits extends Model
{
    public static $table = "management_units";

    public $id;
	public $mu_no;
	public $name;
	public $active;
	public $created_at;
	public $updated_at;
	

}