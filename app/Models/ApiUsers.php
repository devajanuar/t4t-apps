<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/25/2020
 * Time: 11:04 PM
 */

namespace App\Models;


class ApiUsers extends Model
{
    public static $table = "api_users";

    public $id;
	public $created_at;
	public $updated_at;
	public $name;
	public $username;
	public $password;
	public $is_active;
	

}