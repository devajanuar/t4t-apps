<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/25/2020
 * Time: 11:04 PM
 */

namespace App\Models;


class Lahans extends Model
{
    public static $table = "lahans";

    public $id;
	public $lahan_no;
	public $document_no;
	public $land_area;
	public $planting_area;
	public $longitude;
	public $latitude;
	public $coordinate;
	public $polygon;
	public $village;
	public $kecamatan;
	public $city;
	public $province;
	public $description;
	public $elevation;
	public $soil_type;
	public $current_crops;
	public $active;
	public $farmer_no;
	public $mu_no;
	public $user_id;
	public $created_at;
	public $updated_at;

	public function getArray() {
	    return $this->toArray(['id','farmer_no','mu_no','user_id','created_at','updated_at']);
    }

    /**
     * @return Users|null
     */
	public function user() {
	    return Users::findById($this->user_id);
    }

    /**
     * @return Farmers|null
     */
    public function farmer() {
	    return Farmers::findBy("farmer_no", $this->farmer_no);
    }

}