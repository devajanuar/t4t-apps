<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/25/2020
 * Time: 10:10 PM
 */

namespace App\Models;

use Illuminate\Database\Query\Builder;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Model extends ModelAbstract
{

    public function __construct($row = null)
    {
        if($row) {
            foreach($row as $key => $value) {
                $this->{$key} = $value;
            }
        }
    }

    /**
     * @param callable $query
     * @return int
     */
    public static function deleteWhere(callable $query) {
        $q = DB::table(static::$table);
        $d = call_user_func($query, $q);
        /** @var $d Builder */
        return $d->delete();
    }

    /**
     * @param $id
     * @return int
     */
    public static function deleteById($id) {
        return DB::table(static::$table)->where(static::getPrimaryKey(),$id)->delete();
    }

    /**
     * @throws \Exception
     */
    public function save() {
        try {
            $data = [];
            foreach($this as $key => $value) {
                $data[$key] = $value;
            }

            if( $this->{static::getPrimaryKey()} ) {

                if(isset($data['updated_at']) && empty($data['updated_at'])) {
                    $data['updated_at'] = date('Y-m-d H:i:s');
                }

                DB::table(static::$table)->where(static::getPrimaryKey(),$this->{static::getPrimaryKey()})->update($data);
            } else {

                if(isset($data['created_at']) && empty($data['created_at'])) {
                    $data['created_at'] = date('Y-m-d H:i:s');
                }

                $last_id = DB::table(static::$table)->insertGetId($data);
                $this->{static::getPrimaryKey()} = $last_id;
            }
        } catch (QueryException $e) {
            Log::error($e);
            throw new \Exception($e);
        } catch (\Exception $e) {
            Log::error($e);
            throw new \Exception($e);
        }
    }

    /**
     * @return null|string
     */
    public static function getPrimaryKey()
    {
        $pk = DB::getDoctrineSchemaManager()->listTableDetails(static::$table)->getPrimaryKey();
        if(!$pk) {
            return null;
        }
        return $pk->getColumns()[0];
    }

    /**
     * @param string $id
     * @return static|null
     */
    public static function findById($id) {
        $row = DB::table(static::$table)->where(static::getPrimaryKey(),$id)->first();
        if($row) {
            return (new static($row));
        } else {
            return null;
        }
    }


    /**
     * @param $key
     * @return mixed
     */
    public function getValue($key) {
        return $this->{$key};
    }

    /**
     * @param $key
     * @param $value
     * @return static|null
     */
    public static function findBy($key, $value) {
        $row = DB::table(static::$table)->where($key,$value)->first();
        if($row) {
            return (new static($row));
        } else {
            return null;
        }
    }

    /**
     * @param $key
     * @param $value
     * @return static[]
     */
    public static function findAllBy($key, $value) {
        $row = DB::table(static::$table)->where($key,$value)->get();
        if($row) {
            $result = [];
            foreach($row as $item) {
                $result[] = new static($item);
            }
            return $result;
        } else {
            return [];
        }
    }


    /**
     * @param array $except
     * @return array
     */
    public function toArray($except = []) {
        $result = [];
        foreach($this as $key=>$val) {
            if($except && in_array($key, $except)) continue;
            $result[$key] = $val;
        }
        return $result;
    }

    /**
     * @return static[]
     */
    public static function all() {
        $result = DB::table(static::$table)->get();

        $final = [];

        if($result) {
            foreach($result as $row) {
                $final[] = (new static($row));
            }
        } else {
            $final = null;
        }

        return $final;
    }

    /**
     * @param callable $query
     * @return static[]
     */
    public static function allWhere(callable $query) {
        $result = DB::table(static::$table);
        $result = call_user_func($query, $result);
        $result = $result->get();

        $final = [];

        if($result) {
            foreach($result as $row) {
                $final[] = (new static($row));
            }
        } else {
            $final = null;
        }

        return $final;
    }

}