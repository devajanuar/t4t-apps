<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/25/2020
 * Time: 11:04 PM
 */

namespace App\Models;


class FarmerPhotos extends Model
{
    public static $table = "farmer_photos";

    public $id;
	public $farmer_no;
	public $filename;
	public $path;
	public $created_at;
	public $updated_at;

    public static function findAllByFarmerNo($farmer_no) {
        $result = static::findAllBy("farmer_no",$farmer_no);
        $final = [];
        foreach($result as $item) {
            $final[] = [
                'farmer_no'=> $item->farmer_no,
                'filename'=> asset($item->path.'/'.$item->filename)
            ];
        }
        return $final;
    }

}