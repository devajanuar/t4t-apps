<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/25/2020
 * Time: 11:04 PM
 */

namespace App\Models;


class ApiRequestLogs extends Model
{
    public static $table = "api_request_logs";

    public $id;
	public $created_at;
	public $updated_at;
	public $api_tokens_id;
	public $user_agent;
	public $ip_address;
	public $response_code;
	public $url;
	public $payload_data;

	public static function saveLog($response_code = 200) {
        $log = new static();
        $log->created_at = date('Y-m-d H:i:s');
        $log->api_tokens_id = api_auth()->token()->id;
        $log->user_agent = request()->userAgent();
        $log->ip_address = request()->ip();
        $payload = [];
        $payload['header'] = collect(request()->header())->transform(function ($item) {
            return $item[0];
        })->toArray();
        $payload['form'] = request()->all();
        $log->payload_data = json_encode($payload);
        $log->url = request()->url();
        $log->response_code = $response_code;
        $log->save();
    }


    /**
     * @return ApiTokenLogs|null
     */
	public function apiTokens() {
	    return ApiTokenLogs::findById($this->api_tokens_id);
    }
}