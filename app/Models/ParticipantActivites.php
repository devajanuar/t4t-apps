<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/25/2020
 * Time: 11:04 PM
 */

namespace App\Models;


class ParticipantActivites extends Model
{
    public static $table = "participant_activites";

    public $id;
	public $comment;
	public $donation_amount;
	public $add_newslater;
	public $recurring;
	public $payment_gateway;
	public $donation_impact;
	public $IP;
	public $giftee_firstname;
	public $giftee_lastname;
	public $giftee_email;
	public $created_at;
	public $updated_at;
	

}