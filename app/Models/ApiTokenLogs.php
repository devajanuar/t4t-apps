<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/25/2020
 * Time: 11:04 PM
 */

namespace App\Models;


class ApiTokenLogs extends Model
{
    public static $table = "api_token_logs";

    public $id;
	public $created_at;
	public $updated_at;
	public $api_users_id;
	public $user_agent;
	public $ip_address;
	public $token;
	public $expired_at;

    /**
     * @return ApiUsers|null
     */
	public function apiUser() {
	    return ApiUsers::findById($this->api_users_id);
    }

}