<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/25/2020
 * Time: 11:04 PM
 */

namespace App\Models;


class ProjectPhotos extends Model
{
    public static $table = "project_photos";

    public $id;
	public $project_no;
	public $filename;
	public $path;
	public $created_at;
	public $updated_at;


    /**
     * @return Projects|null
     */
    public function project() {
        return Projects::findBy("project_no",$this->project_no);
    }
}