<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/25/2020
 * Time: 11:04 PM
 */

namespace App\Models;


use Illuminate\Database\Query\Builder;

class FarmerTestimonial extends Model
{
    public static $table = "farmer_testimonial";

    public $id;
	public $farmer_no;
	public $testimonial;
	public $year;
	public $created_at;
	public $updated_at;

    /**
     * @param $farmer_no
     * @return FarmerTestimonial[]
     */
    public static function findAllByFarmerNo($farmer_no) {
        $result = static::allWhere(function(Builder $query) use ($farmer_no) {
            $query->where("farmer_no", $farmer_no);
            return $query;
        });
        return $result;
    }

}