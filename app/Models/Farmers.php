<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/25/2020
 * Time: 11:04 PM
 */

namespace App\Models;


class Farmers extends Model
{
    public static $table = "farmers";

    public $id;
	public $farmer_no;
	public $name;
	public $birthday;
	public $religion;
	public $ethnic;
	public $origin;
	public $gender;
	public $join_date;
	public $family_size;
	public $ktp_no;
	public $phone;
	public $address;
	public $village;
	public $kecamatan;
	public $city;
	public $province;
	public $post_code;
	public $mu_no;
	public $group_no;
	public $mou_no;
	public $main_income;
	public $side_income;
	public $active;
	public $user_id;
	public $created_at;
	public $updated_at;
	

}