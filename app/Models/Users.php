<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/25/2020
 * Time: 11:04 PM
 */

namespace App\Models;


class Users extends Model
{
    public static $table = "users";

    public $id;
	public $name;
	public $email;
	public $email_verified_at;
	public $participant_id;
	public $password;
	public $active;
	public $remember_token;
	public $created_at;
	public $updated_at;

    /**
     * @return Participants|null
     */
	public function participant() {
	    return Participants::findById($this->participant_id);
    }
	

}