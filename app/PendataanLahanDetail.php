<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PendataanLahanDetail extends Model
{
	protected $fillable = ['pendataan_no', 'lahan_no', 'luas_lahan', 'tutupan_lahan', 'luas_tanam', 'condition', 'description'];
	
    public function pendataan()
    {
    	return $this->belongsTo('App\PendataanLahan', 'pendataan_no', 'pendataan_no');
    }

    public function lahan()
    {
    	return $this->belongsTo('App\Lahan', 'lahan_no', 'lahan_no');
    }
}
