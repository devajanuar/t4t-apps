<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FarmerGroup extends Model
{
    protected $fillable = ['group_no', 'name', 'village', 'mu_no', 'target_area', 'mou_no', 'phone', 'pic', 'organization_structure', 'active', 'user_id'];

    public function farmer()
    {
    	return $this->hasMany('App\Farmer', 'farmer_no', 'farmer_no');
    }

    public function management_unit()
    {
    	return $this->belongsTo('App\ManagementUnit', 'mu_no', 'mu_no');
    }
}
