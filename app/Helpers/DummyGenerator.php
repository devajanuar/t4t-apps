<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 3/13/2020
 * Time: 10:17 AM
 */

namespace App\Helpers;

use App\Models\FarmerPhotos;
use App\Models\Farmers;
use App\Models\FarmerStories;
use App\Models\FarmerTestimonial;
use App\Models\Lahans;
use App\Models\Participants;
use App\Models\PlantedWinTrees;
use App\Models\ProjectDetails;
use App\Models\Projects;
use App\Models\TransactionTrees;
use App\Models\TreePhotos;
use App\Models\Trees;
use Faker\Factory;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DummyGenerator
{
    private $console;

    public function __construct(Command $console)
    {
        $this->console = $console;
    }

    public function clearDummy() {
        DB::select(DB::raw("
        TRUNCATE `farmers`;
        TRUNCATE `farmer_groups`;
        TRUNCATE `farmer_photos`;
        TRUNCATE `farmer_stories`;
        TRUNCATE `farmer_testimonial`;
        TRUNCATE `lahans`;
        TRUNCATE `lahan_photos`;
        TRUNCATE `lahan_videos`;
        TRUNCATE `participants`;
        TRUNCATE `participant_activites`;
        TRUNCATE `participant_certificates`;
        TRUNCATE `planted_win_trees`;
        TRUNCATE `projects`;
        TRUNCATE `project_details`;
        TRUNCATE `project_photos`;
        TRUNCATE `transaction_trees`;
        TRUNCATE `trees`;
        TRUNCATE `tree_photos`;
        "));
        $this->console->info("Clear dummy done!");
    }


    public function run() {
        $total = $this->console->argument("total")?:20;
        $faker = Factory::create("id_ID");

        // Dummy Pohon
        for($i=1;$i<=10;$i++) {
            $tree = new Trees();
            $tree->created_at = now();
            $tree->description = $faker->text(90);
            $tree->tree_code = strtoupper(Str::random(8));
            $tree->tree_category = "-";
            $tree->tree_name = ucwords("Pohon ".$faker->words(3, true));
            $tree->scientific_name = $tree->tree_name;
            $tree->common_name = $tree->tree_name;
            $tree->short_information = $tree->description;
            $tree->save();

            // Tree Photo
            for($e=1;$e<=3;$e++) {
                $tree_photo = new TreePhotos();
                $tree_photo->tree_code = $tree->tree_code;
                $tree_photo->created_at = now();
                $tree_photo->filename = "tree.jpg";
                $tree_photo->path = "assets/adminmart/assets/images";
                $tree_photo->save();
            }

            $this->console->info("Tree created: ".$tree->tree_name);
        }

        // Dummy Project
        for($i=1;$i<=5;$i++) {
            $project = new Projects();
            $project->created_at = now();
            $project->location = $faker->address;
            $project->project_no = strtoupper(Str::random(8));
            $project->project_category = $faker->randomElement(['arboretum','desa konservasi','restorasi_kta','argorgorestri','hutan_rakyat']);
            $project->project_name = ucwords("Project ".$faker->words(5, true)." ".date('Y'));
            $project->project_date = date('Y').'-01-01';
            $project->end_project = date("Y")."-12-30";
            $project->project_description = $faker->text(190);
            $project->total_trees = rand(1000, 5000);
            $project->donors = $faker->company;
            $project->mu_no = 0;
            $project->save();

            $this->console->info("Project created: ".$project->project_name);
        }

        $projects = collect(Projects::all());

        // Dummy Farmer
        for($i=1;$i<=$total*5;$i++) {
            $farmer = new Farmers();
            $farmer->name = $faker->name;
            $farmer->farmer_no = strtoupper(Str::random(8));
            $farmer->birthday = $faker->date("Y-m-d","-25 years");
            $farmer->active = 1;
            $farmer->gender = "male";
            $farmer->religion = $faker->randomElement(['islam','kristen','hindu','buddha']);
            $farmer->ethnic = "-";
            $farmer->origin = $faker->randomElement(['lokal','pendatang']);
            $farmer->family_size = rand(2, 10);
            $farmer->ktp_no = rand(1000,10000);
            $farmer->phone = $faker->phoneNumber;
            $farmer->address = $faker->address;
            $farmer->village = "-";
            $farmer->kecamatan = "-";
            $farmer->city = $faker->city;
            $farmer->province = $faker->state;
            $farmer->post_code = $faker->postcode;
            $farmer->mu_no = 0;
            $farmer->group_no = 0;
            $farmer->user_id = 0;
            $farmer->main_income = rand(1000,10000);
            $farmer->side_income = rand(1000,10000);
            $farmer->created_at = now();
            $farmer->save();

            // insert farmer photo
            for($photo_i=1;$photo_i<=3;$photo_i++) {
                $photo = new FarmerPhotos();
                $photo->created_at = now();
                $photo->farmer_no = $farmer->farmer_no;
                $photo->filename = "2.jpg";
                $photo->path = "assets/adminmart/assets/images/users";
                $photo->save();

                $story = new FarmerStories();
                $story->story_no = strtoupper(Str::random(8));
                $story->created_at = now();
                $story->farmer_no = $farmer->farmer_no;
                $story->year = date('Y-m-d');
                $story->active = 1;
                $story->story_description = $faker->text(190);
                $story->story_title = $faker->text(40);
                $story->save();

                $testi = new FarmerTestimonial();
                $testi->farmer_no = $farmer->farmer_no;
                $testi->created_at = now();
                $testi->year = date('Y-m-d');
                $testi->testimonial = $faker->text(100);
                $testi->save();
            }

            $this->console->info("Farmer created: ".$farmer->name);

            // Dummy Lahan
            for($e=1;$e<=3;$e++) {
                $lahan = new Lahans();
                $lahan->created_at = now();
                $lahan->lahan_no = strtoupper(Str::random(8));
                $lahan->farmer_no = $farmer->farmer_no;
                $lahan->active = 1;
                $lahan->city = $faker->city;
                $lahan->description = $faker->text(100);
                $lahan->latitude = $faker->latitude(-60,60);
                $lahan->longitude = $faker->longitude(-70, 70);
                $lahan->document_no = 0;
                $lahan->land_area = 0;
                $lahan->planting_area = rand(1,10);
                $lahan->coordinate = 0;
                $lahan->polygon = 0;
                $lahan->village = "-";
                $lahan->kecamatan = "-";
                $lahan->province = $faker->state;
                $lahan->mu_no = 0;
                $lahan->user_id = 0;
                $lahan->save();

                // Assign to project
                $project_lahan = new ProjectDetails();
                $project_lahan->project_no = $projects->random(1)->first()->project_no;
                $project_lahan->lahan_no = $lahan->lahan_no;
                $project_lahan->save();

                $this->console->info("Lahan created: ".$lahan->lahan_no);
            }
        }

        $lahans = collect(Lahans::all());

        for($i=1;$i<=$total;$i++) {
            $part = new Participants();
            $part->created_at = now();
            $part->participant_no = "EU".strtoupper(Str::random(6));
            $part->first_name = $faker->firstName;
            $part->last_name = $faker->lastName;
            $part->email = $faker->email;
            $part->join_date = now();
            $part->address1 = $faker->address;
            $part->company = $faker->company;
            $part->city = $faker->city;
            $part->country = $faker->country;
            $part->phone = $faker->phoneNumber;
            $part->postal_code = $faker->postcode;
            $part->save();

            $this->console->info("Participant created: ".$part->participant_no);

            for($e=1;$e<=$total;$e++) {
                try {
                    $trans = new TransactionTrees();
                    $trans->participant_no = $part->participant_no;
                    $trans->created_at = $faker->dateTimeInInterval("-2 weeks","+5 days")->format("Y-m-d");
                    $trans->win_number = strtoupper(Str::random(6));
                    $trans->transaction_at = $trans->created_at;
                    $trans->tree_code = Trees::findRandom()->tree_code;
                    $trans->quantity = rand(10,100);
                    $trans->save();

                    $lahan = $lahans->random(1)->first();
                    $planted = new PlantedWinTrees();
                    $planted->created_at = $trans->created_at;
                    $planted->win_number = $trans->win_number;
                    $planted->lahan_no = $lahan->lahan_no;
                    $planted->tree_code = $trans->tree_code;
                    $planted->quantity = $trans->quantity;
                    $planted->planted_date = $faker->dateTimeInInterval("now","+10 days")->format("Y-m-d");
                    $planted->latitude = $lahan->latitude;
                    $planted->longitude = $lahan->longitude;
                    $planted->save();

                    $this->console->info("Transaction created: ".$trans->win_number);
                } catch (\Exception $e) {

                }
            }
        }
    }
}