<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/29/2020
 * Time: 2:13 PM
 */

namespace App\Helpers;


use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ModelGenerator
{
    private $console;

    public function __construct(Command $console)
    {
        $this->console = $console;
    }

    public function run() {
        $this->console->info("Create a model");

        if($this->console->argument("table_name")) {
            $table = $this->console->argument("table_name");
            $model_name = $this->console->argument("model_name")?:$table;
            $model_name = ucfirst(Str::camel($model_name));
            $this->generate($table, $model_name);
        } else {
            $tables = DB::select('SHOW TABLES');
            foreach($tables as $table) {
                $table = head($table);
                $model_name = ucfirst(Str::camel($table));
                $this->generate($table, $model_name);
            }
        }

        $this->console->info("Create a model has been completed!");
    }

    private function generate($table, $model_name) {

        $fields = DB::getSchemaBuilder()->getColumnListing($table);
        $public_property = "";
        foreach($fields as $field) {
            $public_property .= 'public $'.$field.';'."\n\t";
        }

        $template = file_get_contents(base_path("routes/ModelMakerTemplate/ModelTemplate.php.stub"));

        $template = str_replace("ModelTemplate", $model_name, $template);
        $template = str_replace('public $property;', $public_property, $template);
        $template = str_replace('public static $table = null;','public static $table = "'.$table.'";', $template);

        file_put_contents(base_path("app/Models/".$model_name.".php"), $template);

        $this->console->info("Generated model $table");
    }

}