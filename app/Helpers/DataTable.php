<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 3/5/2020
 * Time: 1:51 PM
 */

namespace App\Helpers;

class DataTable
{
    private $table;
    private $searchable_columns;
    private $query;
    private $render;
    private $columns;

    public function __construct($table)
    {
        $this->table = $table;
    }

    /**
     * @param array $columns
     */
    public function columns($columns) {
        $this->columns = $columns;
    }

    /**
     * @param callable $query
     */
    public function query($query) {
        $this->query = $query;
    }

    /**
     * @param array $columns
     */
    public function searchable($columns)
    {
        $this->searchable_columns = $columns;
    }

    /**
     * @param callable $render
     */
    public function render($render) {
        $this->render = $render;
    }

    public function get() {
        $result = DB($this->table);

        if(isset($this->query)) {
            $result = call_user_func($this->query, $result);
        }

        if($search = request('search')['value']) {
            if($this->searchable_columns) {
                foreach($this->searchable_columns as $i=>$column) {
                    if($i==0) $result->where($column,"like","%".$search."%");
                    else $result->orWhere($column,"like","%".$search."%");
                }
            }
        }

        if(request('order')) {
            $order_column_idx = request('order')[0]['column'];
            $order_column = $this->columns[$order_column_idx];
            $order_column_dir = request('order')[0]['dir'];
        } else {
            $order_column = $this->table.".id";
            $order_column_dir = "desc";
        }

        $result->orderBy($order_column, $order_column_dir);
        $result->offset(request('start'));
        $result->limit(request('length'));
        $result_data = $result->get();

        $no_start = request('start');
        $data = [];
        foreach($result_data as &$item) {
            $no_start++;
            $item->no = $no_start;

            // Render
            $data[] = call_user_func($this->render, $item);
        }

        $result = DB($this->table);
        if(isset($this->query)) {
            $result = call_user_func($this->query, $result);
        }
        $records_total = $result->count();

        $result = DB($this->table);
        if(isset($this->query)) {
            $result = call_user_func($this->query, $result);
        }

        if($search = request('search')['value']) {
            if($this->searchable_columns) {
                foreach($this->searchable_columns as $i=>$column) {
                    if($i==0) $result->where($column,"like","%".$search."%");
                    else $result->orWhere($column,"like","%".$search."%");
                }
            }
        }

        $records_total_filtered = $result->count();

        return [
            'draw'=> request('draw'),
            'recordsTotal'=> $records_total,
            'recordsFiltered'=> $records_total_filtered,
            'data'=> $data
        ];
    }

}