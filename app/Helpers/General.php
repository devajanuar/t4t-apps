<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/29/2020
 * Time: 5:12 PM
 */

if(!function_exists('update_setting')) {
    function update_setting($setting_name, $value) {
        $setting = \App\Models\ApiSettings::findBy("setting_name", $setting_name);
        if($setting) {
            \Illuminate\Support\Facades\Cache::forever("setting_".$setting_name, $value);
        } else {
            $new = new \App\Models\ApiSettings();
            $new->setting_name = $setting_name;
            $new->setting_value = $value;
            $new->save();
        }
    }
}

if(!function_exists('get_setting')) {
    function get_setting($setting_name, $default = null) {
        if(\Illuminate\Support\Facades\Cache::has("setting_".$setting_name)) {
            return \Illuminate\Support\Facades\Cache::get("setting_".$setting_name);
        }

        $setting = \App\Models\ApiSettings::findBy("setting_name", $setting_name);
        if($setting) {
            \Illuminate\Support\Facades\Cache::forever("setting_".$setting_name, $setting->setting_value);
            return $setting->setting_value;
        } else {
            return $default;
        }
    }
}

if(!function_exists('html_response_code')) {
    function html_response_code($code) {
        switch ($code) {
            case 401:
                return "<span style='font-size: 12px' title='Unauthorized' class='btn btn-block disabled btn-warning'>401</span>";
                break;
            case 403:
                return "<span style='font-size: 12px' title='Forbidden Access' class='btn btn-block disabled btn-warning'>403</span>";
                break;
            case 500:
                return "<span style='font-size: 12px' title='Internal Server Error' class='btn btn-block disabled btn-danger'>500</span>";
                break;
            case 404:
                return "<span style='font-size: 12px' title='URL Not Found' class='btn btn-block disabled btn-info'>404</span>";
                break;
            case 405:
                return "<span style='font-size: 12px' title='Method not allowed' class='btn btn-block disabled btn-warning'>405</span>";
                break;
            case 200:
                return "<span style='font-size: 12px' title='Success' class='btn btn-block disabled btn-success'>200</span>";
                break;
            default:
                return "<span style='font-size: 12px' class='btn btn-block disabled btn-secondary'>$code</span>";
                break;
        }
    }
}

if(!function_exists('admin_auth')) {
    /**
     * @return \App\Helpers\AdminAuth
     */
    function admin_auth() {
        return (new \App\Helpers\AdminAuth());
    }
}

if(!function_exists('go_back')) {
    function go_back($message, $type = 'warning') {
        return redirect()->back()->with(['message'=>$message,'type'=>$type]);
    }
}

if(!function_exists('nowrap')) {
    function nowrap($html) {
        return "<div style='text-align:left;white-space:nowrap'>".$html."</div>";
    }
}

if(!function_exists('nowrap_center')) {
    function nowrap_center($html) {
        return "<div style='text-align:center;white-space:nowrap'>".$html."</div>";
    }
}

if(!function_exists('DB')) {
    /**
     * @param $table
     * @return \Illuminate\Database\Query\Builder
     */
    function DB($table) {
        return \Illuminate\Support\Facades\DB::table($table);
    }
}

if(!function_exists('api_auth')) {
    function api_auth() {
        return (new \App\Helpers\ApiAuth());
    }
}

if(!function_exists('alert_flash')) {
    function alert_flash() {
        if(session()->has('message')) {
            return "<div class='alert alert-".session('type')."'>".session('message')."</div>";
        } else {
            return null;
        }
    }
}