<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/29/2020
 * Time: 5:09 PM
 */

namespace App\Helpers;

use App\Models\ApiRequestLogs;
use Illuminate\Support\Facades\Cache;

class ApiBadLogin
{
    public static function isTemporaryBlocked() {
        $hash_name = md5(request()->ip().request()->userAgent());

        return Cache::has("temporary_blocked_".$hash_name);
    }

    public static function attempt() {
        $hash_name = md5(request()->ip().request()->userAgent());

        if(static::isTemporaryBlocked()) return true;

        Cache::increment("bad_login_attempt_".$hash_name);
        if(Cache::get("bad_login_attempt_".$hash_name) >= get_setting("bad_login_qty",3)) {

            Cache::forget("bad_login_attempt_".$hash_name);
            Cache::put("temporary_blocked_".$hash_name, true, now()->addMinutes(get_setting("temporary_blocked_minute", 15)));

            // Logging
            $log = new ApiRequestLogs();
            $log->created_at = now();
            $payload = [];
            $payload['header'] = collect(request()->header())->transform(function ($item) {
                return $item[0];
            })->toArray();
            $payload['form'] = request()->all();
            $log->payload_data = json_encode($payload);
            $log->ip_address = request()->ip();
            $log->user_agent = request()->userAgent();
            $log->response_code = 403;
            $log->url = request()->url();
            $log->save();

            return true;
        } else {
            return false;
        }
    }
}