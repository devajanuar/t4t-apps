<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 3/10/2020
 * Time: 5:08 PM
 */

namespace App\Helpers;


use App\Models\ApiAdmins;
use Illuminate\Support\Facades\Hash;

class AdminAuth
{
    public static function loginAs($api_admins_id) {

        $api_admin = ApiAdmins::findById($api_admins_id);

        session([
            'api_admins'=>$api_admin
        ]);
    }

    public static function logout() {
        session()->forget("api_admins");
    }

    public static function isLoggedIn() {
        return session()->has("api_admins");
    }

    /**
     * @return ApiAdmins
     */
    public function user() {
        return session('api_admins');
    }

    public function id() {
        return $this->user()->id;
    }

    public function name() {
        return $this->user()->name;
    }

    public static function attempt($email, $password) {
        $row = ApiAdmins::findBy("email", $email);
        if($row && Hash::check($password, $row->password)) {
            static::loginAs($row->id);
            return true;
        }
        return false;
    }

}