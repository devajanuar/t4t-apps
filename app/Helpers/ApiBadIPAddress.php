<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 3/11/2020
 * Time: 2:03 PM
 */

namespace App\Helpers;


class ApiBadIPAddress
{

    public static function isSafe() {
        $setting = get_setting('block_ip_address');
        $setting = explode(",",$setting);
        if(count($setting) && in_array(request()->ip(),$setting)) {
            return false;
        }

        return true;
    }

}