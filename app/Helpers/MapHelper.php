<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\TransactionTrees;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Object_;

class MapHelper extends Controller
{

    public function getSyt($win)
    {
        $farmers = array();

        $oLahan = [];
        $oProject = [];
        $oLocation = [];

        $tes = [];

        $donor = DB::table(TransactionTrees::$table)
            ->join('participants', 'transaction_trees.participant_no', 'participants.participant_no');
        $donor = $donor->select([
            DB::raw("CONCAT(participants.first_name, ' ' , participants.last_name) AS name"),
            'participants.join_date',
            'transaction_trees.quantity as total_trees_planted'
        ]);
        $donor = $donor->where('transaction_trees.win_number', $win)->first();

        $farmer = DB::table('planted_win_trees')->join('lahans', 'planted_win_trees.lahan_no', 'lahans.lahan_no');
        $farmer = $farmer->where('win_number', $win)->get();
        $donor->total_farmers_helped = count($farmer);

        $location = [];

        $lahan = DB::table('planted_win_trees')->join('lahans', 'planted_win_trees.lahan_no', 'lahans.lahan_no');
        $lahan = $lahan->select([
            'lahans.id as lahan_id',
            'lahans.province',
            'lahans.city as district',
            'lahans.village'
        ]);
        $lahan = $lahan->where('planted_win_trees.win_number', $win)->get();

        $oPhotos = [];
        $oFStories = [];
        $oFTesti = [];
        $oFPhoto = [];
        $tPhoto = [];
        $oProject = [];

        $lahan = DB::table('planted_win_trees')->join('lahans', 'planted_win_trees.lahan_no', 'lahans.lahan_no');
        $lahan = $lahan->select([
            'lahans.id as lahan_id',
            'lahans.province',
            'lahans.city as district',
            'lahans.village'
        ]);
        $lahan = $lahan->where('planted_win_trees.win_number', $win)->first();

        $pwt = DB::table('planted_win_trees')
            ->join('lahans', 'planted_win_trees.lahan_no', 'lahans.lahan_no')
            ->leftJoin('project_details', 'lahans.lahan_no', 'project_details.lahan_no')
            ->where('win_number', $win)->first();

        if ($pwt != null) {
            $farmer_no = $pwt->farmer_no;
            $tree_code = $pwt->tree_code;
            $project_no = $pwt->project_no;
            $group_no = $pwt->group_no;
            $lahan_no = $pwt->lahan_no;
        }

        $lahanPhoto = DB::table('lahan_photos')->where('lahan_no', $lahan_no)->select([
            DB::raw("CONCAT('http://', path, '/', filename) as photos")
        ])->get();

        foreach ($lahanPhoto as $lp) {
            $oPhotos[] = $lp->photos;
        }
        $lahan->photos = $oPhotos;

        $farmers = DB::table('farmers')->select([
            'name',
            'join_date'
        ])->where('farmer_no', $farmer_no)->first();

        $farmer_stories = DB::table('farmer_stories')->select([
            'story_title'
        ])->where('farmer_no', $farmer_no)->get();

        foreach ($farmer_stories as $fs) {
            $oFStories[] = $fs->story_title;
        }
        $farmers->farmer_story = $oFStories;

        $farmer_testimonial = DB::table('farmer_testimonial')->select([
            'testimonial'
        ])->where('farmer_no', $farmer_no)->get();

        foreach ($farmer_testimonial as $ft) {
            $oFTesti[] = $ft->testimonial;
        }
        $farmers->farmer_testimonial = $oFTesti;

        $farmer_photo = DB::table('farmer_photos')->select([
            DB::raw("CONCAT('http://', farmer_photos.path, '/', farmer_photos.filename) as photos")
        ])->where('farmer_no', $farmer_no)->get();

        foreach ($farmer_photo as $fp) {
            $oFPhoto[] = $fp->photos;
        }
        $farmers->photos = $oFPhoto;
        $lahan->farmer = $farmers;

        $tree = DB::table('trees')->join('planted_win_trees', 'trees.tree_code', 'planted_win_trees.tree_code')->select([
            'trees.tree_code',
            'trees.tree_name',
            'planted_win_trees.quantity as qty_planted_trees',
            'trees.short_information as short_description'
        ])->where('planted_win_trees.win_number', $win)->get();

        foreach ($tree as $t => $value) {
            if ($value) {
                $tree_photo = DB::table('tree_photos')->select([
                    DB::raw("CONCAT('http://', tree_photos.path, '/', tree_photos.filename) as photos")
                ])->where('tree_code', $tree_code)->get();

                foreach ($tree_photo as $tp) {
                    $tPhoto[] = $tp->photos;
                }

                $tree[$t]->photos = $tPhoto;
            }
        }
        $lahan->tree = $tree;

        $location = DB::table('lahans')->select([
            'group_no as location_id',
            'lahans.id',
            'coordinate as center_flag',
            'polygon',
            'latitude',
            'longitude'
        ])->where('lahan_no', $lahan_no)->first();

        $lahan->location = $location;

        $oLahan[] = $lahan;

        $project = DB::table('projects')->select([
            'project_no as project_code',
            'project_name',
            'project_description'
        ])->where('project_no', $project_no)->first();

        $oProject[] = $project;

        $locations = DB::table('group_locations')->where('group_no', $group_no)->select([
            'group_no as location_id',
            'center_flag',
            'marker_type',
            'polygon',
            'latitude',
            'longitude'
        ])->first();
        if ($locations != null) {
            if ($oLocation != []) {
                foreach ($oLocation as $loc) {
                    $locate = (object)$loc;
                    if ($locate->location_id != $locations->location_id) {
                        $oLocation[] = $locations;
                    }
                }
            } else {
                $oLocation[] = $locations;
            }
        }


        return response()->json([
            "status" => 200,
            "message" => "success",
            "data" => [
                "donor" => $donor,
                "location" => $oLocation,
                "lahan" => $oLahan,
                "project" => $oProject
            ]
        ]);
    }

    public function getSot($partID)
    {

        $farmers = array();

        $oLahan = [];
        $oProject = [];
        $oLocation = [];

        $tes = [];

        $donor = DB::table(TransactionTrees::$table)
            ->join('participants', 'transaction_trees.participant_no', 'participants.participant_no');
        $donor = $donor->select([
            DB::raw("CONCAT(participants.first_name, ' ' , participants.last_name) AS name"),
            'participants.join_date',
            DB::raw("SUM(transaction_trees.quantity) as total_trees_planted")
        ]);
        $donor = $donor->where('transaction_trees.participant_no', $partID)
        ->groupBy('participants.first_name', 'participants.last_name','participants.join_date')->get();

        $ts = DB::table('transaction_trees')->where('participant_no', $partID)->select([
            'win_number'
        ])->get();

        foreach ($ts as $wn) {
            $win = $wn->win_number;
            $farmer = DB::table('planted_win_trees')->join('lahans', 'planted_win_trees.lahan_no', 'lahans.lahan_no');
            $farmer = $farmer->where('win_number', $win)->get();

            $tes[] = $farmer;

            foreach ($donor as $key => $value) {
                if ($value) {
                    $donor[$key]->total_farmers_helped = count($tes);
                }
            }

            $oPhotos = [];
            $oFStories = [];
            $oFTesti = [];
            $oFPhoto = [];
            $tPhoto = [];
            $oProject = [];

            $lahan = DB::table('planted_win_trees')->join('lahans', 'planted_win_trees.lahan_no', 'lahans.lahan_no');
            $lahan = $lahan->select([
                'lahans.id as lahan_id',
                'lahans.province',
                'lahans.city as district',
                'lahans.village'
            ]);
            $lahan = $lahan->where('planted_win_trees.win_number', $win)->first();

            $pwt = DB::table('planted_win_trees')
                ->join('lahans', 'planted_win_trees.lahan_no', 'lahans.lahan_no')
                ->leftJoin('project_details', 'lahans.lahan_no', 'project_details.lahan_no')
                ->where('win_number', $win)->first();

            if ($pwt != null) {
                $farmer_no = $pwt->farmer_no;
                $tree_code = $pwt->tree_code;
                $project_no = $pwt->project_no;
                $group_no = $pwt->group_no;
                $lahan_no = $pwt->lahan_no;
            }

            $lahanPhoto = DB::table('lahan_photos')->where('lahan_no', $lahan_no)->select([
                DB::raw("CONCAT('http://', path, '/', filename) as photos")
            ])->get();

            foreach ($lahanPhoto as $lp) {
                $oPhotos[] = $lp->photos;
            }
            $lahan->photos = $oPhotos;

            $farmers = DB::table('farmers')->select([
                'name',
                'join_date'
            ])->where('farmer_no', $farmer_no)->first();

            $farmer_stories = DB::table('farmer_stories')->select([
                'story_title'
            ])->where('farmer_no', $farmer_no)->get();

            foreach ($farmer_stories as $fs) {
                $oFStories[] = $fs->story_title;
            }
            $farmers->farmer_story = $oFStories;

            $farmer_testimonial = DB::table('farmer_testimonial')->select([
                'testimonial'
            ])->where('farmer_no', $farmer_no)->get();

            foreach ($farmer_testimonial as $ft) {
                $oFTesti[] = $ft->testimonial;
            }
            $farmers->farmer_testimonial = $oFTesti;

            $farmer_photo = DB::table('farmer_photos')->select([
                DB::raw("CONCAT('http://', farmer_photos.path, '/', farmer_photos.filename) as photos")
            ])->where('farmer_no', $farmer_no)->get();

            foreach ($farmer_photo as $fp) {
                $oFPhoto[] = $fp->photos;
            }
            $farmers->photos = $oFPhoto;
            $lahan->farmer = $farmers;

            $tree = DB::table('trees')->join('planted_win_trees', 'trees.tree_code', 'planted_win_trees.tree_code')->select([
                'trees.tree_code',
                'trees.tree_name',
                'planted_win_trees.quantity as qty_planted_trees',
                'trees.short_information as short_description'
            ])->where('planted_win_trees.win_number', $win)->get();

            foreach ($tree as $t => $value) {
                if ($value) {
                    $tree_photo = DB::table('tree_photos')->select([
                        DB::raw("CONCAT('http://', tree_photos.path, '/', tree_photos.filename) as photos")
                    ])->where('tree_code', $tree_code)->get();

                    foreach ($tree_photo as $tp) {
                        $tPhoto[] = $tp->photos;
                    }

                    $tree[$t]->photos = $tPhoto;
                }
            }
            $lahan->tree = $tree;

            $locat = DB::table('lahans')->select([
                'group_no as location_id',
                'lahans.id',
                'coordinate as center_flag',
                'polygon',
                'latitude',
                'longitude'
            ])->where('lahan_no', $lahan_no)->first();
    
            $lahan->location = $locat;

            $oLahan[] = $lahan;

            $locations = DB::table('group_locations')->where('group_no', $group_no)->select([
                'group_no as location_id',
                'center_flag',
                'marker_type',
                'polygon',
                'latitude',
                'longitude'
            ])->first();
            if ($locations != null) {
                if ($oLocation != []) {
                    foreach ($oLocation as $loc) {
                        $locate = (object)$loc;
                        if ($locate->location_id != $locations->location_id) {
                            $oLocation[] = $locations;
                        }
                    }
                } else {
                    $oLocation[] = $locations;
                }
            }

            $project = DB::table('projects')->join('project_details', 'projects.project_no', 'project_details.project_no')
            ->join('planted_win_trees', 'project_details.lahan_no', 'planted_win_trees.lahan_no')->select([
                'projects.project_no as project_code',
                'projects.project_name',
                'projects.project_description'
            ])->where('win_number', $win)->first();

            $oProject[] = $project;
        }

        return response()->json([
            "status" => 200,
            "message" => "success",
            "data" => [
                "donor" => $donor,
                "location" => $oLocation,
                "lahan" => $oLahan,
                "project" => $oProject
            ]
        ]);
    }

    public function getSotZoom($partID, $group_number)
    {

        $data = array();
        $farmers = array();

        $oLahan = [];
        $oProject = [];
        $oLocation = [];

        $tes = [];

        $donor = DB::table(TransactionTrees::$table)
            ->join('participants', 'transaction_trees.participant_no', 'participants.participant_no');
        $donor = $donor->select([
            DB::raw("CONCAT(participants.first_name, ' ' , participants.last_name) AS name"),
            'participants.join_date',
            DB::raw("SUM(transaction_trees.quantity) as total_trees_planted")
        ]);
        $donor = $donor->where('transaction_trees.participant_no', $partID)
        ->groupBy('participants.first_name', 'participants.last_name','participants.join_date')->get();

        $ts = DB::table('transaction_trees')->where('participant_no', $partID)->select([
            'win_number'
        ])->get();

        foreach ($ts as $wn) {
            $win = $wn->win_number;
            $farmer = DB::table('planted_win_trees')->join('lahans', 'planted_win_trees.lahan_no', 'lahans.lahan_no');
            $farmer = $farmer->where('win_number', $win)->get();

            $tes[] = $farmer;

            foreach ($donor as $key => $value) {
                if ($value) {
                    $donor[$key]->total_farmers_helped = count($tes);
                }
            }

            $oPhotos = [];
            $oFStories = [];
            $oFTesti = [];
            $oFPhoto = [];
            $tPhoto = [];
            $oProject = [];

            $lahan = DB::table('planted_win_trees')->join('lahans', 'planted_win_trees.lahan_no', 'lahans.lahan_no');
            $lahan = $lahan->select([
                'lahans.id as lahan_id',
                'lahans.province',
                'lahans.city as district',
                'lahans.village'
            ]);
            $lahan = $lahan->where('lahans.group_no', $group_number);
            $lahan = $lahan->where('planted_win_trees.win_number', $win)->first();

            if ($lahan != null) {
                $pwt = DB::table('planted_win_trees')
                    ->join('lahans', 'planted_win_trees.lahan_no', 'lahans.lahan_no')
                    ->join('project_details', 'planted_win_trees.lahan_no', 'project_details.lahan_no')
                    ->where('win_number', $win)
                    ->where('group_no', $group_number)->first();

                $number_lahan = '';
                $farmer_no = '';
                $tree_code = '';
                $project_no = '';
                $group_no = '';

                if ($pwt != null) {
                    $farmer_no = $pwt->farmer_no;
                    $tree_code = $pwt->tree_code;
                    $project_no = $pwt->project_no;
                    $group_no = $pwt->group_no;
                    $number_lahan = $pwt->lahan_no;
                }

                $lahanPhoto = DB::table('lahan_photos')->where('lahan_no', $number_lahan)->select([
                    DB::raw("CONCAT('http://', path, '/', filename) as photos")
                ])->get();

                foreach ($lahanPhoto as $lp) {
                    $oPhotos[] = $lp->photos;
                }
                $lahan->photos = $oPhotos;

                if ($farmers != null) {
                    $farmers = DB::table('farmers')->select([
                        'name',
                        'join_date'
                    ])->where('farmer_no', $farmer_no)->first();

                    $farmer_stories = DB::table('farmer_stories')->select([
                        'story_title'
                    ])->where('farmer_no', $farmer_no)->get();

                    foreach ($farmer_stories as $fs) {
                        $oFStories[] = $fs->story_title;
                    }
                    $farmers->farmer_story = $oFStories;

                    $farmer_testimonial = DB::table('farmer_testimonial')->select([
                        'testimonial'
                    ])->where('farmer_no', $farmer_no)->get();

                    foreach ($farmer_testimonial as $ft) {
                        $oFTesti[] = $ft->testimonial;
                    }
                    $farmers->farmer_testimonial = $oFTesti;

                    $farmer_photo = DB::table('farmer_photos')->select([
                        DB::raw("CONCAT('http://', farmer_photos.path, '/', farmer_photos.filename) as photos")
                    ])->where('farmer_no', $farmer_no)->get();

                    foreach ($farmer_photo as $fp) {
                        $oFPhoto[] = $fp->photos;
                    }
                    $farmers->photos = $oFPhoto;
                    $lahan->farmer = $farmers;
                }

                $tree = DB::table('trees')->join('planted_win_trees', 'trees.tree_code', 'planted_win_trees.tree_code')->select([
                    'trees.tree_code',
                    'trees.tree_name',
                    'planted_win_trees.quantity as qty_planted_trees',
                    'trees.short_information as short_description'
                ])->where('planted_win_trees.win_number', $win)->get();

                foreach ($tree as $t => $value) {
                    if ($value) {
                        $tree_photo = DB::table('tree_photos')->select([
                            DB::raw("CONCAT('http://', tree_photos.path, '/', tree_photos.filename) as photos")
                        ])->where('tree_code', $tree_code)->get();

                        foreach ($tree_photo as $tp) {
                            $tPhoto[] = $tp->photos;
                        }

                        $tree[$t]->photos = $tPhoto;
                    }
                }
                $lahan->tree = $tree;

                $locat = DB::table('lahans')->select([
                    'group_no as location_id',
                    'lahans.id',
                    'coordinate as center_flag',
                    'polygon',
                    'latitude',
                    'longitude'
                ])->where('lahan_no', $number_lahan)->first();
        
                $lahan->location = $locat;

                $oLahan[] = $lahan;

                $locations = DB::table('group_locations')->where('group_no', $group_no)->select([
                    'group_no as location_id',
                    'center_flag',
                    'marker_type',
                    'polygon',
                    'latitude',
                    'longitude'
                ])->first();
                if ($locations != null) {
                    if ($oLocation != []) {
                        foreach ($oLocation as $loc) {
                            $locate = (object)$loc;
                            if ($locate->location_id != $locations->location_id) {
                                $oLocation[] = $locations;
                            }
                        }
                    }else {
                        $oLocation[] = $locations;
                    }
                }

                $project = DB::table('projects')->select([
                    'project_no as project_code',
                    'project_name',
                    'project_description'
                ])->where('project_no', $project_no)->first();

                if ($project != null) {
                    $oProject[] = $project;
                }
            }
        }

        return response()->json([
            "status" => 200,
            "message" => "success",
            "data" => [
                "donor" => $donor,
                "location" => $oLocation,
                "lahan" => $oLahan,
                "project" => $oProject
            ]
        ]);
    }

    public function getLahan(Request $request)
    {

        $validator = $this->validate($request, [
            'number_type' => 'required|string',
            'number_identification' => 'required|string'
        ]);

        $number_type = $request->input('number_type');
        $number_identification = $request->input('number_identification');
        $data = [];

        if ($validator) {
            if ($number_type == 'syt') {
                foreach ($data as $key) {
                    $data[$key]->win = $number_identification;
                }
                $this->getSyt($data);
            }
        }
    }
}
