<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/29/2020
 * Time: 5:09 PM
 */

namespace App\Helpers;


use App\Models\ApiTokenLogs;
use Illuminate\Support\Str;

class ApiAuth
{

    public static function generateToken()
    {
        return base64_encode(Str::random(16));
    }

    public static function getBasicToken() {
        $auth_token = request()->header("Authorization");
        $auth_token = str_replace("Basic ","", $auth_token);
        return $auth_token;
    }

    public static function getBearerToken() {
        $auth_token = request()->header("Authorization");
        $auth_token = str_replace("Bearer ","", $auth_token);
        return $auth_token;
    }

    /**
     * @return ApiTokenLogs|null
     */
    public function token() {
        $data = ApiTokenLogs::findBy("token", static::getBearerToken());
        $data = ($data)?:new ApiTokenLogs();
        return $data;
    }
}