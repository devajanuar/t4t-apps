<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 3/11/2020
 * Time: 2:24 PM
 */

namespace App\Helpers;


class ApiPermissionList
{

    public static function data() {
        return [
            ['label'=>'Create Participant','path'=>'participant/create'],
            ['label'=>'Get Tree Stock','path'=>'tree/stock'],
            ['label'=>'Submit Donation','path'=>'transaction/assign'],
            ['label'=>'History Transaction','path'=>'transaction/history'],
            ['label'=>'Map - Get SYT','path'=>'map/syt'],
            ['label'=>'Map - Get SOT','path'=>'map/sot'],
            ['label'=>'Map - Get Lahan','path'=>'map/lahan'],
        ];
    }

}