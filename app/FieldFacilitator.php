<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;

class FieldFacilitator extends Model
{
    protected $fillable = ['ff_no', 'name', 'birthday', 'religion', 'gender', 'join_date', 'ktp_no', 'phone', 'address', 'village', 'kecamatan', 'city', 'province',  'post_code', 'mu_no', 'target_area', 'bank_account', 'bank_branch', 'bank_name', 'active','user_id', 'ff_photo'];


    public function pictures()
    {
        return $this->hasMany('App\FieldFacilitatorPhoto', 'ff_no', 'ff_no');
    }

    public function scopeMaxno($query)
    {
        $year=substr(date('Y'), 2);
        $queryMax =  $query->select(DB::raw('SUBSTRING(`ff_no` ,8) AS kd_max'))
            ->where(DB::raw('MONTH(created_at)'), '=', date('m'))
            ->where(DB::raw('YEAR(created_at)'), '=', date('Y'))
            ->orderBy('ff_no', 'asc')
            ->get();
        $arr1 = array();
        if ($queryMax->count() > 0) {
            foreach ($queryMax as $k=>$v)
            {
                $arr1[$k] = (int)$v->kd_max;
            }
            $arr2 = range(1, max($arr1));
            $missing = array_diff($arr2, $arr1);
            if (empty($missing)) {
                $tmp = end($arr1) + 1;
                $kd_fix = sprintf("%04s", $tmp);
            }else{
                $kd_fix = sprintf("%04s", reset($missing));
            }
        }
        else{
            $kd_fix = '0001';
        }
        return 'FFC'.$year.date('m').$kd_fix;
    }
}
