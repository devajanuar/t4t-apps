<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FarmerTestimonial extends Model
{
    protected $fillable = ['farmer_no', 'testimonial_no', 'testimonial', 'year'];

    public function farmer()
    {
    	return $this->belongsTo('App\Farmer', 'farmer_no', 'farmer_no');
    }
}
