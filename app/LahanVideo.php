<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LahanVideo extends Model
{
    protected $fillable = ['lahan_no', 'filename', 'extension', 'path', 'mime_type'];

    public function lahan()
    {
    	return $this->belongsTo('App\Lahan', 'lahan_no', 'lahan_no');
    }
}
