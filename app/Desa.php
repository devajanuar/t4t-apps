<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Desa extends Model
{
    protected $fillable = ['kode_desa', 'name', 'kode_kecamatan'];

    public function kecamatans()
    {
    	return $this->belongsTo('App\Desa', 'kode_kecamatan', 'kode_kecamatan');
    }
}
