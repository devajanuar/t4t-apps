<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FieldFacilitatorPhoto extends Model
{
    protected $fillable = ['ff_no', 'filename', 'path'];

    public function fieldfacilitators()
    {
    	return $this->belongsTo('App\FieldFacilitator', 'ff_no', 'ff_no');
    }
}
