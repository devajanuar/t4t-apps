<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProjectExternal extends Model
{
	protected $fillable = ['projectext_no', 'donor_id', 'pic_donor', 'donor_phone', 'email1', 'email2', 'donor_type', 'true_nominal', 'rupiah_nominal', 'win_type', 'tree_per_win', 'planting_type', 'main_donor', 'donor_owner', 'payment_requirement', 'total_trees', 'tree_allocation', 'location', 'start_project', 'end_project', 'other_requirement', 'approvement', 'active', 'user_id'];

    public function scopeMaxno($query)
    {
        $year=substr(date('Y'), 2);
        $queryMax =  $query->select(DB::raw('SUBSTRING(`projectext_no` ,8) AS kd_max'))
            ->where(DB::raw('MONTH(created_at)'), '=', date('m'))
            ->where(DB::raw('YEAR(created_at)'), '=', date('Y'))
            ->orderBy('projectext_no', 'asc')
            ->get();
        $arr1 = array();
        if ($queryMax->count() > 0) {
            foreach ($queryMax as $k=>$v)
            {
                $arr1[$k] = (int)$v->kd_max;
            }
            $arr2 = range(1, max($arr1));
            $missing = array_diff($arr2, $arr1);
            if (empty($missing)) {
                $tmp = end($arr1) + 1;
                $kd_fix = sprintf("%04s", $tmp);
            }else{
                $kd_fix = sprintf("%04s", reset($missing));
            }
        }
        else{
            $kd_fix = '0001';
        }
        return 'PRO'.$year.$kd_fix;
    }
}
