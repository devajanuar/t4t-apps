<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FarmerStatus extends Model
{
    protected $fillable = ['farmer_no', 'non_formal_education', 'year'];

    public function farmer()
    {
    	return $this->belongsTo('App\Farmer', 'farmer_no', 'farmer_no');
    }
}
