<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Managementunit extends Model
{
    protected $fillable = ['mu_no', 'name', 'active'];

    public function farmer()
    {
    	return $this->hasMany('App\Farmer', 'farmer_no', 'farmer_no');
    }

    public function lahan()
    {
    	return $this->hasMany('App\Lahan', 'lahan_no', 'lahan_no');
    }

    public function project()
    {
    	return $this->hasMany('App\Project', 'project_no', 'project_no');
    }

    public function area()
    {
        return $this->hasMany('App\TargetArea', 'mu_no', 'mu_no');
    }
}
