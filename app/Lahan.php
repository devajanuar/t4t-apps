<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Lahan extends Model
{
    protected $fillable = ['lahan_no', 'document_no', 'land_area', 'planting_area', 'longitude', 'latitude', 'coordinate', 'polygon', 'village', 'kecamatan', 'city', 'province', 'description', 'elevation', 'soil_type', 'current_crops', 'active', 'farmer_no', 'mu_no', 'user_id', 'sppt', 'tutupan_lahan'];

    public function management_unit()
    {
    	return $this->belongsTo('App\Managementunit', 'mu_no', 'mu_no');
    }

    public function farmer()
    {
    	return $this->belongsTo('App\Farmer', 'farmer_no', 'farmer_no');
    }

    public function photos()
    {
		return $this->hasMany('App\LahanPhoto', 'lahan_no', 'lahan_no');
    }

    public function videos()
    {
    	return $this->hasMany('App\LahanVideo', 'lahan_no', 'lahan_no');
    }

    public function scopeMaxno($query)
    {
        $year=substr(date('Y'), 2);
        $queryMax =  $query->select(DB::raw('SUBSTRING(`lahan_no` ,8) AS kd_max'))
            ->where(DB::raw('MONTH(created_at)'), '=', date('m'))
            ->where(DB::raw('YEAR(created_at)'), '=', date('Y'))
            ->orderBy('lahan_no', 'asc')
            ->get();
        $arr1 = array();
        if ($queryMax->count() > 0) {
            foreach ($queryMax as $k=>$v)
            {
                $arr1[$k] = (int)$v->kd_max;
            }
            $arr2 = range(1, max($arr1));
            $missing = array_diff($arr2, $arr1);
            if (empty($missing)) {
                $tmp = end($arr1) + 1;
                $kd_fix = sprintf("%04s", $tmp);
            }else{
                $kd_fix = sprintf("%04s", reset($missing));
            }
        }
        else{
            $kd_fix = '0001';
        }
        return 'LHN'.$year.date('m').$kd_fix;
    }
}
