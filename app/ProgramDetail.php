<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramDetail extends Model
{
    protected $fillable = ['program_no', 'tree_code', 'detail_qty', 'detail_unit', 'detail_amount', 'detail_total', 'detail_date', 'detail_sku'];

    public function program()
    {
    	return $this->belongsTo('App\Program', 'program_no', 'program_no');
    }

    public function tree()
    {
    	return $this->belongsTo('App\Tree', 'tree_code', 'tree_code');
    }
}
