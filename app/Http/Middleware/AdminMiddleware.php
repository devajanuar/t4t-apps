<?php

namespace App\Http\Middleware;

use App\Helpers\AdminAuth;
use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!AdminAuth::isLoggedIn()) {
            return redirect("admin/auth/login")->with(['message'=>'Please login for first!','type'=>'warning']);
        }

        return $next($request);
    }
}
