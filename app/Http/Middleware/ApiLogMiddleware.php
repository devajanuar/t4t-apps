<?php

namespace App\Http\Middleware;

use App\Helpers\ApiBadIPAddress;
use App\Helpers\ApiBadLogin;
use App\Models\ApiRequestLogs;
use Closure;

class ApiLogMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {
        if(!ApiBadIPAddress::isSafe()) {

            ApiRequestLogs::saveLog(403);

            return response()->json(['status'=>403,'message'=>'Your ip address is denied!'], 403);
        }

        if(ApiBadLogin::isTemporaryBlocked()) {

            ApiRequestLogs::saveLog(403);

            return response()->json(['status'=>403,'message'=>'Forbidden Temporary Access!'], 403);
        }

        $result = $next($request);

        // Insert log
        ApiRequestLogs::saveLog(200);

        return $result;
    }
}
