<?php

namespace App\Http\Middleware;

use App\Helpers\ApiPermissionList;
use App\Models\ApiRequestLogs;
use App\Models\ApiTokenLogs;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class ApiMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {
        try {
            $api_data = $this->validate($request);

            if($this->permissionValidate($api_data)) {
                return $next($request);
            } else {
                return response()->json(['status'=>403,'message'=>"You don't have permission to access this resource!"],403);
            }

        } catch (\Exception $e) {

            ApiRequestLogs::saveLog(401);

            return response()->json(['status'=>401,'message'=>$e->getMessage()],401);
        }
    }

    private function permissionValidate(ApiTokenLogs $api_data) {
        $permission = Cache::get("token_logs_".$api_data->id);
        if(isset($permission['permission'])) {
            $permission_list = collect(ApiPermissionList::data());
            $current_path = request()->path();
            $pl = $permission_list->where("path", str_replace("api/","",$current_path))->first();
            $permission_name = strtolower(Str::slug($pl['label'], "_"));
            $pm = collect($permission['permission'])->where("permission_name", $permission_name)->where("is_active",1)->count();
            if($pm) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param Request $request
     * @return ApiTokenLogs|null
     * @throws \Exception
     */
    private function validate(Request $request) {
        $auth_value = $request->header("Authorization");
        $auth_value = str_replace("Bearer ","", $auth_value);
        if($api_data = ApiTokenLogs::findBy("token", $auth_value)) {
            if($api_data->expired_at < now()->format("Y-m-d H:i:s")) {
                throw new \Exception("Token has been expired!");
            } else {
                $api_data->expired_at = now()->addMinutes(60);
                $api_data->save();
            }

            return $api_data;
        } else {
            throw new \Exception("Token is invalid!");
        }
    }
}
