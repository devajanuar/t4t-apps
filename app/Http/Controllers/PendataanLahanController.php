<?php

namespace App\Http\Controllers;

use App\PendataanLahan;
use App\PendataanLahanDetail;
use App\Lahan;
use App\Desa;
use App\FieldFacilitator;
use App\TargetArea;
use App\ManagementUnit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PDF; 

class PendataanLahanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $this->authorize('purchasing.read.modify');
        $begin = $request->input('begin');
        $end = $request->input('end');

        if(!$begin && !$end)
        {
            $now = date('Y-m-');
            $d1 = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
            // $begin = new \DateTime($now.'01');
            // $end = new \DateTime($now.$d1);
            $year = date('Y-01-01');
            $year_end  = date('Y-12-31');
            $begin = new \DateTime($year);
            $end = new \DateTime($year_end);
        }else{
            $begin = new \DateTime($begin);
            $end = new \DateTime($end);
        }
            
        $pendataan = PendataanLahan::orderBy('created_at', 'desc')
                                    ->whereDate('created_at', '>=', $begin)
                                    ->whereDate('created_at','<=', $end)
                                    ->get();        
        
        return view('lahan.pendataan.index', compact('pendataan','begin','end'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lahan = Lahan::all();
        $mu = ManagementUnit::pluck('mu_no', 'name');   
        $ta = TargetArea::pluck('area_code', 'name');
        $village = Desa::pluck('kode_desa', 'name');
        $ff = FieldFacilitator::pluck('ff_no', 'name');

        return view('lahan.pendataan.create', compact('mu', 'ta', 'village', 'ff', 'lahan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'village'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $p = new PendataanLahan();
        $p->pendataan_no = $p->OfMaxno();
        $p->mu_no = $request->mu_no;
        $p->target_area = $request->target_area;
        $p->village = $request->village;
        $p->field_facilitator = $request->field_facilitator;
        $p->user_id = auth()->user()->name;
        $p->active = 1;
        $p->status = 'complete';
        $p->save();

        $this->pendataanDetail($request, $p);

        $p->save();

        if (!$p) {
            return redirect()->back()->withInput()->withError('cannot create Pendataan Lahan');
        }else{
            return redirect('/pendataan')->with('success', 'Successfully create Pendataan Lahan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pendataan = PendataanLahan::where('id', $id)->first();

        return view('lahan.pendataan.show', compact('pendataan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int    $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pendataan = PendataanLahan::where('id', $id)->first();
        $mu = ManagementUnit::pluck('mu_no', 'name');   
        $ta = TargetArea::pluck('area_code', 'name');
        $village = Desa::pluck('kode_desa', 'name');
        $ff = FieldFacilitator::pluck('ff_no', 'name');

        return view('lahan.pendataan.edit', compact('mu', 'supplier_list', 'village', 'ff', 'pendataan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function pendataanDetail($request, $pendataan)
    {
        foreach ($request->detail_id as $key => $value) {
            if ($value != '') {
                $pd = PendataanLahanDetail::find($value);
                $pd->lahan_no = $request->lahan_no[$key];
                $pd->pendataan_no = $pendataan->pendataan_no;
                $pd->farmer_no = $pendataan->farmer_no;
                $pd->luas_lahan = intval(str_replace(',','',$request->detail_luas[$key]));
                $pd->tutupan_lahan = $request->detail_tutupan[$key];
                $pd->luas_tanam = intval(str_replace(',','',$request->detail_tanam[$key]));
                $pd->condition = null;
                $pd->description = ' ';
                $pd->save();
            }else{
                if($request->luas_lahan[$key]!= null){
                    $detail = [
                        'lahan_no' => $request->lahan_no[$key],
                        'pendataan_no' => $pendataan->pendataan_no,
                        'farmer_no' => $pendataan->farmer_no,
                        'luas_lahan' => intval(str_replace(',','',$request->detail_luas[$key])),
                        'tutupan_lahan' => $request->detail_tutupan[$key],
                        'luas_tanam' => intval(str_replace(',','',$request->detail_tanam[$key])),
                        'condition' => null,
                        'description' =>' ',
                    ];
                    PendataanLahanDetail::create($detail);
                }
            }
        }
    }
}
