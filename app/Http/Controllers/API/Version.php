<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Version extends Controller
{
    //

    public function index() {
        return response()->json(['version'=>'1.0']);
    }
}
