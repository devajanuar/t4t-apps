<?php

namespace App\Http\Controllers\API;

use App\Models\Participants;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

class Participant extends Controller
{
    //

    public function create(Request $request) {
        try {
            $this->validate($request,[
                'first_name'=>'required',
                'last_name'=>'required',
                'address1'=>'required',
                'address2'=>'nullable|string',
                'company'=>'required',
                'city'=>'nullable|string',
                'state'=>'nullable|string',
                'country'=>'nullable|string',
                'email'=>'required|email',
                'phone'=>'required']);

            $part = new Participants();
            $part->join_date = now()->format("Y-m-d H:i:s");
            $part->participant_no = Participants::generateParticipantNo();
            $part->category = 'donor';
            $part->created_at = now();
            $part->first_name = request('first_name');
            $part->last_name = request('last_name');
            $part->address1 = request('address1');
            $part->address2 = request('address2');
            $part->company = request('company');
            $part->city = request('city');
            $part->state = request('state');
            $part->country = request('country');
            $part->email = request('email');
            $part->phone = request('phone');
            $part->save();

            return response()->json([
                'status'=>200,
                'message'=>'The participant has been created!',
                'data'=> [
                    'participant_no'=> $part->participant_no
                ]
            ]);

        } catch (ValidationException $e) {
            return response()->json(['status'=>400,'message'=>implode(", ",$e->validator->errors()->all())],400);
        }
    }
}
