<?php

namespace App\Http\Controllers\API;

use App\Models\Participants;
use App\Models\TransactionTrees;
use App\Models\Trees;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class Tree extends Controller
{
    //

    public function stock(Request $request) {
        $result = DB::table(Trees::$table)
            ->select("trees.*",DB::Raw("IFNULL((SELECT SUM(quantity) FROM transaction_trees a WHERE a.tree_code = trees.tree_code),0) as stock"))
            ->orderBy("trees.tree_name","asc")
            ->skip($request->get("offset",0))
            ->take(20);

        if($request->get("search")) {
            $result->where("trees.tree_name","like","%".$request->get("search")."%");
        }

        $result = $result->get();

        return response()->json(['status'=>200,'message'=>'success','data'=>$result]);
    }

    public function assign(Request $request) {
        try {
            $this->validate($request, [
                'participant_no' => 'required',
                'quantity' => 'required|int'
            ]);

            $tree = Trees::findRandom();

            $trans = new TransactionTrees();
            $trans->created_at = now();
            $trans->tree_code = $tree->tree_code;
            $trans->transaction_at = now();
            $trans->quantity = $request->get("quantity");
            $trans->win_number = TransactionTrees::generateWinNumber();
            $trans->save();

            return response()->json(['status'=>200,'message'=>'Transaction has been created!','data'=>[
                'win_number'=> $trans->win_number
            ]]);

        } catch (ValidationException $e) {
            return response()->json(['status'=>400,'message'=>implode(", ",$e->validator->errors()->all())],400);
        }
    }
}
