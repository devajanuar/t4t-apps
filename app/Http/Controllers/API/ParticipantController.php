<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Participant;

class ParticipantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $participant = Participant::all();

        return response()->json([
            'data'=>$this->transformCollection($participant)
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $participant = Participant::where('participant_no', $id)->first();

        if(!$participant) {
            return response()->json([
                'error'=>['message'=>'Participant does not exist']
            ], 404);
        }

        return response()->json(['data'=>$this->transform($participant)], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function transformCollection($participants){
        return array_map([$this, 'transform'], $participants->toArray());
    }

    public function transform($participant){
        return [
            'participant_id' => $participant['id'],
            'participant_no' => $participant['participant_no'],
            'participant_category' => $participant['category'],
            'participant_first_name' => $participant['first_name'],
            'participant_last_name' => $participant['last_name'],
            'participant_company' => $participant['company'],
            'participant_address1' => $participant['address1'],
            'participant_address2' => $participant['address2'],
            'participant_city' => $participant['city'],
            'participant_state' => $participant['state'],
            'participant_postalcode' => $participant['postalcode'],
            'participant_country' => $participant['country'],
            'participant_email' => $participant ['email'],
            'participant_website' => $participant['website'],
            'participant_phone' => $participant['phone'],
            'participant_join_date' => $participant['join_date'],
            'participant_active' => $participant['active'],
            'participant_user_id' => $participant['user_id'],
            'participant_participant_photo' => $participant['participant_photo'],
            'participant_comment' => $participant['comment'],
            'participant_source_of_contact' => $participant['source_of_contact'],
        ];
    }
}
