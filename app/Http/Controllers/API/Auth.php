<?php

namespace App\Http\Controllers\API;

use App\Helpers\ApiAuth;
use App\Helpers\ApiBadLogin;
use App\Models\ApiTokenLogs;
use App\Models\ApiUserPermissions;
use App\Models\ApiUsers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;

class Auth extends Controller
{

    public function __construct()
    {

    }

    public function requestToken()
    {
        $basic_token = ApiAuth::getBasicToken();
        $basic_token = base64_decode($basic_token);
        if($basic_token) {
            $basic_token = explode(":",$basic_token);
            if(count($basic_token) == 2) {

                $user = ApiUsers::findBy("username", $basic_token[0]);

                if($user) {
                    if($user->is_active) {
                        if($user && Hash::check($basic_token[1],$user->password)) {
                            $new_token = ApiAuth::generateToken();

                            // Insert into table api_token_logs
                            $token_data = new ApiTokenLogs();
                            $token_data->created_at = now();
                            $token_data->token = $new_token;
                            $token_data->user_agent = \request()->userAgent();
                            $token_data->ip_address = \request()->ip();
                            $token_data->api_users_id = $user->id;
                            $token_data->expired_at = now()->addMinutes(get_setting('token_expiry'));
                            $token_data->save();

                            $permissions = ApiUserPermissions::findAllByUser($user->id);
                            Cache::forever("token_logs_".$token_data->id,[
                               'permission'=> $permissions
                            ]);

                            return response()->json(['status'=>200,'message'=>'success','data'=>[
                                'token'=> $new_token,
                                'expired_at'=> $token_data->expired_at->toAtomString()
                            ]]);
                        }
                    } else {
                        ApiBadLogin::attempt();
                        return response()->json(['status'=>403,'message'=>'This account is inactive. Please contact T4T team.'], 403);
                    }
                }
            }
        }

        ApiBadLogin::attempt();

        return response()->json(['status'=>400,'message'=>'The credential is invalid'], 400);
    }

}
