<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Participants;
use App\Models\PlantedWinTrees;
use App\Models\TransactionTrees;
use App\Http\Controllers\Api\MapHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use phpDocumentor\Reflection\Types\Object_;

class Map extends Controller
{
    public function getSyt(Request $request, MapHelper $help) {
        $validator = $this->validate($request, [
            'win' => 'required|string'
        ]);

        $win = $request->input('win');

		return response()->json($win, 200);
        if($validator){
            return $help->getSyt($win);
        }
    }

    public function getSot(Request $request, MapHelper $help) {
        $validator = $this->validate($request, [
            'partID' => 'required|string'
        ]);

        $partID = $request->input('partID');
        $group_no = $request->input('group_location_id');
        $location_id = $request->input('location_id', null);

        if($validator){
            if($group_no != null || $group_no != ""){
                return $help->getSotZoom($partID, $group_no);
            }else{
                return $help->getSot($partID, $location_id);
            }
        }

    }

    public function getLahan(Request $request, MapHelper $help) {

        $validator = $this->validate($request, [
            'number_type' => 'required|string',
            'number_identification' => 'required|string'
        ]);

        $number_identification = $request->input('number_identification');
        $location_id = $request->input('number_type', null);

        if($validator){
            return $help->getSot($number_identification, $location_id);
        }
    }
}
