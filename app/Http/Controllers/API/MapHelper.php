<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\TransactionTrees;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MapHelper extends Controller
{

    public function getSyt($win)
    {
        $farmers = array();

        try {
            $donor = DB::table(TransactionTrees::$table)
                ->join('participants', 'transaction_trees.participant_no', 'participants.participant_no');
            $donor = $donor->select([
                DB::raw("CONCAT(participants.first_name, ' ' , participants.last_name) AS name"),
                'participants.join_date',
                DB::raw("SUM(transaction_trees.quantity) as total_trees_planted"),
                "participants.category as donor_type",
                'participants.photo'
            ]);
            $donor = $donor->where('transaction_trees.win_number', $win)
                ->groupBy('participants.first_name', 'participants.last_name', 'participants.join_date', 'participants.category', 'participants.photo')->get();

            $farmer = DB::table('planted_win_trees')
                ->join('lahans', 'planted_win_trees.lahan_no', 'lahans.lahan_no')
                ->join('transaction_trees', 'planted_win_trees.win_number', 'transaction_trees.win_number');
            $farmer = $farmer->where('transaction_trees.win_number', $win)->get();

            foreach ($donor as $key => $value) {
                if ($value) {
                    $donor[$key]->total_farmers_helped = count($farmer);
                }
            }

            $lahan = DB::table('planted_win_trees')->join('lahans', 'planted_win_trees.lahan_no', 'lahans.lahan_no')
                ->join('transaction_trees', 'planted_win_trees.win_number', 'transaction_trees.win_number');
            $lahan = $lahan->select([
                'lahans.id as lahan_id',
                'lahans.province',
                'lahans.city as district',
                'lahans.village'
            ]);
            $lahan = $lahan->where('transaction_trees.win_number', $win)->get();

            $pwt = DB::table('planted_win_trees')
                ->join('lahans', 'planted_win_trees.lahan_no', 'lahans.lahan_no')
                ->join('transaction_trees', 'planted_win_trees.win_number', 'transaction_trees.win_number')
                ->where('transaction_trees.win_number', $win)->get();

            foreach ($pwt as $keys => $value) {
                if ($value) {
                    $oPhotos = [];
                    $oFStories = [];
                    $oFTesti = [];
                    $oFPhoto = [];
                    $tPhoto = [];

                    $dataLahan = $pwt[$keys];

                    $farmer_no = $dataLahan->farmer_no;
                    $tree_code = $dataLahan->tree_code;
                    $lahan_no = $dataLahan->lahan_no;
                    $win = $dataLahan->win_number;

                    $lahanPhoto = DB::table('lahan_photos')->where('lahan_no', $lahan_no)->select([
                        'path as photos'
                    ])->get();

                    foreach ($lahanPhoto as $lp) {
                        $oPhotos[] = $lp->photos;
                    }
                    $lahan[$keys]->photos = $oPhotos;

                    $farmers = DB::table('farmers')->select([
                        'name',
                        'join_date'
                    ])->where('farmer_no', $farmer_no)->first();

                    $farmer_stories = DB::table('farmer_stories')->select([
                        'story_title'
                    ])->where('farmer_no', $farmer_no)->get();

                    foreach ($farmer_stories as $fs) {
                        $oFStories[] = $fs->story_title;
                    }
                    $farmers->farmer_story = $oFStories;

                    $farmer_testimonial = DB::table('farmer_testimonials')->select([
                        'testimonial'
                    ])->where('farmer_no', $farmer_no)->get();

                    foreach ($farmer_testimonial as $ft) {
                        $oFTesti[] = $ft->testimonial;
                    }
                    $farmers->farmer_testimonial = $oFTesti;

                    $farmer_photo = DB::table('farmer_photos')->select([
                        'path as photos'
                    ])->where('farmer_no', $farmer_no)->get();

                    foreach ($farmer_photo as $fp) {
                        $oFPhoto[] = $fp->photos;
                    }
                    $farmers->photos = $oFPhoto;
                    $lahan[$keys]->farmer = $farmers;

                    $tree = DB::table('trees')->join('planted_win_trees', 'trees.tree_code', 'planted_win_trees.tree_code')->select([
                        'trees.tree_code',
                        'trees.tree_name',
                        'planted_win_trees.quantity as qty_planted_trees',
                        'trees.short_information as short_description'
                    ])->where('planted_win_trees.win_number', $win)->get();

                    foreach ($tree as $t => $value) {
                        if ($value) {
                            $tree_photo = DB::table('tree_photos')->select([
                                'path as photos'
                            ])->where('tree_code', $tree_code)->get();

                            foreach ($tree_photo as $tp) {
                                $tPhoto[] = $tp->photos;
                            }

                            $tree[$t]->photos = $tPhoto;
                        }
                    }
                    $lahan[$keys]->tree = $tree;

                    $locat = DB::table('lahans')->where('lahan_no', $lahan_no)->select([
                        'coordinate',
                        'polygon',
                        'longitude',
                        'latitude',
                        'group_no'
                    ])->first();

                    $lahan[$keys]->location = $locat;

                    $locations = DB::table('group_locations')
                        ->join('lahans', 'group_locations.group_no', 'lahans.group_no')
                        ->join('planted_win_trees', 'lahans.lahan_no', 'planted_win_trees.lahan_no')
                        ->join('transaction_trees', 'planted_win_trees.win_number', 'transaction_trees.win_number')
                        ->select([
                            'group_locations.group_no as location_id',
                            'group_locations.flag as center_flag',
                            'group_locations.polygon',
                            'group_locations.latitude',
                            'group_locations.longitude',
                            DB::raw("SUM(transaction_trees.quantity) as qty")
                        ])->where('transaction_trees.win_number', $win)
                        ->groupBy(
                            'group_locations.group_no',
                            'group_locations.flag',
                            'group_locations.polygon',
                            'group_locations.latitude',
                            'group_locations.longitude',
                        )->get();

                    $project = DB::table('planted_win_trees')->join('transaction_trees', 'planted_win_trees.win_number', 'transaction_trees.win_number')
                        ->join('lahans', 'planted_win_trees.lahan_no', 'lahans.lahan_no')
                        ->leftJoin('project_details', 'planted_win_trees.lahan_no', 'project_details.lahan_no')
                        ->join('projects', 'planted_win_trees.project_no', 'projects.project_no')
                        ->select([
                            'projects.project_no as project_code',
                            'projects.project_name',
                            'projects.project_description'
                        ])->where('transaction_trees.win_number', $win)
                        ->groupBy(
                            'projects.project_no',
                            'projects.project_name',
                            'projects.project_description'
                        )->get();
                }
            }

            return response()->json([
                "status" => 200,
                "message" => "Success",
                "data" => [
                    "donor" => $donor,
                    //"group_location" => $locations,
                    "lahan" => $lahan,
                    "project" => $project
                ]
            ]);
        } catch (\Exception $th) {
            return response()->json([
                "status" => 400,
                "message" => $th->getMessage(),
                "data" => null
            ]);
        }
    }

    public function getSot($partID, $location_id)
    {

        $farmers = array();

        try {
            $donor = DB::table(TransactionTrees::$table)
                ->join('participants', 'transaction_trees.participant_no', 'participants.participant_no');
            $donor = $donor->select([
                DB::raw("CONCAT(participants.first_name, ' ' , participants.last_name) AS name"),
                'participants.join_date',
                DB::raw("SUM(transaction_trees.quantity) as total_trees_planted"),
                "participants.category as donor_type",
                'participants.photo'
            ]);
            $donor = $donor->where('transaction_trees.participant_no', $partID)
                ->groupBy('participants.first_name', 'participants.last_name', 'participants.join_date', 'participants.category', 'participants.photo')->get();

            $farmer = DB::table('planted_win_trees')
                ->join('lahans', 'planted_win_trees.lahan_no', 'lahans.lahan_no')
                ->join('transaction_trees', 'planted_win_trees.win_number', 'transaction_trees.win_number');
            $farmer = $farmer->where('transaction_trees.participant_no', $partID)->get();

            foreach ($donor as $key => $value) {
                if ($value) {
                    $donor[$key]->total_farmers_helped = count($farmer);
                }
            }

            if ($location_id != null) {
                $lahan = DB::table('planted_win_trees')->join('lahans', 'planted_win_trees.lahan_no', 'lahans.lahan_no')
                    ->join('transaction_trees', 'planted_win_trees.win_number', 'transaction_trees.win_number');
                $lahan = $lahan->select([
                    'lahans.id as lahan_id',
                    'lahans.province',
                    'lahans.city as district',
                    'lahans.village'
                ]);
                $lahan = $lahan->where('transaction_trees.participant_no', $partID)->where('lahans.id', $location_id)->get();

                $pwt = DB::table('planted_win_trees')
                    ->join('lahans', 'planted_win_trees.lahan_no', 'lahans.lahan_no')
                    ->join('transaction_trees', 'planted_win_trees.win_number', 'transaction_trees.win_number')
                    ->where('transaction_trees.participant_no', $partID)->where('lahans.id', $location_id)->get();

                foreach ($pwt as $keys => $value) {
                    if ($value) {
                        $oPhotos = [];
                        $oFStories = [];
                        $oFTesti = [];
                        $oFPhoto = [];
                        $tPhoto = [];

                        $dataLahan = $pwt[$keys];

                        $farmer_no = $dataLahan->farmer_no;
                        $tree_code = $dataLahan->tree_code;
                        $lahan_no = $dataLahan->lahan_no;
                        $win = $dataLahan->win_number;

                        $lahanPhoto = DB::table('lahan_photos')->where('lahan_no', $lahan_no)->select([
                            'path as photos'
                        ])->get();

                        foreach ($lahanPhoto as $lp) {
                            $oPhotos[] = $lp->photos;
                        }
                        $lahan[$keys]->photos = $oPhotos;

                        $farmers = DB::table('farmers')->select([
                            'name',
                            'join_date'
                        ])->where('farmer_no', $farmer_no)->first();

                        $farmer_stories = DB::table('farmer_stories')->select([
                            'story_title'
                        ])->where('farmer_no', $farmer_no)->get();

                        foreach ($farmer_stories as $fs) {
                            $oFStories[] = $fs->story_title;
                        }
                        $farmers->farmer_story = $oFStories;

                        $farmer_testimonial = DB::table('farmer_testimonials')->select([
                            'testimonial'
                        ])->where('farmer_no', $farmer_no)->get();

                        foreach ($farmer_testimonial as $ft) {
                            $oFTesti[] = $ft->testimonial;
                        }
                        $farmers->farmer_testimonial = $oFTesti;

                        $farmer_photo = DB::table('farmer_photos')->select([
                            'path as photos'
                        ])->where('farmer_no', $farmer_no)->get();

                        foreach ($farmer_photo as $fp) {
                            $oFPhoto[] = $fp->photos;
                        }
                        $farmers->photos = $oFPhoto;
                        $lahan[$keys]->farmer = $farmers;

                        $tree = DB::table('trees')->join('planted_win_trees', 'trees.tree_code', 'planted_win_trees.tree_code')->select([
                            'trees.tree_code',
                            'trees.tree_name',
                            'planted_win_trees.quantity as qty_planted_trees',
                            'trees.short_information as short_description'
                        ])->where('planted_win_trees.win_number', $win)->get();

                        foreach ($tree as $t => $value) {
                            if ($value) {
                                $tree_photo = DB::table('tree_photos')->select([
                                    'path as photos'
                                ])->where('tree_code', $tree_code)->get();

                                foreach ($tree_photo as $tp) {
                                    $tPhoto[] = $tp->photos;
                                }

                                $tree[$t]->photos = $tPhoto;
                            }
                        }
                        $lahan[$keys]->tree = $tree;

                        $locat = DB::table('lahans')->where('lahan_no', $lahan_no)->select([
                            'coordinate',
                            'polygon',
                            'longitude',
                            'latitude',
                            'group_no'
                        ])->first();

                        $lahan[$keys]->location = $locat;

                        $locations = DB::table('group_locations')
                            ->join('lahans', 'group_locations.group_no', 'lahans.group_no')
                            ->join('planted_win_trees', 'lahans.lahan_no', 'planted_win_trees.lahan_no')
                            ->join('transaction_trees', 'planted_win_trees.win_number', 'transaction_trees.win_number')
                            ->select([
                                'group_locations.group_no as location_id',
                                'group_locations.flag as center_flag',
                                'group_locations.polygon',
                                'group_locations.latitude',
                                'group_locations.longitude',
                                DB::raw("SUM(transaction_trees.quantity) as qty")
                            ])->where('transaction_trees.participant_no', $partID)
                            ->groupBy(
                                'group_locations.group_no',
                                'group_locations.flag',
                                'group_locations.polygon',
                                'group_locations.latitude',
                                'group_locations.longitude',
                            )->get();

                        $project = DB::table('planted_win_trees')->join('transaction_trees', 'planted_win_trees.win_number', 'transaction_trees.win_number')
                            ->join('lahans', 'planted_win_trees.lahan_no', 'lahans.lahan_no')
                            ->leftJoin('project_details', 'planted_win_trees.lahan_no', 'project_details.lahan_no')
                            ->join('projects', 'planted_win_trees.project_no', 'projects.project_no')
                            ->select([
                                'projects.project_no as project_code',
                                'projects.project_name',
                                'projects.project_description'
                            ])->where('transaction_trees.participant_no', $partID)
                            ->groupBy(
                                'projects.project_no',
                                'projects.project_name',
                                'projects.project_description'
                            )->get();
                    }
                }
            } else {
                $lahan = DB::table('planted_win_trees')->join('lahans', 'planted_win_trees.lahan_no', 'lahans.lahan_no')
                    ->join('transaction_trees', 'planted_win_trees.win_number', 'transaction_trees.win_number');
                $lahan = $lahan->select([
                    'lahans.id as lahan_id',
                    'lahans.province',
                    'lahans.city as district',
                    'lahans.village'
                ]);
                $lahan = $lahan->where('transaction_trees.participant_no', $partID)->get();

                $pwt = DB::table('planted_win_trees')
                    ->join('lahans', 'planted_win_trees.lahan_no', 'lahans.lahan_no')
                    ->join('transaction_trees', 'planted_win_trees.win_number', 'transaction_trees.win_number')
                    ->where('transaction_trees.participant_no', $partID)->get();

                foreach ($pwt as $keys => $value) {
                    if ($value) {
                        $oPhotos = [];
                        $oFStories = [];
                        $oFTesti = [];
                        $oFPhoto = [];
                        $tPhoto = [];

                        $dataLahan = $pwt[$keys];

                        $farmer_no = $dataLahan->farmer_no;
                        $tree_code = $dataLahan->tree_code;
                        $group_no = $dataLahan->group_no;
                        $lahan_no = $dataLahan->lahan_no;
                        $win = $dataLahan->win_number;

                        $lahanPhoto = DB::table('lahan_photos')->where('lahan_no', $lahan_no)->select([
                            'path as photos'
                        ])->get();

                        foreach ($lahanPhoto as $lp) {
                            $oPhotos[] = $lp->photos;
                        }
                        $lahan[$keys]->photos = $oPhotos;

                        $farmers = DB::table('farmers')->select([
                            'name',
                            'join_date'
                        ])->where('farmer_no', $farmer_no)->first();

                        $farmer_stories = DB::table('farmer_stories')->select([
                            'story_title'
                        ])->where('farmer_no', $farmer_no)->get();

                        foreach ($farmer_stories as $fs) {
                            $oFStories[] = $fs->story_title;
                        }
                        $farmers->farmer_story = $oFStories;

                        $farmer_testimonial = DB::table('farmer_testimonials')->select([
                            'testimonial'
                        ])->where('farmer_no', $farmer_no)->get();

                        foreach ($farmer_testimonial as $ft) {
                            $oFTesti[] = $ft->testimonial;
                        }
                        $farmers->farmer_testimonial = $oFTesti;

                        $farmer_photo = DB::table('farmer_photos')->select([
                            'path as photos'
                        ])->where('farmer_no', $farmer_no)->get();

                        foreach ($farmer_photo as $fp) {
                            $oFPhoto[] = $fp->photos;
                        }
                        $farmers->photos = $oFPhoto;
                        $lahan[$keys]->farmer = $farmers;

                        $tree = DB::table('trees')->join('planted_win_trees', 'trees.tree_code', 'planted_win_trees.tree_code')->select([
                            'trees.tree_code',
                            'trees.tree_name',
                            'planted_win_trees.quantity as qty_planted_trees',
                            'trees.short_information as short_description'
                        ])->where('planted_win_trees.win_number', $win)->get();

                        foreach ($tree as $t => $value) {
                            if ($value) {
                                $tree_photo = DB::table('tree_photos')->select([
                                    'path as photos'
                                ])->where('tree_code', $tree_code)->get();

                                foreach ($tree_photo as $tp) {
                                    $tPhoto[] = $tp->photos;
                                }

                                $tree[$t]->photos = $tPhoto;
                            }
                        }
                        $lahan[$keys]->tree = $tree;

                        $locat = DB::table('lahans')->where('lahan_no', $lahan_no)->select([
                            'coordinate',
                            'polygon',
                            'longitude',
                            'latitude',
                            'group_no'
                        ])->first();

                        $lahan[$keys]->location = $locat;

                        $locations = DB::table('group_locations')
                            ->join('lahans', 'group_locations.group_no', 'lahans.group_no')
                            ->join('planted_win_trees', 'lahans.lahan_no', 'planted_win_trees.lahan_no')
                            ->join('transaction_trees', 'planted_win_trees.win_number', 'transaction_trees.win_number')
                            ->select([
                                'group_locations.group_no as location_id',
                                'group_locations.flag as center_flag',
                                'group_locations.polygon',
                                'group_locations.latitude',
                                'group_locations.longitude',
                                DB::raw("SUM(transaction_trees.quantity) as qty")
                            ])->where('transaction_trees.participant_no', $partID)
                            ->groupBy(
                                'group_locations.group_no',
                                'group_locations.flag',
                                'group_locations.polygon',
                                'group_locations.latitude',
                                'group_locations.longitude',
                            )->get();
                    }
                }
                $project = DB::table('planted_win_trees')->join('transaction_trees', 'planted_win_trees.win_number', 'transaction_trees.win_number')
                    ->join('lahans', 'planted_win_trees.lahan_no', 'lahans.lahan_no')
                    ->leftJoin('project_details', 'planted_win_trees.lahan_no', 'project_details.lahan_no')
                    ->join('projects', 'planted_win_trees.project_no', 'projects.project_no')
                    ->select([
                        'projects.project_no as project_code',
                        'projects.project_name',
                        'projects.project_description'
                    ])->where('transaction_trees.participant_no', $partID)
                    ->groupBy(
                        'projects.project_no',
                        'projects.project_name',
                        'projects.project_description'
                    )->get();
            }

            return response()->json([
                "status" => 200,
                "message" => "Success",
                "data" => [
                    "donor" => $donor,
                    "group_location" => $locations,
                    "lahan" => $lahan,
                    "project" => $project
                ]
            ]);
        } catch (\Exception $th) {
            return response()->json([
                "status" => 400,
                "message" => $th,
                "data" => null
            ]);
        }
    }

    public function getSotZoom($partID, $group_number)
    {

        $farmers = array();
        $oLahan = [];
        $tes = [];

        try {
            $donor = DB::table(TransactionTrees::$table)
                ->join('participants', 'transaction_trees.participant_no', 'participants.participant_no');
            $donor = $donor->select([
                DB::raw("CONCAT(participants.first_name, ' ' , participants.last_name) AS name"),
                'participants.join_date',
                DB::raw("SUM(transaction_trees.quantity) as total_trees_planted"),
                "participants.category as donor_type",
                'participants.photo'
            ]);
            $donor = $donor->where('transaction_trees.participant_no', $partID)
                ->groupBy('participants.first_name', 'participants.last_name', 'participants.join_date', 'participants.category', 'participants.photo')->get();

            $ts = DB::table('transaction_trees')->where('participant_no', $partID)->select([
                'win_number'
            ])->get();

            foreach ($ts as $wn) {
                $win = $wn->win_number;
                $farmer = DB::table('planted_win_trees')->join('lahans', 'planted_win_trees.lahan_no', 'lahans.lahan_no');
                $farmer = $farmer->where('win_number', $win)->get();

                $tes[] = $farmer;

                foreach ($donor as $key => $value) {
                    if ($value) {
                        $donor[$key]->total_farmers_helped = count($tes);
                    }
                }

                $oPhotos = [];
                $oFStories = [];
                $oFTesti = [];
                $oFPhoto = [];
                $tPhoto = [];

                $lahan = DB::table('planted_win_trees')->join('lahans', 'planted_win_trees.lahan_no', 'lahans.lahan_no');
                $lahan = $lahan->select([
                    'lahans.id as lahan_id',
                    'lahans.province',
                    'lahans.city as district',
                    'lahans.village'
                ]);
                $lahan = $lahan->where('lahans.group_no', $group_number);
                $lahan = $lahan->where('planted_win_trees.win_number', $win)->first();

                if ($lahan != null) {
                    $pwt = DB::table('planted_win_trees')
                        ->join('lahans', 'planted_win_trees.lahan_no', 'lahans.lahan_no')
                        ->where('win_number', $win)
                        ->where('group_no', $group_number)->first();

                    $number_lahan = '';
                    $farmer_no = '';
                    $tree_code = '';
                    $project_no = '';
                    $group_no = '';

                    if ($pwt != null) {
                        $farmer_no = $pwt->farmer_no;
                        $tree_code = $pwt->tree_code;
                        $number_lahan = $pwt->lahan_no;
                    }

                    $lahanPhoto = DB::table('lahan_photos')->where('lahan_no', $number_lahan)->select([
                        'path as photos'
                    ])->get();

                    foreach ($lahanPhoto as $lp) {
                        $oPhotos[] = $lp->photos;
                    }
                    $lahan->photos = $oPhotos;

                    $farmers = DB::table('farmers')->select([
                        'name',
                        'join_date'
                    ])->where('farmer_no', $farmer_no)->first();

                    $farmer_stories = DB::table('farmer_stories')->select([
                        'story_title'
                    ])->where('farmer_no', $farmer_no)->get();

                    foreach ($farmer_stories as $fs) {
                        $oFStories[] = $fs->story_title;
                    }
                    $farmers->farmer_story = $oFStories;

                    $farmer_testimonial = DB::table('farmer_testimonials')->select([
                        'testimonial'
                    ])->where('farmer_no', $farmer_no)->get();

                    foreach ($farmer_testimonial as $ft) {
                        $oFTesti[] = $ft->testimonial;
                    }
                    $farmers->farmer_testimonial = $oFTesti;

                    $farmer_photo = DB::table('farmer_photos')->select([
                        'path as photos'
                    ])->where('farmer_no', $farmer_no)->get();

                    foreach ($farmer_photo as $fp) {
                        $oFPhoto[] = $fp->photos;
                    }
                    $farmers->photos = $oFPhoto;
                    $lahan->farmer = $farmers;

                    $tree = DB::table('trees')->join('planted_win_trees', 'trees.tree_code', 'planted_win_trees.tree_code')->select([
                        'trees.tree_code',
                        'trees.tree_name',
                        'planted_win_trees.quantity as qty_planted_trees',
                        'trees.short_information as short_description'
                    ])->where('planted_win_trees.win_number', $win)->get();

                    foreach ($tree as $t => $value) {
                        if ($value) {
                            $tree_photo = DB::table('tree_photos')->select([
                                'path as photos'
                            ])->where('tree_code', $tree_code)->get();

                            foreach ($tree_photo as $tp) {
                                $tPhoto[] = $tp->photos;
                            }

                            $tree[$t]->photos = $tPhoto;
                        }
                    }
                    $lahan->tree = $tree;

                    $oLahan[] = $lahan;

                    // $locat = DB::table('lahans')->where('lahan_no', $number_lahan)->select([
                    //     'coordinate',
                    //     'polygon',
                    //     'longitude',
                    //     'latitude',
                    //     'group_no'
                    // ])->first();

                    // $lahan->location = $locat;

                    $locations = DB::table('group_locations')
                        ->join('lahans', 'group_locations.group_no', 'lahans.group_no')
                        ->join('planted_win_trees', 'lahans.lahan_no', 'planted_win_trees.lahan_no')
                        ->join('transaction_trees', 'planted_win_trees.win_number', 'transaction_trees.win_number')
                        ->select([
                            'group_locations.group_no as location_id',
                            'group_locations.flag as center_flag',
                            'group_locations.polygon',
                            'group_locations.latitude',
                            'group_locations.longitude',
                            DB::raw("SUM(transaction_trees.quantity) as qty")
                        ])->where('transaction_trees.win_number', $win)
                        ->groupBy(
                            'group_locations.group_no',
                            'group_locations.flag',
                            'group_locations.polygon',
                            'group_locations.latitude',
                            'group_locations.longitude',
                        )->get();

                    $project = DB::table('planted_win_trees')->join('transaction_trees', 'planted_win_trees.win_number', 'transaction_trees.win_number')
                        ->join('lahans', 'planted_win_trees.lahan_no', 'lahans.lahan_no')
                        ->leftJoin('project_details', 'planted_win_trees.lahan_no', 'project_details.lahan_no')
                        ->join('projects', 'planted_win_trees.project_no', 'projects.project_no')
                        ->select([
                            'projects.project_no as project_code',
                            'projects.project_name',
                            'projects.project_description'
                        ])->where('transaction_trees.participant_no', $partID)
                        ->groupBy(
                            'projects.project_no',
                            'projects.project_name',
                            'projects.project_description'
                        )->get();
                }
            }

            return response()->json([
                "status" => 200,
                "message" => "Success",
                "data" => [
                    "donor" => $donor,
                    "location" => $locations,
                    "lahan" => $oLahan,
                    "project" => $project
                ]
            ]);
        } catch (\Exception $th) {
            return response()->json([
                "status" => 400,
                "message" => $th,
                "data" => null
            ]);
        }
    }
}
