<?php

namespace App\Http\Controllers\Api;

use App\FarmerStory;
use App\Farmer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class FarmerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fa = Farmer::where('farmer_no',$id)->first();

        if (!$fa) {
            return Response::json([
                'error'=>true,
                'message'=>'Cannot find farmer',
                'code'=> 500
            ], 500);
        }else{
            return Response::json([
                'error'=>false,
                'message'=>'Successfully find farmer',
                'data'=> $fa
            ], 200);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getStory($id)
    {
        $story = FarmerStory::where('farmer_no', $id)->get();

        return $story;
    }

    public function storeStory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'story_no'=> 'required|string',
            'title' => 'required|string',
            'story_description' => 'required|string',
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages(); 

            return Response::json([
                'error'=>true,
                'message'=> $messages,
                'code'=> 500
            ], 404);
        }

        $story = new FarmerStory();
        $story->farmer_no = $request->farmer_no;
        $story->story_no = $request->story_no;
        $story->title = $request->title;
        $story->year = $request->year;
        $story->story_description = $request->story_description;
        $story->active = 1;
        $story->save();

        if (!$story) {
            return Response::json([
                'error'=>true,
                'message'=>'Cannot Create Farmer Story',
                'code'=> 500
            ], 500);
        }else{
            return Response::json([
                'error'=>false,
                'message'=>'Successfuly Create Farmer Story',
                'code'=>200,
                'data'=>$story,
            ], 200);
        }
    }

    function encryptWIN($win)
    {
        $theSource = Array("8","v","w","M","P","I","N","o","Z","h","g","_","u","W","D","-","b","O","k","a","G","t","U","j","6","7","K","A","r","y", "l","5","X","p","n","z","F","4","Y","d","2","9","q","V","s","0",".","e","H","c","x","Q","B","S");
        $r = '';
        while($win>0){
            $num = $win % 53;
            $win = ($win - $num) / 53;
            $r .= $theSource[$num];
        }
        return $r;
    } 

    function decryptWIN($crypted) 
    {
        $win = 0;
        $theSource = Array("8","v","w","M","P","I","N","o","Z","h","g","_","u","W","D","-","b","O","k","a","G","t","U","j","6","7","K","A","r","y", "l","5","X","p","n","z","F","4","Y","d","2","9","q","V","s","0",".","e","H","c","x","Q","B","S");
        $theSource = array_flip($theSource);
            $i = strlen($crypted);
            while ($i>0){
                $i--;
                $win = $win * 53 + $theSource[$crypted[$i]];
            }
            return $win;
    }
}
