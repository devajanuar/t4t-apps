<?php

namespace App\Http\Controllers\API;

use App\Models\Participants;
use App\Models\TransactionTrees;
use App\Models\Trees;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class Transaction extends Controller
{
    public function historyTransaction() {
        $limit = 20;
        $offset = request('offset',0);
        $result = DB::table(TransactionTrees::$table)
            ->join("participants","participants.participant_no","=","transaction_trees.participant_no")
            ->select("transaction_trees.*",
                "participants.first_name as participant_first_name",
                "participants.last_name as participant_last_name")
            ->take($limit)
            ->skip($offset)
            ->orderBy("transaction_trees.id","desc")
            ->get();

        return response()->json(['status'=>200,'message'=>'success','data'=>$result]);
    }

    public function assign(Request $request) {
        try {

            $this->validate($request, [
                'participant_no' => 'required|exists:participants,participant_no',
                'quantity' => 'required|int'
            ],[
                'participant_no.exists'=>'Participant is not found'
            ]);

            $tree = Trees::findRandom();

            $trans = new TransactionTrees();
            $trans->participant_no = $request->get("participant_no");
            $trans->created_at = now();
            $trans->tree_code = $tree->tree_code;
            $trans->transaction_at = now();
            $trans->quantity = $request->get("quantity");
            $trans->win_number = TransactionTrees::generateWinNumber();
            $trans->save();

            return response()->json(['status'=>200,'message'=>'Transaction has been created!','data'=>[
                'win_number'=> $trans->win_number
            ]]);

        } catch (ValidationException $e) {
            return response()->json(['status'=>400,'message'=>implode(", ",$e->validator->errors()->all())],400);
        }
    }
}
