<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use App\Kabupaten;
use App\Kecamatan;

class KecamatanController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:kecamatan-list');
         $this->middleware('permission:kecamatan-create', ['only' => ['create','store']]);
         $this->middleware('permission:kecamatan-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:kecamatan-delete', ['only' => ['destroy', 'delete']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kabupaten = Kabupaten::pluck('name', 'kabupaten_no');
        $kecamatans = Kecamatan::all();
        return view('region.kecamatan.index', compact('kabupaten', 'kecamatans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kode_kecamatan'=>'required|string|max:50|unique:kecamatans',
            'name'=>'required|string|max:50',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $kec = new Kecamatan();
        $kec->kode_kecamatan = $request->kode_kecamatan;
        $kec->name = $request->name;
        $kec->kabupaten_no = $request->kabupaten_no;

        $kec->save();

        if (!$kec) {
            return redirect()->back()->withInput()->withError('cannot create kecamatan');
        }else{
            return redirect()->back()->with('success', 'Successfully create kecamatan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validator = Validator::make($request->all(), [
            'name'=>'required|string',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $kec = Kecamatan::find($id);
        $kec->update($request->all());

        if (!$kec) {
            return redirect()->back()->withError('cannot update kecamatan');
        }else{
            return redirect()->back()->with('success', 'Successfully update kecamatan');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        foreach ($request->input('id') as $key => $value) {
            $kec = Kecamatan::find($value);
            $kec->delete();
        }

        if (!$kec) {
            return redirect()->back()->withError('cannot delete kecamatan');
        }else{
            return redirect()->back()->with('success', 'Successfully delete kecamatan');
        }
    }
}
