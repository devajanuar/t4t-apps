<?php

namespace App\Http\Controllers;

use App\FieldFacilitatorPhoto;
use Illuminate\Http\Request;

class FieldFacilitatorPhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FieldFacilitatorPhoto  $fieldFacilitatorPhoto
     * @return \Illuminate\Http\Response
     */
    public function show(FieldFacilitatorPhoto $fieldFacilitatorPhoto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FieldFacilitatorPhoto  $fieldFacilitatorPhoto
     * @return \Illuminate\Http\Response
     */
    public function edit(FieldFacilitatorPhoto $fieldFacilitatorPhoto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FieldFacilitatorPhoto  $fieldFacilitatorPhoto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FieldFacilitatorPhoto $fieldFacilitatorPhoto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FieldFacilitatorPhoto  $fieldFacilitatorPhoto
     * @return \Illuminate\Http\Response
     */
    public function destroy(FieldFacilitatorPhoto $fieldFacilitatorPhoto)
    {
        //
    }
}
