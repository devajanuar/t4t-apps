<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use App\Desa;
use App\Kecamatan;

class DesaController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:village-list');
         $this->middleware('permission:village-create', ['only' => ['create','store']]);
         $this->middleware('permission:village-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:village-delete', ['only' => ['destroy', 'delete']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kecamatan = Kecamatan::pluck('name', 'kode_kecamatan');
        $desas = Desa::all();
        return view('region.desa.index', compact('kecamatan', 'desas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kode_desa'=>'required|string|max:50|unique:desas',
            'name'=>'required|string|max:50',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $ds = new Desa();
        $ds->kode_desa = $request->kode_desa;
        $ds->name = $request->name;
        $ds->kode_kecamatan = $request->kode_kecamatan;

        $ds->save();

        if (!$ds) {
            return redirect()->back()->withInput()->withError('cannot create desa');
        }else{
            return redirect()->back()->with('success', 'Successfully create desa');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required|string',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $ds = Desa::find($id);
        $ds->update($request->all());

        if (!$ds) {
            return redirect()->back()->withError('cannot update desa');
        }else{
            return redirect()->back()->with('success', 'Successfully update desa');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        foreach ($request->input('id') as $key => $value) {
            $ds = Desa::find($value);
            $ds->delete();
        }

        if (!$ds) {
            return redirect()->back()->withError('cannot delete desa');
        }else{
            return redirect()->back()->with('success', 'Successfully delete desa');
        }
    }
}
