<?php

namespace App\Http\Controllers;

use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use File;
use Image;
use PDF;
use App\Exports\LahanExport;
use App\Imports\LahanImport;
use App\AplikasiProgramLahan;
use App\Lahan;
use App\Farmer;
use App\Managementunit;
use App\TargetArea;
use App\Province;
use App\Kabupaten;
use App\Kecamatan;
use App\Desa;

class LahanController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:lahan-list');
         $this->middleware('permission:lahan-create', ['only' => ['create','store']]);
         $this->middleware('permission:lahan-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:lahan-delete', ['only' => ['destroy', 'delete']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $begin = $request->input('begin');
        $end = $request->input('end');
        $today = date('Y-m-d');
        
        if(!$begin && !$end)
        {
            $now = date('Y-m-');
            $d1 = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
            $begin = new \DateTime($now.'01');
            $end = new \DateTime($now.$d1);
        }else{
            $begin = new \DateTime($begin);
            $end = new \DateTime($end);
        }

        if (empty($request->status)) {
            $lhn = Lahan::orderBy('created_at', 'desc')
                                    ->whereDate('created_at', '>=', $begin)
                                    ->whereDate('created_at','<=', $end)
                                    ->get();
        }else{
            $lhn = Lahan::orderBy('created_at', 'desc')
                                    ->whereDate('created_at', '>=', $begin)
                                    ->whereDate('created_at','<=', $end)
                                    ->where('status', $request->status)
                                    ->get();
        }

        $lahans =  Lahan::all();
        return view('lahan.index', compact('lahans', 'lhn', 'begin', 'end', 'today'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $cities = Kabupaten::pluck('name', 'name');
        $kecamatans = Kecamatan::pluck('name', 'name');
        $villages = Desa::pluck('name', 'name');

        $state = Province::pluck('name', 'province_code');
        $farmer_list = Farmer::pluck('name', 'farmer_no');
        $management_unit = Managementunit::pluck('name', 'mu_no');
        $ta = TargetArea::pluck('name', 'name');
        return view('lahan.create', compact('farmer_list', 'management_unit', 'state', 'cities', 'kecamatans', 'villages', 'ta'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'lahan_no' => 'required|string|max:100',
            'document_no' => 'nullable|string|max:100',
            'land_area' => 'nullable|integer',
            'planting_area' => 'nullable|integer',
            'longitude' => 'nullable|string|max:100',
            'latitude' => 'nullable|string|max:100',
            'coordinate' => 'nullable|string|max:100',
            'polygon' => 'nullable|string|max:100',
            'village' => 'nullable|string|max:100',
            'kecamatan' => 'nullable|string|max:100',
            'city' => 'nullable|string|max:100',
            'province' => 'nullable|string|max:100',
            'soil_type' => 'nullable|string|max:100',
            'elevation' => 'nullable|string|max:100',
            'current_crops' => 'nullable|string|max:100',
            'farmer_no' => 'required|exists:farmers,farmer_no',
            'mu_no' => 'required|exists:managementunits,mu_no',
            'tutupan_lahan' => 'required|string',
            'sppt' => 'nullable|image|mimes:jpg,png,jpeg'
        ]);

        try {
            $photo = null;
            if ($request->hasFile('photo')) {
                $photo = $this->saveFile($request->document_no, $request->file('photo'));
            }

            $lahan = Lahan::create([
                'lahan_no' => $request->input('lahan_no'),
                'document_no' => $request->input('document_no'),
                'land_area' => $request->input('land_area'),
                'planting_area' => $request->input('planting_area'),
                'longitude' => $request->input('longitude'),
                'latitude' => $request->input('latitude'),
                'coordinate' => $request->input('coordinate'),
                'polygon'=> $request->input('polygon'),
                'village'=> $request->input('village'),
                'kecamatan'=> $request->input('kecamatan'),
                'city'=> $request->input('city'),
                'province'=> $request->input('province'),
                'description'=> $request->input('description'),
                'soil_type'=> $request->input('soil_type'),
                'current_crops'=> $request->input('current_crops'),
                'active'=> 1,
                'farmer_no'=> $request->input('farmer_no'),
                'mu_no'=> $request->input('mu_no'),
                'user_id'=> 1,
                'sppt'=> $photo,
                'tutupan_lahan'=> $request->input('tutupan_lahan'),
            ]);

            return redirect(route('lahan.index'))
                ->with(['success' => $lahan->document_no . ' Ditambahkan']);
        } catch (\Exception $e) {
            return redirect()->back()
                ->with(['error' => $e->getMessage()]);
        }
    }

    private function saveFile($document_no, $photo)
    {
        $images = str_slug($document_no) . '_' . time() . '.' . $photo->getClientOriginalExtension();
        $path = public_path('uploads/lahan');

        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true, true);
        } 
        Image::make($photo)->save($path . '/' . $images);
        return $images;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $farmer = Farmer::find('farmer_no', $id);
        $listlahan = Lahan::with('farmers')->where('farmer_no', $id)->get();
        $aplikasilahan = AplikasiProgramLahan::with('lahans')->where('lahan_no', $id)->get();

        return view('lahan.show', compact('aplikasilahan', 'listlahan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lahan = Lahan::find($id);
        
        $cities = Kabupaten::pluck('name', 'name');
        $kecamatans = Kecamatan::pluck('name', 'name');
        $villages = Desa::pluck('name', 'name');
        $state = Province::pluck('name', 'province_code');
        $farmer_list = Farmer::pluck('name', 'farmer_no');
        $management_unit = Managementunit::pluck('name', 'mu_no');
        $ta = TargetArea::pluck('name', 'name');

        return view('lahan.edit', compact('lahan', 'cities', 'kecamatans', 'villages', 'state', 'farmer_list', 'management_unit', 'ta'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            // 'lahan_no'=>'required|string|max:10',
            'document_no'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

            $lahan = Lahan::findOrFail($id);
            $photo = $lahan->sppt;

            if ($request->hasFile('photo')) {
                !empty($photo) ? File::delete(public_path('uploads/lahan/' . $photo)):null;
                $photo = $this->saveFile($request->document_no, $request->file('photo'));
            }

            $lahan->update([
                'lahan_no' => $request->input('lahan_no'),
                'document_no' => $request->input('document_no'),
                'land_area' => $request->input('land_area'),
                'planting_area' => $request->input('planting_area'),
                'longitude' => $request->input('longitude'),
                'latitude' => $request->input('latitude'),
                'coordinate' => $request->input('coordinate'),
                'polygon'=> $request->input('polygon'),
                'village'=> $request->input('village'),
                'kecamatan'=> $request->input('kecamatan'),
                'city'=> $request->input('city'),
                'province'=> $request->input('province'),
                'description'=> $request->input('description'),
                'soil_type'=> $request->input('soil_type'),
                'current_crops'=> $request->input('current_crops'),
                'active'=> 1,
                'farmer_no'=> $request->input('farmer_no'),
                'mu_no'=> $request->input('mu_no'),
                'user_id'=> auth()->user()->id,
                'sppt'=> $photo,
                'tutupan_lahan'=> $request->input('tutupan_lahan'),
            ]);

        if (!$lahan) {
            return redirect()->back()->withInput()->withError('cannot create lahan');
        }else{
            return redirect('/lahan')->with('success', 'Successfully create lahan');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lahan = Lahan::findOrFail($id);
        if (!empty($lahan->sppt)) {
            File::delete(public_path('uploads/lahan/' . $lahan->sppt));
        }
        $lahan->delete();
        return redirect()->back()->with(['success' => 'Kategori: ' . $lahan->lahan_no . ' Telah Dihapus']);
    }

    public function delete(Request $request)
    {
        foreach ($request->input('id') as $value) {
            lahan::find($value)->delete();
        }
        return redirect()->back()->with('Success', "lahan deleted Successfully");
    }

    public function barcodegenerator($id, Request $request)
    {
        $lahan = Lahan::with('farmer')->where('id', $id)->first();
        $photo = $lahan->sppt;

            if ($request->hasFile('photo')) {
                !empty($photo) ? File::delete(public_path('uploads/lahan/' . $photo)):null;
                $photo = $this->saveFile($request->document_no, $request->file('photo'));
            }
        return view('lahan.barcode', compact('lahan', 'photo'));
    }

    public function printbarcode($id)
    {
        $lahan = Lahan::where('id', $id)->first();
        $pdf = PDF::loadview('lahan.printbarcode', ['lahan'=>$lahan]);
        return $pdf->stream();
        // return view('lahan.printbarcode', compact('lahan'));
    }

    public function export_excel()
    {
        return Excel::download(new LahanExport, 'lahan.xlsx');
    }

    function import(Request $request)
    {
        $this->validate($request, [
            'file'  => 'required|mimes:xls,xlsx'
        ]);
        Excel::import(new LahanImport,request()->file('file'));
        return back()->with('success', 'Excel Data Lahan Imported successfully.');;
     

     // $path = $request->file('select_file')->getRealPath();

     // $data = Excel::load($path)->get();

     // if($data->count() > 0)
     // {
     //  foreach($data->toArray() as $key => $value)
     //  {
     //   foreach($value as $row)
     //   {
     //    $insert_data[] = array(
     //     'id'  => $row['id'],
     //     'farmer_no'   => $row['farmer_no'],
     //     'name'   => $row['name'],
     //     'birthday'    => $row['birthday'],
     //     'religion'  => $row['religion'],
     //     'ethnic'   => $row['ethnic'],
     //     'origin'  => $row['origin'],
     //     'gender'   => $row['gender'],
     //     'join_date'   => $row['join_date'],
     //     'number_family_member'    => $row['number_family_member'],
     //     'ktp_no'  => $row['ktp_no'],
     //     'phone'   => $row['phone'],         
     //     'address'  => $row['address'],
     //     'village'   => $row['village'],
     //     'kecamatan'   => $row['kecamatan'],
     //     'city'    => $row['city'],
     //     'province'  => $row['province'],
     //     'post_code'   => $row['post_code'],
     //     'mu_no'  => $row['mu_no'],
     //     'group_no'   => $row['group_no'],
     //     'mou_no'   => $row['mou_no'],
     //     'main_income'    => $row['main_income'],
     //     'side_income'  => $row['side_income'],
     //     'active'   => $row['active'],
     //     'user_id' => $row['user_id'],
     //     'created_at' => $row['created_at'],
     //     'updated_at' => $row['updated_at']
     //    );
     //   }
     //  }

     //  if(!empty($insert_data))
     //  {
     //   DB::table('farmers')->insert($insert_data);
     //  }
     // }
     // return back()->with('success', 'Excel Data Farmer Imported successfully.');
    }
}
