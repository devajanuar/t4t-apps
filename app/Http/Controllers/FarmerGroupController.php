<?php

namespace App\Http\Controllers;

use App\FarmerGroup;
use App\Managementunit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class FarmerGroupController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:fg-list');
         $this->middleware('permission:fg-create', ['only' => ['create','store']]);
         $this->middleware('permission:fg-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:fg-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $farmergroups = FarmerGroup::all();
        return view('farmer.farmergroup.index', compact('farmergroups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $management_unit = Managementunit::pluck('name', 'mu_no');

        return view('farmer.farmergroup.create', compact('management_unit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $fg = new FarmerGroup();
        $fg->group_no = $request->input('group_no');
        $fg->name = $request->input('name');
        $fg->village = $request->input('village');
        $fg->mu_no = $request->input('mu_no');
        $fg->target_area = $request->input('target_area');
        $fg->mou_no = $request->input('mou_no');
        $fg->phone = $request->input('phone');
        $fg->pic = $request->input('pic');
        $fg->organization_structure = $request->input('organization_structure');
        $fg->active = '1';
        $fg->user_id = '1';
        // $fg->user_id = auth()->user()->username;
        $fg->save();

        if (!$fg) {
            return redirect()->back()->withInput()->withError('cannot create farmer group');
        }else{
            return redirect('/farmergroup')->with('success', 'Successfully create farmer group');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fg = FarmerGroup::find($id);
        $management_unit = Managementunit::pluck('name', 'mu_no');
        
        return view('farmer.farmergroup.edit', compact('fg', 'management_unit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $fg = FarmerGroup::find($id);
        $fg->group_no = $request->input('group_no');
        $fg->name = $request->input('name');
        $fg->village = $request->input('village');
        $fg->mu_no = $request->input('mu_no');
        $fg->target_area = $request->input('target_area');
        $fg->mou_no = $request->input('mou_no');
        $fg->phone = $request->input('phone');
        $fg->pic = $request->input('pic');
        $fg->organization_structure = $request->input('organization_structure');
        $fg->active = '1';
        $fg->user_id = '1';
        // $fg->user_id = auth()->user()->username;
        $fg->save();

        if (!$fg) {
            return redirect()->back()->withInput()->withError('cannot create farmer group');
        }else{
            return redirect('/farmergroup')->with('success', 'Successfully create farmer group');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        foreach ($request->input('id') as $value) {
            FarmerGroup::find($value)->delete();
        }
        return redirect()->back()->with('Success', "Farmer Group deleted Successfully");
    }

    public function export_excel()
    {
        return Excel::download(new FarmerGroupExport, 'farmergroup.xlsx');
    }

    public function import(Request $request)
    {
        $this->validate($request, [
            'file'  => 'required|mimes:xls,xlsx'
        ]);
        Excel::import(new FarmerGroupImport,request()->file('file'));
        return back()->with('success', 'Excel Data Farmer Group Imported successfully.');;
    }
}
