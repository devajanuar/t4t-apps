<?php

namespace App\Http\Controllers;

use App\Project;
use App\Managementunit;
use App\Participant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use App\Exports\ProjectExport;
use App\Imports\ProjectImport;

class ProjectController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:program-list');
         $this->middleware('permission:program-create', ['only' => ['create','store']]);
         $this->middleware('permission:program-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:program-delete', ['only' => ['destroy', 'delete']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::all();
        return view('project.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $participants = Participant::pluck('first_name', 'participant_no');
        $management_unit = Managementunit::pluck('name', 'mu_no');
        return view('project.create', compact('management_unit', 'participants'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'project_no'=>'required|string|max:10',
            'name'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $project = new Project();
        $project->project_no = Project::Maxno();
        $project->project_category = $request->input('project_category');
        $project->project_name = $request->input('project_name');
        $farmer->project_date = $request->input('project_date');
        $farmer->end_project = $request->input('end_project');
        $farmer->project_description = $request->input('project_description');
        $farmer->location = $request->input('location');
        $farmer->total_trees = $request->input('total_trees');
        $farmer->co2_capture = $request->input('co2_capture');
        $farmer->donors = $request->input('donors');
        $farmer->mu_no = $request->input('mu_no');
        // $farmer->user_id = auth()->user()->username;
        $project->save();

        if (!$project) {
            return redirect()->back()->withInput()->withError('cannot create project');
        }else{
            return redirect('/project')->with('success', 'Successfully create project');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::find($id);
        return view('project.show', compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::find($id);

        return view('project.edit', compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'project_no'=>'required|string|max:10',
            'name'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $project = Project::find($id);
        $project->project_no = $request->input('project_no');
        $project->project_category = $request->input('project_category');
        $project->project_name = $request->input('project_name');
        $farmer->project_date = $request->input('project_date');
        $farmer->end_project = $request->input('end_project');
        $farmer->project_description = $request->input('project_description');
        $farmer->location = $request->input('location');
        $farmer->total_trees = $request->input('total_trees');
        $farmer->co2_capture = $request->input('co2_capture');
        $farmer->donors = $request->input('donors');
        $farmer->mu_no = $request->input('mu_no');
        // $farmer->user_id = auth()->user()->username;
        $project->save();

        if (!$project) {
            return redirect()->back()->withInput()->withError('cannot create project');
        }else{
            return redirect('/project')->with('success', 'Successfully create project');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        foreach ($request->input('id') as $key => $value) {
            $project = Project::find($value);
            $project->delete();
        }

        if (!$project) {
            return redirect()->back()->withError('cannot delete Project');
        }else{
            return redirect()->back()->with('success', 'Successfully delete Project');
        }
    }

    public function export_excel()
    {
        return Excel::download(new ProjectExport, 'project.xlsx');
    }

    function import(Request $request)
    {
        $this->validate($request, [
            'file'  => 'required|mimes:xls,xlsx'
        ]);
        Excel::import(new ProjectImport,request()->file('file'));
        return back()->with('success', 'Excel Data Project Imported successfully.');;
     

     // $path = $request->file('select_file')->getRealPath();

     // $data = Excel::load($path)->get();

     // if($data->count() > 0)
     // {
     //  foreach($data->toArray() as $key => $value)
     //  {
     //   foreach($value as $row)
     //   {
     //    $insert_data[] = array(
     //     'id'  => $row['id'],
     //     'farmer_no'   => $row['farmer_no'],
     //     'name'   => $row['name'],
     //     'birthday'    => $row['birthday'],
     //     'religion'  => $row['religion'],
     //     'ethnic'   => $row['ethnic'],
     //     'origin'  => $row['origin'],
     //     'gender'   => $row['gender'],
     //     'join_date'   => $row['join_date'],
     //     'number_family_member'    => $row['number_family_member'],
     //     'ktp_no'  => $row['ktp_no'],
     //     'phone'   => $row['phone'],         
     //     'address'  => $row['address'],
     //     'village'   => $row['village'],
     //     'kecamatan'   => $row['kecamatan'],
     //     'city'    => $row['city'],
     //     'province'  => $row['province'],
     //     'post_code'   => $row['post_code'],
     //     'mu_no'  => $row['mu_no'],
     //     'group_no'   => $row['group_no'],
     //     'mou_no'   => $row['mou_no'],
     //     'main_income'    => $row['main_income'],
     //     'side_income'  => $row['side_income'],
     //     'active'   => $row['active'],
     //     'user_id' => $row['user_id'],
     //     'created_at' => $row['created_at'],
     //     'updated_at' => $row['updated_at']
     //    );
     //   }
     //  }

     //  if(!empty($insert_data))
     //  {
     //   DB::table('farmers')->insert($insert_data);
     //  }
     // }
     // return back()->with('success', 'Excel Data Farmer Imported successfully.');
    }
}
