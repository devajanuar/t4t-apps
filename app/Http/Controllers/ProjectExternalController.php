<?php

namespace App\Http\Controllers;

use App\ProjectExternal;
use App\Participant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class ProjectExternalController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:projectext-list');
         $this->middleware('permission:projectext-create', ['only' => ['create','store']]);
         $this->middleware('permission:projectext-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:projectext-delete', ['only' => ['destroy', 'delete']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $begin = $request->input('begin');
        $end = $request->input('end');
        $today = date('Y-m-d');
        
        if(!$begin && !$end)
        {
            $now = date('Y-m-');
            $d1 = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
            $begin = new \DateTime($now.'01');
            $end = new \DateTime($now.$d1);
        }else{
            $begin = new \DateTime($begin);
            $end = new \DateTime($end);
        }

        if (empty($request->approvement)) {
            $projectext = ProjectExternal::orderBy('created_at', 'desc')
                                    ->whereDate('created_at', '>=', $begin)
                                    ->whereDate('created_at','<=', $end)
                                    ->get();
        }else{
            $projectext = ProjectExternal::orderBy('created_at', 'desc')
                                    ->whereDate('created_at', '>=', $begin)
                                    ->whereDate('created_at','<=', $end)
                                    ->where('approvement', $request->approvement)
                                    ->get();
        }

        $projectexts = ProjectExternal::all();
        
        return view('projectext.index', compact('projectexts', 'projectext', 'begin', 'end', 'today'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $par = Participant::pluck('first_name', 'participant_no');
        
        return view('projectext.create', compact('par'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // 'projectext_no'=>'required|string|max:10',
            'donor_id'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $projectext = new ProjectExternal();
        $projectext->projectext_no = ProjectExternal::Maxno();
        $projectext->donor_id = $request->input('donor_id');
        $projectext->pic_donor = $request->input('pic_donor');
        $projectext->donor_phone = $request->input('donor_phone');
        $projectext->email1 = $request->input('email1');
        $projectext->email2 = $request->input('email2');
        $projectext->donor_type = $request->input('donor_type');
        $projectext->true_nominal = $request->input('true_nominal');
        $projectext->rupiah_nominal = $request->input('rupiah_nominal');
        $projectext->win_type = $request->input('win_type');
        $projectext->tree_per_win = $request->input('tree_per_win');
        $projectext->planting_type = $request->input('planting_type');
        $projectext->main_donor = $request->input('main_donor');
        $projectext->donor_owner = $request->input('donor_owner');
        $projectext->payment_requirement = $request->input('payment_requirement');
        $projectext->total_trees = $request->input('total_trees');
        $projectext->tree_allocation = $request->input('tree_allocation');
        $projectext->location = $request->input('location');
        $projectext->start_project = $request->input('start_project');
        $projectext->end_project = $request->input('end_project');
        $projectext->other_requirement = $request->input('other_requirement');
        $projectext->approvement = 0;
        $projectext->active = 1;
        $projectext->user_id = 1;
        // $farmer->user_id = auth()->user()->username;
        $projectext->save();

        if (!$projectext) {
            return redirect()->back()->withInput()->withError('cannot create project');
        }else{
            return redirect('/projectext')->with('success', 'Successfully create project');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $projectext = ProjectExternal::find($id);
        $par = Participant::pluck('first_name', 'participant_no');
        
        return view('projectext.edit', compact('par', 'projectext'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            // 'projectext_no'=>'required|string|max:10',
            'donor_id'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $projectext = ProjectExternal::find($id);
        $projectext->projectext_no = $request->input('projectext_no');
        $projectext->donor_id = $request->input('donor_id');
        $projectext->pic_donor = $request->input('pic_donor');
        $projectext->donor_phone = $request->input('donor_phone');
        $projectext->email1 = $request->input('email1');
        $projectext->email2 = $request->input('email2');
        $projectext->donor_type = $request->input('donor_type');
        $projectext->true_nominal = $request->input('true_nominal');
        $projectext->rupiah_nominal = $request->input('rupiah_nominal');
        $projectext->win_type = $request->input('win_type');
        $projectext->tree_per_win = $request->input('tree_per_win');
        $projectext->planting_type = $request->input('planting_type');
        $projectext->main_donor = $request->input('main_donor');
        $projectext->donor_owner = $request->input('donor_owner');
        $projectext->payment_requirement = $request->input('payment_requirement');
        $projectext->total_trees = $request->input('total_trees');
        $projectext->tree_allocation = $request->input('tree_allocation');
        $projectext->location = $request->input('location');
        $projectext->start_project = $request->input('start_project');
        $projectext->end_project = $request->input('end_project');
        $projectext->other_requirement = $request->input('other_requirement');
        $projectext->approvement = 0;
        $projectext->active = 1;
        $projectext->user_id = 1;
        // $farmer->user_id = auth()->user()->username;
        $projectext->save();

        if (!$projectext) {
            return redirect()->back()->withInput()->withError('cannot create project');
        }else{
            return redirect('/projectext')->with('success', 'Successfully create project');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        foreach ($request->input('id') as $value) {
            ProjectExternal::find($value)->delete();
        }
        return redirect()->back()->with('Success', "Project External deleted Successfully");
    }
}
