<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FarmerPhoto;
use App\Farmer;

class FarmerPhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $farmers = Farmer::pluck('name', 'farmer_no');
        $images = FarmerPhoto::get();
        return view('farmer.photo.index', compact('images', 'farmers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        $this->validate($request, [
            'filename' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $photo = new FarmerPhoto();
        $photo->filename = $input['filename'] = time().'.'.$request->image->getClientOriginalExtension();
        $photo->path = $input['path'] = $request->image->move(public_path('images'), $input['image']);
        // $request->image->move(public_path('images'), $input['image']);
        $photo->farmer_no = $request->farmer_no;
        $photo->save();

        // $input['farmer_no'] = $request->farmer_no;
        // FarmerPhoto::create($input);


        return back()
            ->with('success','Image Uploaded successfully.');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'filename' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $photo = new FarmerPhoto();
        // $photo->filename = $input['image'] = time().'.'.$request->filename->getClientOriginalExtension();
        // $photo->path = $input['image'] = $request->filename->move(public_path('images'), $input['image']);
        // $request->image->move(public_path('images'), $input['image']);

        if ($request->hasFile('filename'))
        {
            $file = $request->file('filename');
            $photo->filename = time().'.'.$file->getClientOriginalExtension();
            // $photo->path= $file->storeAs('farmerphoto', $photo->filename);
            $photo->path = $file->move(public_path('images'), $photo->filename);
            $photo->farmer_no = $request->farmer_no;
        }

        $photo->save();


        return back()
            ->with('success','Image Uploaded successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        FarmerPhoto::find($id)->delete();
        return back()->with('success', 'Image Farmer Deleted Successfully');
    }
}
