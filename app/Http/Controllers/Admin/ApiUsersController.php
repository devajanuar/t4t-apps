<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ApiPermissionList;
use App\Helpers\DataTable;
use App\Http\Controllers\Controller;
use App\Models\ApiUserPermissions;
use App\Models\ApiUsers;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class ApiUsersController extends Controller
{
    public function getIndex() {
        return view('admin.api_users.index');
    }

    public function list() {
        $result = new DataTable("api_users");
        $result->searchable([
           "name",
           "username"
        ]);
        $result->columns([
           "id",
           "name",
           "username",
           "is_active",
           "id"
        ]);
        $result->render(function($item) {
            $row = new ApiUsers($item);

            $status = "";
            if($row->is_active) {
                $status .= "<a href='".url("admin/api-users/change-active/".$row->id."/0")."' title='Click here to inactive this user' class='btn btn-success btn-block btn-sm'>Active</a> ";
            } else {
                $status .= "<a href='".url("admin/api-users/change-active/".$row->id."/1")."' title='Click here to active this user' class='btn btn-danger btn-block btn-sm'>Inactive</a> ";
            }

            $button = "";
            $button .= "<a href='".url("admin/api-users/update/".$row->id)."' class='btn btn-primary btn-sm'>Edit</a> ";
            $button .= "<a href='".url("admin/api-users/delete/".$row->id)."' onclick='if(!confirm(\"Are you sure want to delete this data?\")) return false' class='btn btn-danger btn-sm'>Delete</a>";

            return [
              $row->id,
              $row->name,
              $row->username,
              $status,
              nowrap_center($button)
            ];
        });

        $response = $result->get();
        return response()->json($response);
    }

    public function getChangeActive($id,$is_active) {
        $data = ApiUsers::findById($id);
        $data->is_active = $is_active;
        $data->save();

        $status = ($is_active)?"Activated":"Inactivated";
        return go_back("User has been ".$status,"success");
    }

    public function getCreate() {
        return view('admin.api_users.form', ['row'=>new ApiUsers()]);
    }

    public function postCreate(Request $request) {
        try {
            $this->validate($request, ['name', 'username' => 'required|unique:api_users', 'password','permission']);

            $data = new ApiUsers();
            $data->created_at = now();
            $data->name = request('name');
            $data->username = request('username');
            $data->password = Hash::make(request('password'));
            $data->is_active = 1;
            $data->save();

            // insert permission
            foreach(ApiPermissionList::data() as $item) {
                $permission = new ApiUserPermissions();
                $permission->api_users_id = $data->id;
                $permission->permission_name = strtolower(Str::slug($item['label'],'_'));
                $permission->is_active = isset($request->get("permission")[$permission->permission_name])?1:0;
                $permission->save();
            }

            return redirect('admin/api-users')->with(['message'=>'New data has been added!','type'=>'success']);

        } catch (ValidationException $e) {
            return go_back(implode(',',$e->errors()));
        } catch (\Exception $e) {
            Log::error($e);
            return go_back("Something went wrong, please contact admin!");
        }
    }

    public function getUpdate($id) {
        $data = [];
        $data['row'] = ApiUsers::findById($id);
        $data['data_permission'] = ApiUserPermissions::findAllByUser($id);
        return view('admin.api_users.form', $data);
    }

    public function postUpdate(Request $request) {
        try {
            $this->validate($request, ['id','name', 'username' => 'required|exists:api_users','permission']);

            $data = ApiUsers::findById(request('id'));
            $data->updated_at = now();
            $data->name = request('name');
            $data->username = request('username');
            if(request('password')) {
                $data->password = Hash::make(request('password'));
            }
            $data->save();

            // insert permission
            ApiUserPermissions::deleteWhere(function(Builder $query) {
                $query->where("api_users_id", request('id'));
                return $query;
            });
            foreach(ApiPermissionList::data() as $item) {
                $permission = new ApiUserPermissions();
                $permission->api_users_id = $data->id;
                $permission->permission_name = strtolower(Str::slug($item['label'],'_'));
                $permission->is_active = isset($request->get("permission")[$permission->permission_name])?1:0;
                $permission->save();
            }

            return redirect('admin/api-users')->with(['message'=>'The data has been saved!','type'=>'success']);

        } catch (ValidationException $e) {
            return go_back(implode(',',$e->errors()));
        } catch (\Exception $e) {
            Log::error($e);
            return go_back("Something went wrong, please contact admin!");
        }
    }

    public function getDelete($id) {
        ApiUsers::deleteById($id);
        return redirect()->back()->with(['message'=>'The data has been deleted!','type'=>'warning']);
    }

}
