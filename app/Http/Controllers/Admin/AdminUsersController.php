<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\DataTable;
use App\Http\Controllers\Controller;
use App\Models\ApiAdmins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

class AdminUsersController extends Controller
{
    public function getIndex() {
        return view('admin.admin_users.index');
    }

    public function list() {
        $result = new DataTable("api_admins");
        $result->searchable([
           "name",
           "email"
        ]);
        $result->columns([
           "id",
           "name",
           "email",
           "id"
        ]);
        $result->render(function($item) {
            $row = new ApiAdmins($item);
            $button = "";
            $button .= "<a href='".url("admin/admin-users/update/".$row->id)."' class='btn btn-primary btn-sm'>Edit</a> ";
            $button .= "<a href='".url("admin/admin-users/delete/".$row->id)."' onclick='if(!confirm(\"Are you sure want to delete this data?\")) return false' class='btn btn-danger btn-sm'>Delete</a>";

            return [
              $row->id,
              $row->name,
              $row->email,
              nowrap_center($button)
            ];
        });

        $response = $result->get();
        return response()->json($response);
    }

    public function getCreate() {
        return view('admin.admin_users.form', ['row'=>new ApiAdmins()]);
    }

    public function postCreate(Request $request) {
        try {
            $this->validate($request, ['name', 'email' => 'required|unique:api_admins', 'password']);

            $data = new ApiAdmins();
            $data->created_at = now();
            $data->name = request('name');
            $data->email = request('email');
            $data->password = Hash::make(request('password'));
            $data->save();

            return redirect('admin/admin-users')->with(['message'=>'New data has been added!','type'=>'success']);

        } catch (ValidationException $e) {
            return go_back(implode(',',$e->errors()));
        } catch (\Exception $e) {
            Log::error($e);
            return go_back("Something went wrong, please contact admin!");
        }
    }

    public function getUpdate($id) {
        return view('admin.admin_users.form', ['row'=>ApiAdmins::findById($id)]);
    }

    public function postUpdate(Request $request) {
        try {
            $this->validate($request, ['id','name', 'email' => 'required|exists:api_admins']);

            $data = ApiAdmins::findById(request('id'));
            $data->updated_at = now();
            $data->name = request('name');
            $data->email = request('email');
            if(request('password')) {
                $data->password = Hash::make(request('password'));
            }
            $data->save();

            return redirect('admin/admin-users')->with(['message'=>'The data has been saved!','type'=>'success']);

        } catch (ValidationException $e) {
            return go_back(implode(',',$e->errors()));
        } catch (\Exception $e) {
            Log::error($e);
            return go_back("Something went wrong, please contact admin!");
        }
    }


    public function getProfile() {
        return view('admin.admin_users.profile', ['row'=>ApiAdmins::findById(admin_auth()->id())]);
    }

    public function postProfile(Request $request) {
        try {
            $this->validate($request, ['name', 'email' => 'required|exists:api_admins']);

            $data = ApiAdmins::findById(admin_auth()->id());
            $data->updated_at = now();
            $data->name = request('name');
            $data->email = request('email');
            if(request('password')) {
                $data->password = Hash::make(request('password'));
            }
            $data->save();

            return redirect()->back()->with(['message'=>'The profile has been saved!','type'=>'success']);

        } catch (ValidationException $e) {
            return go_back(implode(',',$e->errors()));
        } catch (\Exception $e) {
            Log::error($e);
            return go_back("Something went wrong, please contact admin!");
        }
    }

    public function getDelete($id) {
        ApiAdmins::deleteById($id);
        return redirect()->back()->with(['message'=>'The data has been deleted!','type'=>'warning']);
    }

}
