<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\DataTable;
use App\Http\Controllers\Controller;
use App\Models\ApiRequestLogs;
use App\Models\ApiUsers;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class ApiSettingController extends Controller
{
    public function getIndex() {
        return view('admin.api_setting.form');
    }

    public function postSave(Request $request) {
        foreach($request->all() as $key => $value) {
            update_setting($key, $value);
        }

        return go_back("The setting has been saved!","success");
    }
}
