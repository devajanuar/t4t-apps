<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ApiRequestLogs;
use App\Models\Participants;
use App\Models\TransactionTrees;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class DashboardController extends Controller
{
    public function getIndex() {
        return view('admin.dashboard.index');
    }


    public function getDataHits() {
        $from = \request('filter_from', date('Y-m').'-01');
        $to = request('filter_to', date('Y-m-t'));
        $result = DB(ApiRequestLogs::$table)
            ->whereDate("created_at",">=",$from)
            ->whereDate("created_at","<=",$to)
            ->groupBy(DB::raw("DATE(created_at)"))
            ->select(DB::raw("COUNT(id) as total"),DB::raw("DATE(created_at) as request_date"))
            ->get();

        return response()->json($result);
    }

    public function getDataTopHits() {
        $from = \request('filter_from', date('Y-m').'-01');
        $to = request('filter_to', date('Y-m-t'));
        $result = DB(ApiRequestLogs::$table)
            ->whereDate("created_at",">=",$from)
            ->whereDate("created_at","<=",$to)
            ->groupBy("url")
            ->select(DB::raw("COUNT(id) as total"),"url")
            ->take(10)
            ->get();
        foreach($result as $i=>&$item) {
            $item->url = basename($item->url);
        }

        return response()->json($result);
    }

    // Errors Chart

    public function getDataErrors() {
        $from = \request('filter_from', date('Y-m').'-01');
        $to = request('filter_to', date('Y-m-t'));
        $result = DB(ApiRequestLogs::$table)
            ->whereDate("created_at",">=",$from)
            ->whereDate("created_at","<=",$to)
            ->where("response_code", "!=", 200)
            ->groupBy(DB::raw("DATE(created_at)"))
            ->select(DB::raw("COUNT(id) as total"),DB::raw("DATE(created_at) as request_date"))
            ->get();

        return response()->json($result);
    }

    public function getDataTopErrors() {
        $from = \request('filter_from', date('Y-m').'-01');
        $to = request('filter_to', date('Y-m-t'));
        $result = DB(ApiRequestLogs::$table)
            ->whereDate("created_at",">=",$from)
            ->whereDate("created_at","<=",$to)
            ->where("response_code", "!=", 200)
            ->groupBy("url")
            ->select(DB::raw("COUNT(id) as total"),"url")
            ->take(10)
            ->get();
        foreach($result as $i=>&$item) {
            $item->url = basename($item->url);
        }

        return response()->json($result);
    }

    // Data Tree Allocated

    public function getDataTreeAllocated() {
        $from = request('filter_from', date('Y-m').'-01');
        $to = request('filter_to', date('Y-m-t'));
        $result = DB(TransactionTrees::$table)
            ->whereDate("created_at",">=",$from)
            ->whereDate("created_at","<=",$to)
            ->groupBy(DB::raw("DATE(created_at)"))
            ->select(DB::raw("SUM(quantity) as total"),DB::raw("DATE(created_at) as request_date"));

        if(request('participant_id')) {
            $participant = Participants::findById(request('participant_id'));
            $result->where("participant_no", $participant->participant_no);
        }

        return response()->json($result->get());
    }
}
