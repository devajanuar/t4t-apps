<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\AdminAuth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function getLogin() {
        return view('admin.auth.login');
    }

    public function postLogin(Request $request) {
        try {
            $this->validate($request, ['email', 'password']);

            if(AdminAuth::attempt($request->get('email'), $request->get('password'))) {
                return redirect("admin");
            } else {
                return redirect()->back()->with(['message'=>'Sorry your username and or password is wrong!','type'=>'warning']);
            }

        } catch (ValidationException $e) {
            return redirect()->back()->with(['message'=>implode(', ',$e->errors()),'type'=>'warning']);
        }
    }

    public function getLogout() {
        AdminAuth::logout();
        return redirect('admin/auth/login');
    }
}
