<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ApiLogExport;
use App\Helpers\DataTable;
use App\Http\Controllers\Controller;
use App\Models\ApiRequestLogs;
use App\Models\ApiUsers;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class ApiLogController extends Controller
{
    public function getIndex() {
        return view('admin.api_log.index');
    }

    public function getDownload() {
        $from = request('filter_from',date('Y-m').'-01');
        $to = request('filter_to', date('Y-m-t'));
        return (new ApiLogExport($from, $to))->download("api_log_data_".$from."_".$to.".xlsx");
    }

    public function list() {
        $result = new DataTable("api_request_logs");
        $result->query(function(Builder $query) {
            $query->leftJoin("api_token_logs","api_token_logs.id","=","api_tokens_id");
            $query->select("api_request_logs.*",DB::raw("IFNULL(api_token_logs.token,'Guest') as token"));
           return $query;
        });
        $result->searchable([
           "api_request_logs.ip_address",
           "api_request_logs.user_agent",
           "url"
        ]);
        $result->columns([
           "created_at",
           "url",
           "ip_address",
           "user_agent",
           "token",
           "response_code",
           "id"
        ]);
        $result->render(function($item) {
            $row = new ApiRequestLogs($item);

            $button = "";
            $button .= "<a href='javascript:;' onclick='showPayloadDetail(".$row->id.")' class='btn btn-primary btn-sm'>Payload</a> ";

            $user_agent = Str::limit($row->user_agent, 30);
            $user_agent = "<span title='".$row->user_agent."'>".$user_agent."</span>";

            return [
                nowrap_center($row->created_at),
                nowrap(parse_url($row->url, PHP_URL_PATH)),
                nowrap_center($row->ip_address),
              $user_agent,
              $row->token,
              html_response_code($row->response_code),
              nowrap_center($button)
            ];
        });

        $response = $result->get();
        return response()->json($response);
    }


    public function getPayload($id) {
        $row = ApiRequestLogs::findById($id);
        $payload = print_r(json_decode($row->payload_data, true), true);
        return response()->json(['data'=>$payload]);
    }
}
