<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use App\Exports\TreeExport;
use App\Imports\TreeImport;
use App\Tree;

class TreeController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:tree-list');
         $this->middleware('permission:tree-create', ['only' => ['create','store']]);
         $this->middleware('permission:tree-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:tree-delete', ['only' => ['destroy', 'delete']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trees = Tree::all();
        $coba = 'ttt';
        return view('tree.index', compact('trees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $test = Tree::all();
        return view('tree.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'scientific_name'=>'required',
            'tree_name' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $tree = new Tree();
        $tree->tree_code = Tree::Maxno();
        $tree->tree_name = $request->input('tree_name');
        $tree->scientific_name = $request->input('scientific_name');
        $tree->english_name = $request->input('english_name');
        $tree->common_name = $request->input('common_name');
        $tree->short_information = $request->input('short_information');
        $tree->description = $request->input('description');
        $tree->tree_category = $request->input('tree_category');
        $tree->product_list = $request->input('product_list');
        $tree->estimate_income = $request->input('estimate_income');
        $tree->co2_capture = $request->input('co2_capture');
        $tree->save();

        if (!$tree) {
            return redirect()->back()->withInput()->withError('cannot create Tree');
        }else{
            return redirect('/tree')->with('success', 'Successfully create Tree');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tree = Tree::find($id);
        return view('tree.edit', compact('tree'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'scientific_name'=>'required',
            'tree_name' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $tree = Tree::find($id); 
        $tree->tree_name = $request->input('tree_name');
        $tree->scientific_name = $request->input('scientific_name');
        $tree->english_name = $request->input('english_name');
        $tree->common_name = $request->input('common_name');
        $tree->short_information = $request->input('short_information');
        $tree->description = $request->input('description');
        $tree->tree_category = $request->input('tree_category');
        $tree->product_list = $request->input('product_list');
        $tree->estimate_income = $request->input('estimate_income');
        $tree->co2_capture = $request->input('co2_capture');
        $tree->save();

        if (!$tree) {
            return redirect()->back()->withInput()->withError('cannot create Tree');
        }else{
            return redirect('/tree')->with('success', 'Successfully create Tree');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        foreach ($request->input('id') as $key => $value) {
            $tree = Tree::find($value);
            $tree->delete();
        }

        if (!$tree) {
            return redirect()->back()->withError('cannot delete Tree');
        }else{
            return redirect()->back()->with('success', 'Successfully delete Tree');
        }
    }

    public function export_excel()
    {
        return Excel::download(new TreeExport, 'tree.xlsx');
    }

    function import(Request $request)
    {
        $this->validate($request, [
            'file'  => 'required|mimes:xls,xlsx'
        ]);
        Excel::import(new TreeImport,request()->file('file'));
        return back()->with('success', 'Excel Data Tree Imported successfully.');;
     

     // $path = $request->file('select_file')->getRealPath();

     // $data = Excel::load($path)->get();

     // if($data->count() > 0)
     // {
     //  foreach($data->toArray() as $key => $value)
     //  {
     //   foreach($value as $row)
     //   {
     //    $insert_data[] = array(
     //     'id'  => $row['id'],
     //     'farmer_no'   => $row['farmer_no'],
     //     'name'   => $row['name'],
     //     'birthday'    => $row['birthday'],
     //     'religion'  => $row['religion'],
     //     'ethnic'   => $row['ethnic'],
     //     'origin'  => $row['origin'],
     //     'gender'   => $row['gender'],
     //     'join_date'   => $row['join_date'],
     //     'number_family_member'    => $row['number_family_member'],
     //     'ktp_no'  => $row['ktp_no'],
     //     'phone'   => $row['phone'],         
     //     'address'  => $row['address'],
     //     'village'   => $row['village'],
     //     'kecamatan'   => $row['kecamatan'],
     //     'city'    => $row['city'],
     //     'province'  => $row['province'],
     //     'post_code'   => $row['post_code'],
     //     'mu_no'  => $row['mu_no'],
     //     'group_no'   => $row['group_no'],
     //     'mou_no'   => $row['mou_no'],
     //     'main_income'    => $row['main_income'],
     //     'side_income'  => $row['side_income'],
     //     'active'   => $row['active'],
     //     'user_id' => $row['user_id'],
     //     'created_at' => $row['created_at'],
     //     'updated_at' => $row['updated_at']
     //    );
     //   }
     //  }

     //  if(!empty($insert_data))
     //  {
     //   DB::table('farmers')->insert($insert_data);
     //  }
     // }
     // return back()->with('success', 'Excel Data Farmer Imported successfully.');
    }
}
