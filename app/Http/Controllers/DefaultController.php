<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DefaultController extends Controller
{
    //
    public function getIndex() {
        return redirect("admin/auth/login");
    }
}
