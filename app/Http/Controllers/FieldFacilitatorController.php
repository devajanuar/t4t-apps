<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\FieldFacilitator;
use App\Managementunit;
use App\Province;
use App\Kabupaten;
use App\Kecamatan;
use App\Desa;
use DB;
use File;
use Image;
use Maatwebsite\Excel\Facades\Excel;
use Ramsey\Uuid\Uuid;

class FieldFacilitatorController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:ff-list');
         $this->middleware('permission:ff-create', ['only' => ['create','store']]);
         $this->middleware('permission:ff-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:ff-delete', ['only' => ['destroy', 'delete']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $begin = $request->input('begin');
        $end = $request->input('end');
        $today = date('Y-m-d');
        
        if(!$begin && !$end)
        {
            $now = date('Y-m-');
            $d1 = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
            $begin = new \DateTime($now.'01');
            $end = new \DateTime($now.$d1);
        }else{
            $begin = new \DateTime($begin);
            $end = new \DateTime($end);
        }

        if (empty($request->status)) {
            $ff = FieldFacilitator::orderBy('join_date', 'desc')
                                    ->whereDate('join_date', '>=', $begin)
                                    ->whereDate('join_date','<=', $end)
                                    ->get();
        }else{
            $ff = FieldFacilitator::orderBy('join_date', 'desc')
                                    ->whereDate('join_date', '>=', $begin)
                                    ->whereDate('join_date','<=', $end)
                                    ->where('status', $request->status)
                                    ->get();
        }

        $fieldfacilitators = FieldFacilitator::all();
        return view('fieldfacilitator.index', compact('fieldfacilitators', 'ff', 'begin', 'end', 'today'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = Kabupaten::pluck('name', 'name');
        $kecamatans = Kecamatan::pluck('name', 'name');
        $villages = Desa::pluck('name', 'name');

        $state = Province::pluck('name', 'province_code');
        $management_unit = Managementunit::pluck('name', 'mu_no');

        $province = DB::table('provinces')->pluck('name', 'province_code');

        return view('fieldfacilitator.create', compact('state', 'province', 'management_unit', 'cities', 'kecamatans', 'villages'));
    }

    public function getCity($id)
    {
      $city = DB::table('kabupatens')->where("province_code", $id)->pluck('name', 'kabupaten_no');
      return json_encode($city);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required',
            'ktp_no'=>'required|string|unique:field_facilitators',
            // 'filename' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $photo = null;
        if ($request->hasFile('photo')) {
            $photo = $this->saveFile($request->ff_no, $request->file('photo'));
        }

        $ff = FieldFacilitator::create([
            'ff_no' => $request->input('ff_no'),
            'name' => $request->input('name'),
            'birthday' => $request->input('birthday'),
            'religion' => $request->input('religion'),
            'gender' => $request->input('gender'),
            'join_date'=> $request->input('join_date'),
            'phone'=> $request->input('phone'),
            'ktp_no'=> $request->input('ktp_no'),
            'address'=> $request->input('address'),
            'village'=> $request->input('village'),
            'kecamatan'=> $request->input('kecamatan'),
            'city'=> $request->input('city'),
            'province'=> $request->input('province'),
            'post_code'=> $request->input('post_code'),
            'bank_account'=> $request->input('bank_account'),
            'bank_branch'=> $request->input('bank_branch'),
            'bank_name'=>$request->input('bank_name'),
            'active'=> 1,
            'user_id'=> auth()->user()->id,
            'ff_photo'=> $photo,
        ]);

        if (!$ff) {
            return redirect()->back()->withInput()->withError('cannot create Field Facilitaor');
        }else{
            return redirect('/fieldfacilitator')->with('success', 'Successfully create Field Facilitaor');
        }
    }

    private function saveFile($ff_no, $photo)
    {
        $images = str_slug($ff_no) . '_' . time() . '.' . $photo->getClientOriginalExtension();
        $path = public_path('uploads/fieldfacilitator');

        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true, true);
        } 
        Image::make($photo)->save($path . '/' . $images);
        return $images;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fieldfacilitator = Farmer::find($id);
        return view('fieldfacilitator.show', compact('fieldfacilitator'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ff = FieldFacilitator::find($id);

        $cities = Kabupaten::pluck('name', 'name');
        $kecamatans = Kecamatan::pluck('name', 'name');
        $villages = Desa::pluck('name', 'name');

        $state = Province::pluck('name', 'province_code');
        $management_unit = Managementunit::pluck('name', 'mu_no');

        $province = DB::table('provinces')->pluck('name', 'province_code');


        return view('fieldfacilitator.edit', compact('ff', 'state', 'management_unit', 'cities', 'kecamatans', 'villages', 'province'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required',
            'ktp_no'=>'required|string|unique:ffs',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $ff = FieldFacilitator::findOrFail($id);
        $photo = $ff->photo;

        if ($request->hasFile('photo')) {
            !empty($photo) ? File::delete(public_path('uploads/fieldfacilitator/' . $photo)):null;
            $photo = $this->saveFile($request->ff_no, $request->file('photo'));
        }

        $ff->update([
            'ff_no' => $request->input('ff_no'),
            'name' => $request->input('name'),
            'birthday' => $request->input('birthday'),
            'religion' => $request->input('religion'),
            'gender' => $request->input('gender'),
            'join_date'=> $request->input('join_date'),
            'phone'=> $request->input('phone'),
            'ktp_no'=> $request->input('ktp_no'),
            'address'=> $request->input('address'),
            'village'=> $request->input('village'),
            'kecamatan'=> $request->input('kecamatan'),
            'city'=> $request->input('city'),
            'province'=> $request->input('province'),
            'post_code'=> $request->input('post_code'),
            'mu_no'=>$request->input('mu_no'),
            'target_area'=> $request->input('target_area'),
            'bank_account'=> $request->input('bank_account'),
            'bank_branch'=> $request->input('bank_branch'),
            'bank_name'=>$request->input('bank_name'),
            'active'=> 1,
            'user_id'=> auth()->user()->id,
            'ff_photo'=> $photo,
        ]);

        if (!$ff) {
            return redirect()->back()->withInput()->withError('cannot create Field Facilitaor');
        }else{
            return redirect('/fieldfacilitator')->with('success', 'Successfully update Field Facilitaor');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
