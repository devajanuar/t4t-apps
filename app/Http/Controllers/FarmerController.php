<?php

namespace App\Http\Controllers;

use App\Lahan;
use App\Farmer;
use App\FarmerStory;
use App\FarmerTestimonial;
use App\FarmerGroup;
use App\Managementunit;
use App\Kabupaten;
use App\Kecamatan;
use App\Desa;
use App\Exports\FarmerExport;
use App\Imports\FarmerImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use File;
use Image;

class FarmerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:farmer-list');
         $this->middleware('permission:farmer-create', ['only' => ['create','store']]);
         $this->middleware('permission:farmer-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:farmer-delete', ['only' => ['destroy', 'delete']]);
    }

    public function index(Request $request)
    {

        $begin = $request->input('begin');
        $end = $request->input('end');
        $today = date('Y-m-d');
        
        if(!$begin && !$end)
        {
            $now = date('Y-m-');
            $d1 = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
            $begin = new \DateTime($now.'01');
            $end = new \DateTime($now.$d1);
        }else{
            $begin = new \DateTime($begin);
            $end = new \DateTime($end);
        }

        if (empty($request->status)) {
            $far = Farmer::orderBy('join_date', 'desc')
                                    ->whereDate('join_date', '>=', $begin)
                                    ->whereDate('join_date','<=', $end)
                                    ->get();
        }else{
            $far = Farmer::orderBy('join_date', 'desc')
                                    ->whereDate('join_date', '>=', $begin)
                                    ->whereDate('join_date','<=', $end)
                                    ->where('status', $request->status)
                                    ->get();
        }

        $story_no = FarmerStory::Maxno();

        $farmer = Farmer::all();
        return view('farmer.index', compact('story_no', 'farmer', 'far', 'begin', 'end', 'today'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = Kabupaten::pluck('name', 'name');
        $kecamatans = Kecamatan::pluck('name', 'name');
        $villages = Desa::pluck('name', 'name');

        $fg = FarmerGroup::pluck('name', 'group_no');
        $farmers = new Farmer();
        $story_no = FarmerStory::Maxno();
        $state = ['jateng' => 'Jawa Tengah',
                  'jabar'  => 'Jawa Barat',
                  'jatim' => 'Jawa Timur',
                  'banten' => 'Banten',
                  'diy' => 'Daerah Istimewa Yogyakarta',
                  'bali' => 'Bali',
                  'dki' => 'DKI Jakarta' ];
        $management_unit = Managementunit::pluck('name', 'mu_no');

        return view('farmer.create', compact('story_no', 'farmers', 'state', 'management_unit', 'cities', 'fg', 'kecamatans', 'villages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required',
            'photo' => 'nullable|image|mimes:jpg,png,jpeg'
            // 'ktp_no'=>'required|string|unique:farmers',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $farmer = new Farmer();
        $farmer->farmer_no = Farmer::Maxno();
        $farmer->name = $request->input('name');
        $farmer->birthday = $request->input('birthday');
        $farmer->religion = $request->input('religion');
        $farmer->gender = $request->input('gender');
        $farmer->ethnic = $request->input('ethnic');
        $farmer->origin = $request->input('origin');
        $farmer->join_date = $request->input('join_date');
        $farmer->number_family_member = $request->input('number_family_member');
        $farmer->phone = $request->input('phone');
        $farmer->ktp_no = $request->input('ktp_no');
        $farmer->address = $request->input('address');
        $farmer->village = $request->input('village');
        $farmer->kecamatan = $request->input('kecamatan');
        $farmer->city = $request->input('city');
        $farmer->province = $request->input('province');
        $farmer->post_code = $request->input('post_code');
        $farmer->mou_no = $request->input('mou_no');
        $farmer->mu_no = $request->input('mu_no');
        $farmer->group_no = $request->input('group_no');
        $farmer->main_income = $request->input('main_income');
        $farmer->side_income = $request->input('side_income');
        $farmer->active = '1';
        // $farmer->user_id = '1';
        $farmer->user_id = auth()->user()->id;
        $farmer->farmer_profile = null;
        if ($request->hasFile('farmer_profile')) {
            $farmer->farmer_profile = $this->saveFile($request->name, $request->file('farmer_profile'));
        }

        $farmer->ktp_document = null;
        if ($request->hasFile('ktp_document')) {
            $farmer->ktp_document = $this->saveFile2($request->name, $request->file('ktp_document'));
        }
        $farmer->save();

        return $farmer;

        $this->farmer_stories( 
            $farmer->farmer_no,
            $request->story_no,
            $request->story_title,
            $request->year,
            $request->story_description,
            1);

        if (!$farmer) {
            return redirect()->back()->withInput()->withError('cannot create farmer');
        }else{
            return redirect('/farmer')->with('success', 'Successfully create farmer');
        }
    }

    private function saveFile($name, $photo)
    {
        $images = str_slug($name) . '_' . time() . '.' . $photo->getClientOriginalExtension();
        $path = public_path('uploads/farmer/profil');

        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true, true);
        } 
        Image::make($photo)->save($path . '/' . $images);
        return $images;
    }

    private function saveFile2($ktp_no, $photo)
    {
        $images = str_slug($ktp_no) . '_' . time() . '.' . $photo->getClientOriginalExtension();
        $path = public_path('uploads/farmer/ktp');

        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true, true);
        } 
        Image::make($photo)->save($path . '/' . $images);
        return $images;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lahanlist = Lahan::with('farmer')->where('farmer_no', $id)->get();

        return view('farmer.show', compact('lahanlist'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $farmer = Farmer::with('farmer_stories')->with('farmer_testimonial')->where('id', $id)->first();

        $cities = Kabupaten::pluck('name', 'name');
        $kecamatans = Kecamatan::pluck('name', 'name');
        $villages = Desa::pluck('name', 'name');

        $fg = FarmerGroup::pluck('name', 'group_no');
        $farmers = new Farmer();
        $story_no = FarmerStory::Maxno();
        $state = ['jateng' => 'Jawa Tengah',
                  'jabar'  => 'Jawa Barat',
                  'jatim' => 'Jawa Timur',
                  'banten' => 'Banten',
                  'diy' => 'Daerah Istimewa Yogyakarta',
                  'bali' => 'Bali',
                  'dki' => 'DKI Jakarta' ];
        $management_unit = Managementunit::pluck('name', 'mu_no');

        return view('farmer.edit', compact('story_no', 'farmer', 'state', 'management_unit', 'cities', 'kecamatans', 'villages', 'fg'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required',
            // 'ktp_no'=>'required|string|unique:farmers',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages();
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $farmer = Farmer::find($id);
        $farmer->name = $request->input('name');
        $farmer->birthday = $request->input('birthday');
        $farmer->religion = $request->input('religion');
        $farmer->gender = $request->input('gender');
        $farmer->ethnic = $request->input('ethnic');
        $farmer->origin = $request->input('origin');
        $farmer->join_date = $request->input('join_date');
        $farmer->number_family_member = $request->input('number_family_member');
        $farmer->phone = $request->input('phone');
        $farmer->ktp_no = $request->input('ktp_no');
        $farmer->address = $request->input('address');
        $farmer->village = $request->input('village');
        $farmer->kecamatan = $request->input('kecamatan');
        $farmer->city = $request->input('city');
        $farmer->province = $request->input('province');
        $farmer->post_code = $request->input('post_code');
        $farmer->mou_no = $request->input('mou_no');
        $farmer->mu_no = $request->input('mu_no');
        $farmer->group_no = $request->input('group_no');
        $farmer->main_income = $request->input('main_income');
        $farmer->side_income = $request->input('side_income');
        $farmer->active = '1';
        $farmer->user_id = '1';
        // $farmer->user_id = auth()->user()->username;
        $farmer->save();

        if (!$farmer) {
            return redirect()->back()->withInput()->withError('cannot create farmer');
        }else{
            return redirect('/farmer')->with('success', 'Successfully create farmer');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listLahan($id, Request $request)
    {
        // $lahanlist = Lahan::where('farmer_no', $f->farmer_no)->get();

        // return $lahanlist;
        // return view('farmer.lahanlist', compact('lahanlist'));
    }

    public function delete(Request $request)
    {
        foreach ($request->input('id') as $value) {
            Farmer::find($value)->delete();
        }
        return redirect()->back()->with('Success', "Farmer deleted Successfully");
    }

    public function export_excel()
    {
        return Excel::download(new FarmerExport, 'farmer.xlsx');
    }

    public function import(Request $request)
    {
        $this->validate($request, [
            'file'  => 'required|mimes:xls,xlsx'
        ]);
        Excel::import(new FarmerImport,request()->file('file'));
        return back()->with('success', 'Excel Data Farmer Imported successfully.');;
    }

    public function farmer_stories($farmer_no, $story_no, $title, $year, $description, $active)
    {
        $farmerstory = FarmerStory::where('story_no', $story_no)
                ->where('farmer_no', $farmer_no)
                ->where('story_title', $title)
                ->where('year', $year)
                ->where('story_description', $description)
                ->where('active',$active)
                ->first();
        
        if($farmerstory){
            $farmerstory->story_no = $story_no;
            $farmerstory->story_title = $title;
            $farmerstory->year = $year;
            $farmerstory->story_description = $description;
            $farmerstory->active = $active;
            $farmerstory->save();
        }else{
            if(!empty($story_no)){
                $farmerstory = FarmerStory::create([
                    'farmer_no' => $farmer_no,
                    'story_no'=> $story_no,
                    'story_title'=>$title,
                    'year'=>$year,
                    'story_description'=>$description,
                ]);
            }
        }
        return $farmerstory;
    }
}
