<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use App\Province;

class ProvinceController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:province-list');
         $this->middleware('permission:province-create', ['only' => ['create','store']]);
         $this->middleware('permission:province-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:province-delete', ['only' => ['destroy', 'delete']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provinces = Province::all();
        return view('region.province.index', compact('provinces'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'province_code'=>'required|string|max:50|unique:provinces',
            'name'=>'required|string|max:50',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $prov = new Province();
        $prov->province_code = $request->province_code;
        $prov->name = $request->name;
        $prov->save();

        if (!$prov) {
            return redirect()->back()->withInput()->withError('cannot create province');
        }else{
            return redirect()->back()->with('success', 'Successfully create province');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required|string',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $prov = Province::find($id);
        $prov->update($request->all());

        if (!$prov) {
            return redirect()->back()->withError('cannot update province');
        }else{
            return redirect()->back()->with('success', 'Successfully update province');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        foreach ($request->input('id') as $key => $value) {
            $prov = Province::find($value);
            $prov->delete();
        }

        if (!$prov) {
            return redirect()->back()->withError('cannot delete province');
        }else{
            return redirect()->back()->with('success', 'Successfully delete province');
        }
    }
}
