<?php

namespace App\Http\Controllers;

use App\Bank;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;



class BankController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:bank-list');
         $this->middleware('permission:bank-create', ['only' => ['create','store']]);
         $this->middleware('permission:bank-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:bank-delete', ['only' => ['destroy', 'delete']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banks = Bank::all();
        return view('master.bank.index', compact('banks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'bank_code'=>'required|string|max:50|unique:banks',
            'name'=>'required|string|max:50|unique:banks',
            'alias'=>'required'
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $bank = new Bank();
        $bank->id = Uuid::uuid4()->getHex();
        $bank->bank_code = $request->bank_code;
        $bank->name = $request->name;
        $bank->alias = $request->alias;
        $bank->address = $request->bank_address;
        $bank->phone = $request->bank_phone;
        $bank->email = $request->bank_email;
        $bank->save();

        if (!$bank) {
            return redirect()->back()->withInput()->withError('cannot create bank');
        }else{
            return redirect()->back()->with('success', 'Successfully create bank');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required|string',
            'alias'=>'required|string'
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $bank = Bank::find($id);
        $bank->update($request->all());

        if (!$bank) {
            return redirect()->back()->withError('cannot update bank');
        }else{
            return redirect()->back()->with('success', 'Successfully update bank');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        foreach ($request->input('id') as $key => $value) {
            $bank = Bank::find($value);
            $bank->delete();
        }

        if (!$bank) {
            return redirect()->back()->withError('cannot delete bank');
        }else{
            return redirect()->back()->with('success', 'Successfully delete bank');
        }
    }
}
