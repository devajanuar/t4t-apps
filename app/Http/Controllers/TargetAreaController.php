<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use App\TargetArea;
use App\Managementunit;

class TargetAreaController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:ta-list');
         $this->middleware('permission:ta-create', ['only' => ['create','store']]);
         $this->middleware('permission:ta-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:ta-delete', ['only' => ['destroy', 'delete']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $targetareas = TargetArea::all();
        $management_unit = Managementunit::pluck('name', 'mu_no');
        return view('management_unit.target_area.index', compact('targetareas', 'management_unit'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'area_code'=>'required|string|max:50|unique:target_areas',
            'name'=>'required|string|max:50|unique:target_areas',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $ta = new TargetArea();
        $ta->area_code = $request->area_code;
        $ta->name = $request->name;
        $ta->mu_no = $request->mu_no;
        $ta->active = 1;
        $ta->save();

        if (!$ta) {
            return redirect()->back()->withInput()->withError('cannot create target area');
        }else{
            return redirect()->back()->with('success', 'Successfully create target area');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required|string',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $ta = TargetArea::find($id);
        $ta->update($request->all());

        if (!$ta) {
            return redirect()->back()->withError('cannot update target area');
        }else{
            return redirect()->back()->with('success', 'Successfully update target area');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        foreach ($request->input('id') as $key => $value) {
            $ta = TargetArea::find($value);
            $ta->delete();
        }

        if (!$ta) {
            return redirect()->back()->withError('cannot delete target area');
        }else{
            return redirect()->back()->with('success', 'Successfully delete target area');
        }
    }
}
