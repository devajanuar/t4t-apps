<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use App\Managementunit;

class ManagementUnitController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:mu-list');
         $this->middleware('permission:mu-create', ['only' => ['create','store']]);
         $this->middleware('permission:mu-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:mu-delete', ['only' => ['destroy', 'delete']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $managementunits = Managementunit::all();
        return view('management_unit.index', compact('managementunits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mu_no'=>'required|string|max:50|unique:managementunits',
            'name'=>'required|string|max:50|unique:managementunits',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $mu = new Managementunit();
        $mu->mu_no = $request->mu_no;
        $mu->name = $request->name;
        $mu->active = 1;
        $mu->save();

        if (!$mu) {
            return redirect()->back()->withInput()->withError('cannot create management unit');
        }else{
            return redirect()->back()->with('success', 'Successfully create management unit');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required|string',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $mu = Managementunit::find($id);
        $mu->update($request->all());

        if (!$mu) {
            return redirect()->back()->withError('cannot update managmeent unit');
        }else{
            return redirect()->back()->with('success', 'Successfully update managmeent unit');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        foreach ($request->input('id') as $key => $value) {
            $mu = Managementunit::find($value);
            $mu->delete();
        }

        if (!$mu) {
            return redirect()->back()->withError('cannot delete management unit');
        }else{
            return redirect()->back()->with('success', 'Successfully delete management unit');
        }
    }
}
