<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use File;
use Image;
use App\Exports\ParticipantExport;
use App\Imports\ParticipantImport;
use App\Participant;

class ParticipantController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:participant-list');
         $this->middleware('permission:participant-create', ['only' => ['create','store']]);
         $this->middleware('permission:participant-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:participant-delete', ['only' => ['destroy', 'delete']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $begin = $request->input('begin');
        $end = $request->input('end');
        $today = date('Y-m-d');
        
        if(!$begin && !$end)
        {
            $now = date('Y-m-');
            $d1 = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
            $begin = new \DateTime($now.'01');
            $end = new \DateTime($now.$d1);
        }else{
            $begin = new \DateTime($begin);
            $end = new \DateTime($end);
        }

        if (empty($request->status)) {
            $par = Participant::orderBy('join_date', 'desc')
                                    ->whereDate('join_date', '>=', $begin)
                                    ->whereDate('join_date','<=', $end)
                                    ->get();
        }else{
            $par = Participant::orderBy('join_date', 'desc')
                                    ->whereDate('join_date', '>=', $begin)
                                    ->whereDate('join_date','<=', $end)
                                    ->where('status', $request->status)
                                    ->get();
        }

        $participants = Participant::all();
        return view('participant.index', compact('participants', 'par', 'begin', 'end', 'today'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries =array( "AF" => "Afghanistan", "AL" => "Albania", "DZ" => "Algeria",
                "AS" => "American Samoa", "AD" => "Andorra", "AO" => "Angola",
                "AI" => "Anguilla", "AQ" => "Antarctica", "AG" => "Antigua and Barbuda", 
                "AR" => "Argentina", "AM" => "Armenia", "AW" => "Aruba",
                "AU" => "Australia", "AT" => "Austria", "AZ" => "Azerbaijan",
                "BS" => "Bahamas", "BH" => "Bahrain", "BD" => "Bangladesh",
                "BB" => "Barbados", "BY" => "Belarus", "BE" => "Belgium",
                "BZ" => "Belize", "BJ" => "Benin", "BM" => "Bermuda",
                "BT" => "Bhutan", "BO" => "Bolivia", "BA" => "Bosnia and Herzegovina",
                "BW" => "Botswana", "BV" => "Bouvet Island", "BR" => "Brazil",
                "IO" => "British Indian Ocean Territory", "BN" => "Brunei Darussalam", "BG" => "Bulgaria",
                "BF" => "Burkina Faso", "BI" => "Burundi", "KH" => "Cambodia",
                "CM" => "Cameroon", "CA" => "Canada", "CV" => "Cape Verde",
                "KY" => "Cayman Islands", "CF" => "Central African Republic", "TD" => "Chad",
                "CL" => "Chile", "CN" => "China", "CX" => "Christmas Island",
                "CC" => "Cocos (Keeling) Islands", "CO" => "Colombia", "KM" => "Comoros",
                "CG" => "Congo", "CD" => "Congo, the Democratic Republic of the", "CK" => "Cook Islands",
                "CR" => "Costa Rica", "CI" => "Cote D'Ivoire", "HR" => "Croatia",
                "CU" => "Cuba", "CY" => "Cyprus", "CZ" => "Czech Republic",
                "DK" => "Denmark", "DJ" => "Djibouti", "DM" => "Dominica",
                "DO" => "Dominican Republic", "EC" => "Ecuador", "EG" => "Egypt",
                "SV" => "El Salvador", "GQ" => "Equatorial Guinea", "ER" => "Eritrea",
                "EE" => "Estonia", "ET" => "Ethiopia", "FK" => "Falkland Islands (Malvinas)",
                "FO" => "Faroe Islands", "FJ" => "Fiji", "FI" => "Finland",
                "FR" => "France", "GF" => "French Guiana", "PF" => "French Polynesia",
                "TF" => "French Southern Territories", "GA" => "Gabon", "GM" => "Gambia",
                "GE" => "Georgia", "DE" => "Germany", "GH" => "Ghana",
                "GI" => "Gibraltar", "GR" => "Greece", "GL" => "Greenland", "GD" => "Grenada",
                "GP" => "Guadeloupe", "GU" => "Guam", "GT" => "Guatemala", "GN" => "Guinea",
                "GW" => "Guinea-Bissau", "GY" => "Guyana", "HT" => "Haiti",
                "HM" => "Heard Island and Mcdonald Islands", "VA" => "Holy See (Vatican City State)",
                "HN" => "Honduras", "HK" => "Hong Kong", "HU" => "Hungary", "IS" => "Iceland", 
                "IN" => "India", "ID" => "Indonesia", "IR" => "Iran, Islamic Republic of", 
                "IQ" => "Iraq", "IE" => "Ireland", "IL" => "Israel",
                "IT" => "Italy", "JM" => "Jamaica", "JP" => "Japan", "JO" => "Jordan", "KZ" => "Kazakhstan",
                "KE" => "Kenya", "KI" => "Kiribati", "KP" => "Korea, Democratic People's Republic of",
                "KR" => "Korea, Republic of", "KW" => "Kuwait", "KG" => "Kyrgyzstan", "LA" => "Laos",
                "LV" => "Latvia", "LB" => "Lebanon", "LS" => "Lesotho", "LR" => "Liberia", "LY" => "Libyan Arab Jamahiriya",
                "LI" => "Liechtenstein", "LT" => "Lithuania", "LU" => "Luxembourg", "MO" => "Macao",
                "MK" => "Macedonia, the Former Yugoslav Republic of", "MG" => "Madagascar", "MW" => "Malawi",
                "MY" => "Malaysia", "MV" => "Maldives", "ML" => "Mali", "MT" => "Malta", "MH" => "Marshall Islands",
                "MQ" => "Martinique", "MR" => "Mauritania", "MU" => "Mauritius", "YT" => "Mayotte", "MX" => "Mexico",
                "FM" => "Micronesia, Federated States of", "MD" => "Moldova, Republic of", "MC" => "Monaco",
                "MN" => "Mongolia", "MS" => "Montserrat", "MA" => "Morocco", "MZ" => "Mozambique",
                "MM" => "Myanmar", "NA" => "Namibia", "NR" => "Nauru", "NP" => "Nepal", "NL" => "Netherlands",
                "AN" => "Netherlands Antilles", "NC" => "New Caledonia", "NZ" => "New Zealand", "NI" => "Nicaragua",
                "NE" => "Niger", "NG" => "Nigeria", "NU" => "Niue", "NF" => "Norfolk Island",
                "MP" => "Northern Mariana Islands", "NO" => "Norway", "OM" => "Oman", "PK" => "Pakistan", "PW" => "Palau",
                "PS" => "Palestinian Territory, Occupied", "PA" => "Panama", "PG" => "Papua New Guinea", "PY" => "Paraguay",
                "PE" => "Peru", "PH" => "Philippines", "PN" => "Pitcairn", "PL" => "Poland", "PT" => "Portugal",
                "PR" => "Puerto Rico", "QA" => "Qatar", "RE" => "Reunion", "RO" => "Romania", "RU" => "Russian Federation",
                "RW" => "Rwanda", "SH" => "Saint Helena", "KN" => "Saint Kitts and Nevis", "LC" => "Saint Lucia",
                "PM" => "Saint Pierre and Miquelon", "VC" => "Saint Vincent and the Grenadines", "WS" => "Samoa",
                "SM" => "San Marino", "ST" => "Sao Tome and Principe", "SA" => "Saudi Arabia", "SN" => "Senegal",
                "CS" => "Serbia and Montenegro", "SC" => "Seychelles", "SL" => "Sierra Leone", "SG" => "Singapore",
                "SK" => "Slovakia", "SI" => "Slovenia", "SB" => "Solomon Islands", "SO" => "Somalia",
                "ZA" => "South Africa", "GS" => "South Georgia and the South Sandwich Islands", "ES" => "Spain",
                "LK" => "Sri Lanka", "SD" => "Sudan", "SR" => "Suriname", "SJ" => "Svalbard and Jan Mayen",
                "SZ" => "Swaziland", "SE" => "Sweden", "CH" => "Switzerland", "SY" => "Syrian Arab Republic",
                "TW" => "Taiwan, Province of China", "TJ" => "Tajikistan", "TZ" => "Tanzania, United Republic of",
                "TH" => "Thailand", "TL" => "Timor-Leste", "TG" => "Togo", "TK" => "Tokelau", "TO" => "Tonga",
                "TT" => "Trinidad and Tobago", "TN" => "Tunisia", "TR" => "Turkey", "TM" => "Turkmenistan",
                "TC" => "Turks and Caicos Islands", "TV" => "Tuvalu", "UG" => "Uganda",
                "UA" => "Ukraine", "AE" => "United Arab Emirates", "GB" => "United Kingdom",
                "US" => "United States of America", "UM" => "United States Minor Outlying Islands","UY" => "Uruguay",
                "UZ" => "Uzbekistan", "VU" => "Vanuatu", "VE" => "Venezuela",
                "VN" => "Vietnam", "VG" => "Virgin Islands, British", "VI" => "Virgin Islands, U.s.",
                "WF" => "Wallis and Futuna", "EH" => "Western Sahara", "YE" => "Yemen",
                "ZM" => "Zambia", "ZW" => "Zimbabwe"
                );
        return view('participant.create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $photo = null;
        if ($request->hasFile('photo')) {
            $photo = $this->saveFile($request->first_name, $request->file('photo'));
        }

        $participant = Participant::create([
            'participant_no' => $request->input('participant_no'),
            'category' => $request->input('category'),
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'company' => $request->input('company'),
            'address1' => $request->input('address1'),
            'address2' => $request->input('address2'),
            'city'=> $request->input('city'),
            'state'=> $request->input('state'),
            'postal_code'=> $request->input('postal_code'),
            'country'=> $request->input('country'),
            'email'=> $request->input('email'),
            'website'=> $request->input('website'),
            'phone'=> $request->input('phone'),
            'join_date'=> $request->input('join_date'),
            'active'=> 1,
            'comment'=> '-',
            'source_of_contact'=> '-',
            'user_id'=> auth()->user()->name,
            'photo'=> $photo,
        ]);

        if (!$participant) {
            return redirect()->back()->withInput()->withError('cannot create Participant');
        }else{
            return redirect('/participant')->with('success', 'Successfully create Participant');
        }
    }

    private function saveFile($first_name, $photo)
    {
        $images = str_slug($first_name) . '_' . time() . '.' . $photo->getClientOriginalExtension();
        $path = public_path('uploads/participant');

        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true, true);
        } 
        Image::make($photo)->save($path . '/' . $images);
        return $images;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $participant = Participant::find($id);
        return view('participant.show', compact('participant'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $participant = Participant::find($id);

        $countries =array( "AF" => "Afghanistan", "AL" => "Albania", "DZ" => "Algeria",
                "AS" => "American Samoa", "AD" => "Andorra", "AO" => "Angola",
                "AI" => "Anguilla", "AQ" => "Antarctica", "AG" => "Antigua and Barbuda", 
                "AR" => "Argentina", "AM" => "Armenia", "AW" => "Aruba",
                "AU" => "Australia", "AT" => "Austria", "AZ" => "Azerbaijan",
                "BS" => "Bahamas", "BH" => "Bahrain", "BD" => "Bangladesh",
                "BB" => "Barbados", "BY" => "Belarus", "BE" => "Belgium",
                "BZ" => "Belize", "BJ" => "Benin", "BM" => "Bermuda",
                "BT" => "Bhutan", "BO" => "Bolivia", "BA" => "Bosnia and Herzegovina",
                "BW" => "Botswana", "BV" => "Bouvet Island", "BR" => "Brazil",
                "IO" => "British Indian Ocean Territory", "BN" => "Brunei Darussalam", "BG" => "Bulgaria",
                "BF" => "Burkina Faso", "BI" => "Burundi", "KH" => "Cambodia",
                "CM" => "Cameroon", "CA" => "Canada", "CV" => "Cape Verde",
                "KY" => "Cayman Islands", "CF" => "Central African Republic", "TD" => "Chad",
                "CL" => "Chile", "CN" => "China", "CX" => "Christmas Island",
                "CC" => "Cocos (Keeling) Islands", "CO" => "Colombia", "KM" => "Comoros",
                "CG" => "Congo", "CD" => "Congo, the Democratic Republic of the", "CK" => "Cook Islands",
                "CR" => "Costa Rica", "CI" => "Cote D'Ivoire", "HR" => "Croatia",
                "CU" => "Cuba", "CY" => "Cyprus", "CZ" => "Czech Republic",
                "DK" => "Denmark", "DJ" => "Djibouti", "DM" => "Dominica",
                "DO" => "Dominican Republic", "EC" => "Ecuador", "EG" => "Egypt",
                "SV" => "El Salvador", "GQ" => "Equatorial Guinea", "ER" => "Eritrea",
                "EE" => "Estonia", "ET" => "Ethiopia", "FK" => "Falkland Islands (Malvinas)",
                "FO" => "Faroe Islands", "FJ" => "Fiji", "FI" => "Finland",
                "FR" => "France", "GF" => "French Guiana", "PF" => "French Polynesia",
                "TF" => "French Southern Territories", "GA" => "Gabon", "GM" => "Gambia",
                "GE" => "Georgia", "DE" => "Germany", "GH" => "Ghana",
                "GI" => "Gibraltar", "GR" => "Greece", "GL" => "Greenland", "GD" => "Grenada",
                "GP" => "Guadeloupe", "GU" => "Guam", "GT" => "Guatemala", "GN" => "Guinea",
                "GW" => "Guinea-Bissau", "GY" => "Guyana", "HT" => "Haiti",
                "HM" => "Heard Island and Mcdonald Islands", "VA" => "Holy See (Vatican City State)",
                "HN" => "Honduras", "HK" => "Hong Kong", "HU" => "Hungary", "IS" => "Iceland", 
                "IN" => "India", "ID" => "Indonesia", "IR" => "Iran, Islamic Republic of", 
                "IQ" => "Iraq", "IE" => "Ireland", "IL" => "Israel",
                "IT" => "Italy", "JM" => "Jamaica", "JP" => "Japan", "JO" => "Jordan", "KZ" => "Kazakhstan",
                "KE" => "Kenya", "KI" => "Kiribati", "KP" => "Korea, Democratic People's Republic of",
                "KR" => "Korea, Republic of", "KW" => "Kuwait", "KG" => "Kyrgyzstan", "LA" => "Laos",
                "LV" => "Latvia", "LB" => "Lebanon", "LS" => "Lesotho", "LR" => "Liberia", "LY" => "Libyan Arab Jamahiriya",
                "LI" => "Liechtenstein", "LT" => "Lithuania", "LU" => "Luxembourg", "MO" => "Macao",
                "MK" => "Macedonia, the Former Yugoslav Republic of", "MG" => "Madagascar", "MW" => "Malawi",
                "MY" => "Malaysia", "MV" => "Maldives", "ML" => "Mali", "MT" => "Malta", "MH" => "Marshall Islands",
                "MQ" => "Martinique", "MR" => "Mauritania", "MU" => "Mauritius", "YT" => "Mayotte", "MX" => "Mexico",
                "FM" => "Micronesia, Federated States of", "MD" => "Moldova, Republic of", "MC" => "Monaco",
                "MN" => "Mongolia", "MS" => "Montserrat", "MA" => "Morocco", "MZ" => "Mozambique",
                "MM" => "Myanmar", "NA" => "Namibia", "NR" => "Nauru", "NP" => "Nepal", "NL" => "Netherlands",
                "AN" => "Netherlands Antilles", "NC" => "New Caledonia", "NZ" => "New Zealand", "NI" => "Nicaragua",
                "NE" => "Niger", "NG" => "Nigeria", "NU" => "Niue", "NF" => "Norfolk Island",
                "MP" => "Northern Mariana Islands", "NO" => "Norway", "OM" => "Oman", "PK" => "Pakistan", "PW" => "Palau",
                "PS" => "Palestinian Territory, Occupied", "PA" => "Panama", "PG" => "Papua New Guinea", "PY" => "Paraguay",
                "PE" => "Peru", "PH" => "Philippines", "PN" => "Pitcairn", "PL" => "Poland", "PT" => "Portugal",
                "PR" => "Puerto Rico", "QA" => "Qatar", "RE" => "Reunion", "RO" => "Romania", "RU" => "Russian Federation",
                "RW" => "Rwanda", "SH" => "Saint Helena", "KN" => "Saint Kitts and Nevis", "LC" => "Saint Lucia",
                "PM" => "Saint Pierre and Miquelon", "VC" => "Saint Vincent and the Grenadines", "WS" => "Samoa",
                "SM" => "San Marino", "ST" => "Sao Tome and Principe", "SA" => "Saudi Arabia", "SN" => "Senegal",
                "CS" => "Serbia and Montenegro", "SC" => "Seychelles", "SL" => "Sierra Leone", "SG" => "Singapore",
                "SK" => "Slovakia", "SI" => "Slovenia", "SB" => "Solomon Islands", "SO" => "Somalia",
                "ZA" => "South Africa", "GS" => "South Georgia and the South Sandwich Islands", "ES" => "Spain",
                "LK" => "Sri Lanka", "SD" => "Sudan", "SR" => "Suriname", "SJ" => "Svalbard and Jan Mayen",
                "SZ" => "Swaziland", "SE" => "Sweden", "CH" => "Switzerland", "SY" => "Syrian Arab Republic",
                "TW" => "Taiwan, Province of China", "TJ" => "Tajikistan", "TZ" => "Tanzania, United Republic of",
                "TH" => "Thailand", "TL" => "Timor-Leste", "TG" => "Togo", "TK" => "Tokelau", "TO" => "Tonga",
                "TT" => "Trinidad and Tobago", "TN" => "Tunisia", "TR" => "Turkey", "TM" => "Turkmenistan",
                "TC" => "Turks and Caicos Islands", "TV" => "Tuvalu", "UG" => "Uganda",
                "UA" => "Ukraine", "AE" => "United Arab Emirates", "GB" => "United Kingdom",
                "US" => "United States", "UM" => "United States Minor Outlying Islands","UY" => "Uruguay",
                "UZ" => "Uzbekistan", "VU" => "Vanuatu", "VE" => "Venezuela",
                "VN" => "Vietnam", "VG" => "Virgin Islands, British", "VI" => "Virgin Islands, U.s.",
                "WF" => "Wallis and Futuna", "EH" => "Western Sahara", "YE" => "Yemen",
                "ZM" => "Zambia", "ZW" => "Zimbabwe"
                );

        return view('participant.edit', compact('participant', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'first_name'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $participant = Participant::findOrFail($id);
        $photo = $participant->photo;

        if ($request->hasFile('photo')) {
            !empty($photo) ? File::delete(public_path('uploads/participant/' . $photo)):null;
            $photo = $this->saveFile($request->first_name, $request->file('photo'));
        }

        $participant->update([
            'participant_no' => $request->input('participant_no'),
            'category' => $request->input('category'),
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'company' => $request->input('company'),
            'address1' => $request->input('address1'),
            'address2' => $request->input('address2'),
            'city'=> $request->input('city'),
            'state'=> $request->input('state'),
            'postal_code'=> $request->input('postal_code'),
            'country'=> $request->input('country'),
            'email'=> $request->input('email'),
            'website'=> $request->input('website'),
            'phone'=> $request->input('phone'),
            'join_date'=> $request->input('join_date'),
            'active'=> 1,
            'comment'=> '-',
            'source_of_contact'=> '-',
            'user_id'=> auth()->user()->name,
            'photo'=> $photo,
        ]);

        if (!$participant) {
            return redirect()->back()->withInput()->withError('cannot update Participant');
        }else{
            return redirect('/participant')->with('success', 'Successfully update Participant');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        foreach ($request->input('id') as $value) {
            Participant::find($value)->delete();
        }
        return redirect()->back()->with('Success', "participant deleted Successfully");
    }

    public function export_excel()
    {
        return Excel::download(new ParticipantExport, 'participant.xlsx');
    }

    function import(Request $request)
    {
        $this->validate($request, [
            'file'  => 'required|mimes:xls,xlsx'
        ]);
        Excel::import(new ParticipantImport,request()->file('file'));
        return back()->with('success', 'Excel Data Participant Imported successfully.');;
     

     // $path = $request->file('select_file')->getRealPath();

     // $data = Excel::load($path)->get();

     // if($data->count() > 0)
     // {
     //  foreach($data->toArray() as $key => $value)
     //  {
     //   foreach($value as $row)
     //   {
     //    $insert_data[] = array(
     //     'id'  => $row['id'],
     //     'farmer_no'   => $row['farmer_no'],
     //     'name'   => $row['name'],
     //     'birthday'    => $row['birthday'],
     //     'religion'  => $row['religion'],
     //     'ethnic'   => $row['ethnic'],
     //     'origin'  => $row['origin'],
     //     'gender'   => $row['gender'],
     //     'join_date'   => $row['join_date'],
     //     'number_family_member'    => $row['number_family_member'],
     //     'ktp_no'  => $row['ktp_no'],
     //     'phone'   => $row['phone'],         
     //     'address'  => $row['address'],
     //     'village'   => $row['village'],
     //     'kecamatan'   => $row['kecamatan'],
     //     'city'    => $row['city'],
     //     'province'  => $row['province'],
     //     'post_code'   => $row['post_code'],
     //     'mu_no'  => $row['mu_no'],
     //     'group_no'   => $row['group_no'],
     //     'mou_no'   => $row['mou_no'],
     //     'main_income'    => $row['main_income'],
     //     'side_income'  => $row['side_income'],
     //     'active'   => $row['active'],
     //     'user_id' => $row['user_id'],
     //     'created_at' => $row['created_at'],
     //     'updated_at' => $row['updated_at']
     //    );
     //   }
     //  }

     //  if(!empty($insert_data))
     //  {
     //   DB::table('farmers')->insert($insert_data);
     //  }
     // }
     // return back()->with('success', 'Excel Data Farmer Imported successfully.');
    }
}
