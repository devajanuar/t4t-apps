<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use App\Kabupaten;
use App\Province;

class KabupatenController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:kabupaten-list');
         $this->middleware('permission:kabupaten-create', ['only' => ['create','store']]);
         $this->middleware('permission:kabupaten-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:kabupaten-delete', ['only' => ['destroy', 'delete']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provinsi = Province::pluck('name', 'province_code');
        $kabupatens = Kabupaten::all();
        return view('region.kabupaten.index', compact('provinsi','kabupatens'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kabupaten_no'=>'required|string|max:50|unique:kabupatens',
            'name'=>'required|string|max:50',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $kab = new Kabupaten();
        $kab->kabupaten_no = $request->kabupaten_no;
        $kab->name = $request->name;
        $kab->province_code = $request->province_code;
        $kab->save();

        if (!$kab) {
            return redirect()->back()->withInput()->withError('cannot create kabupaten / kota');
        }else{
            return redirect()->back()->with('success', 'Successfully create kabupaten / kota');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required|string',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $kab = Kabupaten::find($id);
        $kab->update($request->all());

        if (!$kab) {
            return redirect()->back()->withError('cannot update kabupaten / kota');
        }else{
            return redirect()->back()->with('success', 'Successfully update kabupaten / kota');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        foreach ($request->input('id') as $key => $value) {
            $kab = Kabupaten::find($value);
            $kab->delete();
        }

        if (!$kab) {
            return redirect()->back()->withError('cannot delete kabupaten / kota');
        }else{
            return redirect()->back()->with('success', 'Successfully delete kabupaten / kota');
        }
    }
}
