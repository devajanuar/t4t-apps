<?php

namespace App\Http\Controllers;

use App\Tree;
use App\Program;
use App\ProgramDetail;
use App\Participant;
use App\ManagementUnit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class ProgramController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:program-list');
         $this->middleware('permission:program-create', ['only' => ['create','store']]);
         $this->middleware('permission:program-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:program-delete', ['only' => ['destroy', 'delete']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $programs = Program::all();

        $begin = $request->input('begin');
        $end = $request->input('end');

        if(!$begin && !$end)
        {
            $now = date('Y-m-');
            $d1 = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
            // $begin = new \DateTime($now.'01');
            // $end = new \DateTime($now.$d1);
            $year = date('Y-01-01');
            $year_end  = date('Y-12-31');
            $begin = new \DateTime($year);
            $end = new \DateTime($year_end);
        }else{
            $begin = new \DateTime($begin);
            $end = new \DateTime($end);
        }
        
        return view('program.index', compact('programs', 'begin', 'end'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $trees = Tree::all();
        $mu = ManagementUnit::pluck('name', 'name');
        $year = ['2020', '2021', '2022', '2023', '2024', '2025', '2026', '2027', '2028', '2029', '2030'];
        // $par = Participant::pluck('first_name', 'participant_no');
        
        // return $trees;
        return view('program.create', compact('trees', 'mu', 'year'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // 'projectext_no'=>'required|string|max:10',
            // 'donor_id'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $p = new Program();
        $p->program_no = Program::Maxno();
        $p->type_plant = $request->input('planting_type');
        $p->year = $request->input('year');
        $p->location = $request->input('location');
        $p->total_trees = $request->input('total_trees');
        $p->description = $request->input('description');
        $p->program_note = $request->input('program_note');
        $p->total_qty = $request->input('total_qty');
        $p->approvement = 0;
        $p->active = 1;
        $p->user_id = 1;
        // $farmer->user_id = auth()->user()->username;
        $p->save();

        $this->programDetail($request, $p);

        $pd_total = ProgramDetail::where('program_no', $p->program_no)->sum('detail_total');

        // $program->purchasing_total = ($pd_total);
        $p->save();

        return $p;
        // if (!$p) {
        //     return redirect()->back()->withInput()->withError('cannot create project');
        // }else{
        //     return redirect('/program')->with('success', 'Successfully create program');
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $program = Program::find($id);
        $par = Participant::pluck('first_name', 'participant_no');
        
        return view('program.edit', compact('par', 'program'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            // 'projectext_no'=>'required|string|max:10',
            'donor_id'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $program = new Program();
        $program->program_no = $request->input('program_no');
        $program->donor_id = $request->input('donor_id');
        $program->pic_donor = $request->input('pic_donor');
        $program->donor_phone = $request->input('donor_phone');
        $program->email1 = $request->input('email1');
        $program->email2 = $request->input('email2');
        $program->donor_type = $request->input('donor_type');
        $program->true_nominal = $request->input('true_nominal');
        $program->rupiah_nominal = $request->input('rupiah_nominal');
        $program->win_type = $request->input('win_type');
        $program->tree_per_win = $request->input('tree_per_win');
        $program->planting_type = $request->input('planting_type');
        $program->main_donor = $request->input('main_donor');
        $program->donor_owner = $request->input('donor_owner');
        $program->payment_requirement = $request->input('payment_requirement');
        $program->total_trees = $request->input('total_trees');
        $program->tree_allocation = $request->input('tree_allocation');
        $program->location = $request->input('location');
        $program->start_project = $request->input('start_project');
        $program->end_project = $request->input('end_project');
        $program->other_requirement = $request->input('other_requirement');
        $program->approvement = 0;
        $program->active = 1;
        $program->user_id = 1;
        // $farmer->user_id = auth()->user()->username;
        $program->save();

        if (!$program) {
            return redirect()->back()->withInput()->withError('cannot create project');
        }else{
            return redirect('/program')->with('success', 'Successfully create program');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        foreach ($request->input('id') as $value) {
            Program::find($value)->delete();
        }
        return redirect()->back()->with('Success', "Program deleted Successfully");
    }

    public function programDetail($request, $program)
    {
        foreach ($request->detail_id as $key => $value) {
            if ($value != '') {
                $pd = ProgramDetail::find($value);
                $pd->tree_code = $request->tree_code[$key];
                $pd->program_no = $program->program_no;
                $pd->detail_qty = intval(str_replace(',','',$request->detail_qty[$key]));
                $pd->detail_unit = $request->detail_unit[$key];
                $pd->detail_total = intval(str_replace(',','',$request->detail_total[$key]));
                $pd->detail_date = null;
                $pd->save();
            }else{
                if($request->detail_qty[$key]!= null){
                    $detail = [
                        'tree_code' => $request->tree_code[$key],
                        'program_no' => $program->program_no,
                        'detail_qty' => intval(str_replace(',','',$request->detail_qty[$key])),
                        'detail_unit' => $request->detail_unit[$key],
                        'detail_total' => intval(str_replace(',','',$request->detail_total[$key])),
                        'detail_date' => null,
                    ];
                    ProgramDetail::create($detail);
                }
            }
        }
    }
}
