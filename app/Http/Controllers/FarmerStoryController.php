<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use App\FarmerStory;
use App\Farmer;

class FarmerStoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $farmers = Farmer::pluck('name', 'farmer_no');
        $story_no = FarmerStory::Maxno();
        $farmerstories = FarmerStory::all();
        return view('farmer.story.index', compact('farmers', 'story_no', 'farmerstories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'story_title'=>'required|string',
            'story_description'=>'required|string',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $fs = new FarmerStory();
        $fs->farmer_no = $request->farmer_no;
        $fs->story_no = $request->story_no;
        $fs->story_title = $request->story_title;
        $fs->year = $request->year;
        $fs->story_description = $request->story_description;
        $fs->active = $request->active;
        $fs->save();

        if (!$fs) {
            return redirect()->back()->withInput()->withError('cannot create farmer story');
        }else{
            return redirect()->back()->with('success', 'Successfully create farmer story');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'story_title'=>'required|string',
            'story_description'=>'required|string',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $fs = FarmerStory::find($id);
        $fs->update($request->all());

        if (!$fs) {
            return redirect()->back()->withError('cannot update farmer story');
        }else{
            return redirect()->back()->with('success', 'Successfully update farmer story');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        foreach ($request->input('id') as $key => $value) {
            $fs = FarmerStory::find($value);
            $fs->delete();
        }

        if (!$fs) {
            return redirect()->back()->withError('cannot delete farmer story');
        }else{
            return redirect()->back()->with('success', 'Successfully delete farmer story');
        }
    }
}
