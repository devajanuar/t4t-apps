<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use App\FarmerTestimonial;
use App\Farmer;

class FarmerTestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $farmers = Farmer::pluck('name', 'farmer_no');
        $farmertestimonials = FarmerTestimonial::all();
        return view('farmer.testimonial.index', compact('farmers','farmertestimonials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'testimonial'=>'required|string',
            'year'=>'required|date',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $ft = new FarmerTestimonial();
        $ft->farmer_no = $request->farmer_no;
        $ft->testimonial = $request->testimonial;
        $ft->year = $request->year;
        $ft->save();

        if (!$ft) {
            return redirect()->back()->withInput()->withError('cannot create farmer testimonial');
        }else{
            return redirect()->back()->with('success', 'Successfully create farmer testimonial');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'testimonial'=>'required|string',
            'year'=>'required|date',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $ft = FarmerTestimonial::find($id);
        $ft->update($request->all());

        if (!$ft) {
            return redirect()->back()->withError('cannot update farmer testimonial');
        }else{
            return redirect()->back()->with('success', 'Successfully update farmer testimonial');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'=>'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        foreach ($request->input('id') as $key => $value) {
            $ft = FarmerTestimonial::find($value);
            $ft->delete();
        }

        if (!$ft) {
            return redirect()->back()->withError('cannot delete farmer testimonial');
        }else{
            return redirect()->back()->with('success', 'Successfully delete farmer testimonial');
        }
    }
}
