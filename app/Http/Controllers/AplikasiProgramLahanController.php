<?php

namespace App\Http\Controllers;

use App\Tree;
use App\Farmer;
use App\Lahan;
use App\AplikasiProgramLahan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AplikasiProgramLahanController extends Controller
{
    public function index(Request $request)
    {
        $begin = $request->input('begin');
        $end = $request->input('end');
        $today = date('Y-m-d');
        
        if(!$begin && !$end)
        {
            $now = date('Y-m-');
            $d1 = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
            $begin = new \DateTime($now.'01');
            $end = new \DateTime($now.$d1);
        }else{
            $begin = new \DateTime($begin);
            $end = new \DateTime($end);
        }

        if (empty($request->status)) {
            $apls = AplikasiProgramLahan::orderBy('created_at', 'desc')
                                    ->whereDate('created_at', '>=', $begin)
                                    ->whereDate('created_at','<=', $end)
                                    ->get();
        }else{
            $apls = AplikasiProgramLahan::orderBy('created_at', 'desc')
                                    ->whereDate('created_at', '>=', $begin)
                                    ->whereDate('created_at','<=', $end)
                                    ->where('status', $request->status)
                                    ->get();
        }
        return view('lahan.aplikasiprogram.index', compact('apls', 'begin', 'end', 'today'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $farmer_list = Farmer::pluck('name', 'farmer_no');
        $lahan_list = Lahan::pluck('lahan_no', 'lahan_no');
        $year = ['2020', '2021', '2022', '2023', '2024', '2025', '2026', '2027', '2028', '2029', '2030'];
        $program = ['Arboretum', 'Agroforestri', 'Hutan Rakyat', 'Padi Organik', 'Konservasi dan restorasi tanah dan air', 'Sayur Organik', 'Desa Konservasi', 'Kopi'];
        $tree = Tree::pluck('tree_name', 'tree_name');

        return view('lahan.aplikasiprogram.create', compact('farmer_list', 'year', 'program', 'tree', 'lahan_list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required',
            // 'ktp_no'=>'required|string|unique:farmers',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $apl = new AplikasiProgramLahan();
        $apl->year = $request->input('year');
        $apl->farmer_no = $request->input('farmer_no');
        $apl->name = $request->input('name');
        $apl->nik = $request->input('nik');
        $apl->address = $request->input('address');
        $apl->lahan_no = $request->input('lahan_no');
        $apl->document_no = $request->input('document_no');
        $apl->type_program = $request->input('type_program');
        $apl->lahan_status = $request->input('lahan_status');
        $apl->luas_lahan = $request->input('luas_lahan');
        $apl->tutupan_lahan = $request->input('tutupan_lahan');
        $apl->luas_tanam = $request->input('luas_tanam');
        $apl->tree1 = $request->input('tree1');
        $apl->tree2 = $request->input('tree2');
        $apl->tree3 = $request->input('tree3');
        $apl->tree4 = $request->input('tree4');
        $apl->tree5 = $request->input('tree5');
        $apl->user_id = '1';
        // $farmer->user_id = auth()->user()->username;
        $apl->save();

        if (!$apl) {
            return redirect()->back()->withInput()->withError('cannot create Form Aplikasi Program');
        }else{
            return redirect('/lahan/aplikasiprogram')->with('success', 'Successfully create Form Aplikasi Program');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $apl = AplikasiProgramLahan::find($id);
        return view ('lahan.aplikasiprogram.edit', compact('apl'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required',
            // 'ktp_no'=>'required|string|unique:farmers',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages(); 
            
            return redirect()->back()->withInput()->withErrors($validator);
        }
        
        $apl = AplikasiProgramLahan::find($id);
        $apl->year = $request->input('year');
        $apl->farmer_no = $request->input('farmer_no');
        $apl->name = $request->input('name');
        $apl->nik = $request->input('nik');
        $apl->address = $request->input('address');
        $apl->lahan_no = $request->input('lahan_no');
        $apl->document_no = $request->input('document_no');
        $apl->type_program = $request->input('type_program');
        $apl->lahan_status = $request->input('lahan_status');
        $apl->luas_lahan = $request->input('luas_lahan');
        $apl->tutupan_lahan = $request->input('tutupan_lahan');
        $apl->luas_tanam = $request->input('luas_tanam');
        $apl->tree1 = $request->input('tree1');
        $apl->tree2 = $request->input('tree2');
        $apl->tree3 = $request->input('tree3');
        $apl->tree4 = $request->input('tree4');
        $apl->tree5 = $request->input('tree5');
        $apl->user_id = '1';
        // $farmer->user_id = auth()->user()->username;
        $apl->save();

        if (!$apl) {
            return redirect()->back()->withInput()->withError('cannot create Form Aplikasi Program');
        }else{
            return redirect('/lahan/aplikasiprogram')->with('success', 'Successfully create Form Aplikasi Program');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        foreach ($request->input('id') as $value) {
            AplikasiProgramLahan::find($value)->delete();
        }
        return redirect()->back()->with('Success', "Aplikasi Program Lahan deleted Successfully");
    }
}
