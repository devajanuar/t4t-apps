<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableTransactionTrees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_trees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->dateTime("transaction_at");
            $table->string("win_number")->unique();
            $table->string("tree_latitude")->nullable();
            $table->string("tree_longitude")->nullable();
            $table->string("tree_code")->unique();
            $table->foreign("tree_code")
                ->references("tree_code")->on("trees")
                ->onDelete("cascade");
            $table->integer("quantity")->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_trees');
    }
}
