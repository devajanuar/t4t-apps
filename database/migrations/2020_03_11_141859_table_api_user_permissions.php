<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableApiUserPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_user_permissions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger("api_users_id");
            $table->string("permission_name");
            $table->tinyInteger("is_active")->default(0);
            $table->unique(['api_users_id','permission_name']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_user_permissions');
    }
}
