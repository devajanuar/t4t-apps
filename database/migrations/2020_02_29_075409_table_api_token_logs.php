<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableApiTokenLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_token_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->unsignedBigInteger('api_users_id');
            $table->foreign("api_users_id")
                ->references("id")->on("api_users")
                ->onDelete("cascade");
            $table->string("user_agent");
            $table->string("ip_address");
            $table->string("token");
            $table->dateTime("expired_at");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_token_logs');
    }
}
