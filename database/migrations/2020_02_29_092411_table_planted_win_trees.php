<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablePlantedWinTrees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planted_win_trees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->date("planted_date");
            $table->string("lahan_no");
            $table->foreign("lahan_no")
                ->references("lahan_no")->on("lahans")
                ->onDelete("cascade");
            $table->string("win_number");
            $table->foreign("win_number")
                ->references("win_number")->on("transaction_trees")
                ->onDelete("cascade");
            $table->string("tree_code");
            $table->foreign("tree_code")
                ->references("tree_code")->on("trees")
                ->onDelete("cascade");
            $table->integer("quantity")->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planted_win_trees');
    }
}
