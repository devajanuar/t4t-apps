<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParticipantNoTransTree extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_trees', function (Blueprint $table) {
            //
            $table->string("participant_no");
            $table->foreign("participant_no")
                ->references("participant_no")
                ->on("participants")
                ->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_trees', function (Blueprint $table) {
            //
        });
    }
}
